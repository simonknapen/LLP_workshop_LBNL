#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <TRandom3.h>
#include "TGraphErrors.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TF1.h"

//  declare a single instance of the class
//  make a track by calling 
//     track(px, py, pz, vx, vy, vz, charge);
//
//  that calculates the positions of the hits
//  int n()                - returns number of hits
//  unsigned int stubmap() - returns bitmap of stubs (layer 1 is LSB)
//  int get_stubs(double *r, double *phi, double *z)
//                         - returns the number of stubs, as well as their positions
//  int get_smearedstubs(double *r, double *phi, double *z , double *sphi, double *sz)
//                         - same as get_stubs, but the positions of stubs are smeared
//                           sphi and sz will hold the errors on hit positions.
//  int fit()  fits the smeared track. Returns 0 if the fit converged
//             every time you call the fit(), it re-smears the hits
//
//  double frinv(), fphi0(), fd0(), ft(), fz0() - return the fit results
//  get_conv(double **m) returns covariance matrix m[5][5], as determined by the number of stubs
//


class trigfit {

 public:

  static double helix(Double_t *x, Double_t *par)
  {
    double rproj = fabs(x[0]);
    
    double rinv = par[0];
    double phi0 = par[1];
    double d0   = par[2];
    double t    = par[3];
    double z0   = par[4];
    
    double rho = 1/rinv;
    double r0 = rho - d0;
    double phiproj=phi0-asin((rproj*rproj+r0*r0-rho*rho)/(2*rproj*r0));
    double beta = acos((rho*rho+r0*r0-rproj*rproj)/(2*r0*rho));
    double zproj=z0+t*fabs(rho)*fabs(beta);
    
    if(x[0]>0){
      // that is the phi measurement
      return phiproj;
    }
    else{
      //this is z measurement
    return zproj;
    }
  }

  static double prop(double phi, double R, double xD, double yD, double rho, double &beta, bool &isok) {
    
    double xD0 = xD*cos(phi) + yD*sin(phi);
    double yD0 = yD*cos(phi) - xD*sin(phi);

    double x1  = xD0;
    double y1  = R + yD0;
    double d2  = x1*x1 + y1*y1;
    double d   = sqrt(d2);
    
    double phi_c = - atan(x1 / y1);
    int s = R>0? 1 : -1;
    double y2  = (rho*rho + d2 - R*R)/(2*d)*s;
    
    double phi2 = y2 / rho;
    if(fabs(phi2)>1){
      isok = false;
      return 0;
    }
    isok = true;
    phi2 = asin(phi2);
    
    double yy = rho * sin(phi2+phi_c);
    double cbeta = (y1-yy)/R;
    beta = 0;
    if(fabs(cbeta)<=1)
      beta = acos(cbeta);
    else {
      printf("cbeta too large %f\n",cbeta);
    }
    return  phi + phi2 + phi_c;
  }

  void initialize(){
    printf("initializing trigstubs...\n");
    bool isok;
    double beta;
    double R2 = 100 * pt_cut / (0.3 * 3.8);
    for(int l=0; l<6; ++l){
      double phi12 = prop(1., R2, 0, 0, Rlayer[l], beta, isok);
      assert(isok);
      double phi22 = prop(1., R2, 0, 0, Rlayer[l]+DRlayer[l], beta, isok);
      assert(isok);
      phi22 = acos(cos(phi22 - phi12));
      bend_cut[l] = fabs(phi22 * Rlayer[l]);
    }
    f1 = new TF1("f1",helix,-120,120,5);
    is_initialized = true;
  }

  void track (double px, double py, double pz, double vx, double vy, double vz, int charge){

    if(!is_initialized) initialize();
    px_ = px;
    py_ = py;
    pz_ = pz;
    vx_ = vx;
    vy_ = vy;
    vz_ = vz;
    q_  = charge;
    
    double pt = sqrt(px*px+py*py);
    double phi = atan2(py,px);
    double RR = 100 * pt / (0.3 * 3.8);
    if(charge<0) RR = -RR;

    bool isok1, isok2;
    stubmap_ =0;
    n_ = 0;
    double beta = 0;
    for(unsigned int l=0; l<6; ++l){
      double phi2 = prop(phi, RR, vx, vy, Rlayer[l]+DRlayer[l], beta, isok2);
      double phi1 = prop(phi, RR, vx, vy, Rlayer[l], beta, isok1);
      phi2 = asin(sin(phi2 - phi1));
      double bend = phi2 * Rlayer[l];
      if(isok1 && isok2 && bend_cut[l]-fabs(bend)>0 ){
	//trigger stub
	stubmap_ |= (1<<l);
	r_[n_]   = Rlayer[l];
	phi_[n_] = phi1;
	z_[n_]   = vz + pz/pt*fabs(RR*beta);
	++n_;
      }
    }
  }

  double frinv(){return frinv_;}
  double fphi0(){return fphi0_;}
  double fd0(){return fd0_;}
  double ft(){return ft_;}
  double fz0(){return fz0_;}
  int  fstat(){return fstat_;}

  int fit()
  {
    double stub_r[6], stub_phi[6], stub_z[6];
    double stub_sphi[6], stub_sz[6];

    int n = get_smearedstubs(stub_r, stub_phi, stub_z, stub_sphi, stub_sz);

    TGraphErrors *tg = new TGraphErrors();
    for(int is=0; is<n; is++){
      tg->SetPoint(n+is, stub_r[is], stub_phi[is]);
      tg->SetPointError(n+is, 0, stub_sphi[is]);
      tg->SetPoint(n-is-1, -stub_r[is], stub_z[is]);
      tg->SetPointError(n-is-1, 0, stub_sz[is]);
    }

    double pt = sqrt(px_*px_+py_*py_);
    double phi = atan2(py_,px_);
    double t   = pz_ / pt;
    f1->FixParameter(0,-0.3*3.8/(100. * pt));
    f1->SetParameter(1,phi);
    f1->SetParameter(2,0);
    f1->SetParameter(3,t);
    f1->SetParameter(4,vz_);
    tg->Fit("f1","Q");
    f1->ReleaseParameter(0);
    fstat_ = tg->Fit("f1","Q");
    int fcount = 100;
    while (fstat_!=0 && fcount>0){
      f1->SetParameter(0,f1->GetParameter(0)+0.01*rr.Rndm());
      fstat_ = tg->Fit("f1","Q");
      fcount --;
    }
    frinv_ = f1->GetParameter(0);
    fphi0_ = f1->GetParameter(1);
    fd0_   = f1->GetParameter(2);
    ft_    = f1->GetParameter(3);
    fz0_   = f1->GetParameter(4);

    return fstat_;
  }
  
  unsigned int stubmap() { return stubmap_;}
  int n() {return n_;}
  void get_cov(double **m)
  {
    double m4[5][5] = {{6.3538403e-11,  4.98958e-09,   -1.8470201e-07, 6.1274503e-11,  2.6809638e-10},
		       {4.98958e-09,    3.9635159e-07, -1.4853274e-05, 4.8459058e-09,  1.9386603e-08},
		       {-1.8470201e-07, -1.4853274e-05, 0.00056646148, -1.7641721e-07, -1.1102913e-06},
		       {6.1274503e-11,  4.8459058e-09, -1.7641721e-07, 0.00042241437,  -0.021373728},
		       {2.6809638e-10,  1.9386603e-08, -1.1102913e-06, -0.021373728,    1.0835077}};

    double m5[5][5] = { {2.2678636e-11,  1.6204201e-09, -5.114969e-08,   2.1705746e-12, -6.9255573e-10},
			{1.6204201e-09,  1.18215e-07,   -3.8261724e-06,  1.7791819e-10, -5.0886742e-08},
			{-5.114969e-08, -3.8261724e-06,  0.00012921155, -6.6947713e-09,  1.7609029e-06},
			{2.1705746e-12,  1.7791819e-10, -6.6947713e-09,  1.5756297e-05, -0.00068327473},
			{-6.9255573e-10, -5.0886742e-08, 1.7609029e-06, -0.00068327473,  0.030571069}};

    double m6[5][5] = { {1.0445188e-11,  6.8212168e-10,  -1.786274e-08,   6.2503409e-13,  3.6837802e-11},
			{6.8212168e-10,  4.6105927e-08,  -1.2643472e-06,  4.214812e-11,   2.6362533e-09},
			{-1.786274e-08, -1.2643472e-06,   3.8105523e-05, -1.1015259e-09, -8.3006037e-08},
			{6.2503409e-13,  4.214812e-11,   -1.1015259e-09,  4.6743873e-06, -0.00017144811},
			{3.6837802e-11,  2.6362533e-09,  -8.3006037e-08, -0.00017144811,  0.0069131731}};

    for(int i1=0; i1<5; ++i1)
      for(int i2=0; i2<5; ++i2)
	if(n_ == 4)
	  m[i1][i2] = m4[i1][i2];
	else if(n_==5)
	  m[i1][i2] = m5[i1][i2];
	else if(n_==6)
	  m[i1][i2] = m6[i1][i2];
  }
  int get_smearedstubs(double *r, double *phi, double *z , double *sphi, double *sz){
    for(int i=0; i<n_; ++i){
      r[i]   =   r_[i];
      phi[i] = phi_[i] + 0.01 /r_[i] * (rr.Rndm()-0.5);
      sphi[i] = 0.01 /r_[i]/sqrt(12.);
      if(r_[i]<60){
	z[i]   =   z_[i] + 0.15 * (rr.Rndm()-0.5);
	sz[i]  =  0.15/sqrt(12.);
      }
      else{ 
	z[i]   =   z_[i] + 5. * (rr.Rndm()-0.5);
	sz[i]  =   5./sqrt(12.);
      }
    }
    return n_;
  }
  int get_stubs(double *r, double *phi, double *z){
    for(int i=0; i<n_; ++i){
      r[i]   =   r_[i];
      phi[i] = phi_[i];
      z[i]   =   z_[i];
    }
    return n_;
  }
  
 private:
   //geometry for the stub finding
   double Rlayer[6]={23, 36, 51, 68, 88, 108};
   double DRlayer[6] = {0.26, 0.16, 0.16, 0.18, 0.18, 0.18};
   double pt_cut = 2; //hits are out if stub pt>pt_cut GeV

   bool is_initialized = false;
   double bend_cut[6];
   TRandom3 rr;

   //track pars
   // truth:
   double px_;
   double py_;
   double pz_;
   double vx_;
   double vy_;
   double vz_;
   int q_;

   //fit:
   TF1 *f1;
   double frinv_;
   double fphi0_;
   double fd0_;
   double ft_;
   double fz0_;
   int fstat_;
   
   //stubs:
   unsigned int stubmap_;
   int n_;
   double r_[6];
   double phi_[6];
   double z_[6];
   
};
