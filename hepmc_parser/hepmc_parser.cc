// -*- C++ -*-
//
// This file is part of HepMC
// Copyright (C) 2014 The HepMC collaboration (see AUTHORS for details)
<<<<<<< HEAD

=======
//
// This file reads in a hepmc file, performs an operation on particles with specific properties, and writes out new hepmc file.
// In this example, the code class "trigstubs.h" checks whether or not a track is reconstructed by the CMS track trigger. It stores then stores this information in the status code of the particles in the outgoing hepmc file.
// writen by Simon 06/29/2018.
//
>>>>>>> 110de6f0a5072f5e93dbe02497e00f3707c113a0

#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenEvent.h"
#include "trigstubs.h"
#include <stdlib.h>
#include <vector>
#include "TROOT.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TFile.h"



int main(int argc, char *argv[]) { 
    
    if(!(argc==3)){
    std::cout << "I need inputfile name and outputfile name";
    return 0;
  }

    gROOT->ProcessLine("#include <vector>");
    
    //create the file, the Tree and a few branches
    TFile f(argv[2],"recreate");
    TTree t1("t1","a simple Tree with simple variables");
    std::vector<float> px, py, pz;
    std::vector<float> Vx, Vy, Vz;
    t1.Branch("px","std::vector",&px);
    t1.Branch("py","std::vector",&py);
    t1.Branch("pz","std::vector",&pz);
    t1.Branch("Vx","std::vector",&Vx);
    t1.Branch("Vy","std::vector",&Vy);
    t1.Branch("Vz","std::vector",&Vz);
    float px1, py1, pz1, Vx1, Vy1, Vz1;
    
    
    { // begin scope of ascii_in and ascii_out
    
    //create trig stub object   
        
	HepMC::IO_GenEvent ascii_in(argv[1],std::ios::in);
	// declare another IO_GenEvent for writing out the good events
    HepMC::FourVector mom;
    HepMC::FourVector vert;
            
	//........................................EVENT LOOP
	int icount=0;
	int num_good_events=0;
	HepMC::GenEvent* evt = ascii_in.read_next_event();
	while ( evt ) {
	    icount++;
        px.clear();
        py.clear();
        pz.clear();
        Vx.clear();
        Vy.clear();
        Vz.clear();
        std::cout << "Processing Event Number " << icount <<"\n";
	    if ( icount%50==1 ) std::cout << "Processing Event Number " << icount
					  << " its # " << evt->event_number() 
					  << std::endl;
	    
        
        for ( HepMC::GenEvent::particle_const_iterator p 
		  = evt->particles_begin(); p != evt->particles_end(); ++p ) {
	       if ( abs((*p)->pdg_id())==13 && (*p)->status()==1 ) 
            {    
            
            mom=(*p)->momentum();
            vert=(*p)->production_vertex()->position();
        
            px.push_back(mom.px());
            py.push_back(mom.py());
            pz.push_back(mom.pz());
            Vx.push_back(vert.x());
            Vy.push_back(vert.y());
            Vz.push_back(vert.z());      
           } 
        }
  
        
        t1.Fill();    
    	
		++num_good_events;
	    
        delete evt;
	    ascii_in >> evt;
	
    }
    t1.Write();
        
	//........................................PRINT RESULT
	std::cout << num_good_events << " out of " << icount 
		  << " processed events passed the cuts. Finished." << std::endl;
    } // end scope of ascii_in and ascii_out
    return 0;
//}
}



