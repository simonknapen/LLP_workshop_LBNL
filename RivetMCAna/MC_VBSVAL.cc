// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/RivetAIDA.hh"
#include "Rivet/RivetHepMC.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
//#include "Rivet/Projections/DressedLeptons.hh" //--- Rivet 2.X
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/LeptonClusters.hh"

namespace Rivet {


  class MC_VBSVAL : public Analysis {
  private:

    // Useful enums, constants, etc..
    
    /// Define fiducial phase-space definition
    enum FiducialPhaseSpaceDef {
      fid_WpWpVBS_Extended, ///< Generator-level study in extended phase space
      fid_WpWpVBSLoose, ///< Analysis-level phase space
      fid_WpWpVBS ///< Analysis-level phase space
    };

    enum PhaseSpaceSelections {
      ps_PreFilter, ///< number of starting events
      ps_All, ///< no selections (after filters)
      ps_TauVeto, ///< number of leptons from taus 
      ps_NLep, ///< at least 2 leptons, pT > XX GeV, |eta| < YY 
      ps_Lep1Pt, ///< leading lepton pT > XX GeV
      ps_DRLep, ///< if applicable, DR(l,l)>ZZ
      ps_LepVeto, ///< third lepton veto with relaxed pT,|eta| requirements (if applicable)
      ps_Mll, /// m(ll) > XX GeV
      ps_SS, ///SS leptons
      ps_Zveto, /// |m(ll) - m(Z)| > XX GeV for e-e channel
      ps_NJets, ///< >= 2 jets, pT > XX GeV, |eta| < YY 
      ps_DRJetsLep, ///< if applicable, min(DR(l,j))>ZZ
      ps_Met, ///< if applicable, Met (with neutrinos) > XX GeV
      ps_BVeto, ///< if applicable, veto true b-jets
      ps_DRjj, ///< DR of tagging jets > XX (double-check jet distance)
      ps_Mjj, ///< di-jet invariant mass
      ps_DeltaY, ///< if applicable, |delta_eta(jj)| > XX
      ps_NCuts
    };

    enum channelList {
      cl_ee,
      cl_em,
      cl_mm,
      cl_et,
      cl_mt,
      cl_tt,
      cl_NChannels
    };


    static bool ptbare_comp(const ClusteredLepton p1,const ClusteredLepton p2)
    {
      return (p1.constituentLepton().momentum().pT() > p2.constituentLepton().momentum().pT());
    }


  public:

    /// @name Settings
    //@{
    bool _useShapeComparison; ///< Shape-only comparison
    FiducialPhaseSpaceDef _vbsPhaseSpace;
    bool _adaptCutsForWZjj; //adapt cuts for a 3-lepton region
    //@}

    /// @name Constructors etc.
    //@{

    /// Constructor
    MC_VBSVAL()
      : Analysis("MC_VBSVAL")
    {
      idxEvent=-1;
      _useShapeComparison=false;
      _vbsPhaseSpace=fid_WpWpVBS; 
      _adaptCutsForWZjj=false; //switch to 3-lepton final-state

      /// No need of absolute normalization
      if (!_useShapeComparison)
        setNeedsCrossSection(false);
      else
        setNeedsCrossSection(true);
    }    

    //@}

  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      //init cut values
      if (_vbsPhaseSpace == fid_WpWpVBS_Extended) {
        _cutValues[ps_PreFilter] = -999.0; // not set
        _cutValues[ps_All] = -999.0; // not set
	_cutValues[ps_TauVeto] = 1;
	if (not _adaptCutsForWZjj)
	  _cutValues[ps_NLep] = 2.0; // >= 2 leptons
	else
	  _cutValues[ps_NLep] = 3.0; // >= 3 leptons
        _cutValues_fidLep_pT = 10.0*GeV;
        _cutValues_fidLep_eta = 5.0;
        _cutValues[ps_Lep1Pt] = -999.; 
        _cutValues[ps_DRLep] = -999.; // disabled;
        _cutValues[ps_LepVeto] = 0; //disabled
        _cutValues_fidLepVeto_el_pT = 7.0*GeV; //used for ele-jet overlap removal
        _cutValues_fidLepVeto_mu_pT = 6.0*GeV;
        _cutValues_fidLepVeto_Eta = 2.5; //used for ele-jet overlap removal
	_cutValues_fidLepVeto_dRJets = 0.4; //min DR between lepton and jet to bo considered
        _cutValues[ps_Mll] = -999.; //not applied
	if (not _adaptCutsForWZjj)
	  _cutValues[ps_SS] = +1; // product of leptons' electric charges
	else
	  _cutValues[ps_SS] = 0; //no SS requirement
        _cutValues[ps_Zveto] = -999.; //disabled
        _cutValues[ps_NJets] = 2.0; //>= 2 jets
        _cutValues_fidJets_pT = 20.*GeV;
        _cutValues_fidJets_eta = 5.0;
	_cutValues_fidJets_eleOverlap_dR = 0.05;
        _cutValues[ps_DRJetsLep] = -999.; //disabled
        _cutValues[ps_Met] = -999.; //disabled
	_cutValues[ps_BVeto] = -999.; //disabled
	_cutValues[ps_DRjj] = 0.4;
        _cutValues[ps_Mjj] = 150.0; // m(jj) > 150 GeV
        _cutValues[ps_DeltaY] = -999.; //disabled
      } else if (_vbsPhaseSpace == fid_WpWpVBS or _vbsPhaseSpace == fid_WpWpVBSLoose) {
        _cutValues[ps_PreFilter] = -999.0; // not set
        _cutValues[ps_All] = -999.0; // not set
	_cutValues[ps_TauVeto] = 1;
	if (not _adaptCutsForWZjj)
	  _cutValues[ps_NLep] = 2.0; // >= 2 leptons
	else
	  _cutValues[ps_NLep] = 3.0; // >= 3 leptons -- WZ case
        _cutValues_fidLep_pT = 25.0*GeV;        
        _cutValues_fidLep_eta = 2.5;
        _cutValues[ps_Lep1Pt] = 25.0*GeV; 
        _cutValues[ps_DRLep] = 0.3;
        //_cutValues[ps_LepVeto] = 1; //enabled
	_cutValues[ps_LepVeto] = 0; //disabled
        _cutValues_fidLepVeto_el_pT = 7.0*GeV;
        _cutValues_fidLepVeto_mu_pT = 6.0*GeV;
        _cutValues_fidLepVeto_Eta = 2.5;
	_cutValues_fidLepVeto_dRJets = 0.3; //min DR between lepton and jet to bo considered
        _cutValues[ps_Mll] = 20.0*GeV; 
	if (not _adaptCutsForWZjj)
	  _cutValues[ps_SS] = 1.0; // require SS leptons
	else
	  _cutValues[ps_SS] = 0.0; // disabled for WZ case
        _cutValues[ps_Zveto] = -999.; //10 GeV window -> disabled
        _cutValues[ps_NJets] = 2.0; //>= 2 jets
        _cutValues_fidJets_pT = 30.*GeV;
        _cutValues_fidJets_eta = 4.5;
	_cutValues_fidJets_eleOverlap_dR = 0.05;
        _cutValues[ps_DRJetsLep] = 0.3;        
        _cutValues[ps_Met] = 40.*GeV; 
	_cutValues[ps_BVeto] = -999; //1; //veto events with at least ps_BVeto b-jets
	if (_vbsPhaseSpace == fid_WpWpVBSLoose) {
	  _cutValues[ps_DRjj] = 0.4;
	  _cutValues[ps_Mjj] = 500.0; // m(jj) > 150 GeV
	  _cutValues[ps_DeltaY] = -999.; 
	} else {
	  _cutValues[ps_DRjj] = 0.4;
	  _cutValues[ps_Mjj] = 500.0; // m(jj) > 150 GeV
	  _cutValues[ps_DeltaY] = 2.4; 
	}
      }

      //some hypothesis checks
      if (static_cast<int>(_cutValues[ps_NLep]) < 2) {
        MSG_ERROR("This analysis is supposed to work on >= 2 leptons.");
      }

      //reset counters
      for (int ps = ps_PreFilter; ps <= ps_NCuts; ps++) {
        PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
        _processedEvents[idx]=0;
        _fiducialWeights[idx]=0.0;
        _fiducialWeights2[idx]=0.0;
	for (int ic = cl_ee; ic < cl_NChannels; ++ic) {
	  _fiducialWeightsByChannel[ic][idx]=0.0;
	  _fiducialWeightsByChannel[ic][idx]=0.0;
	}
      }

      // Initialise and register projections here
      //Charged leptons
      ///--- Rivet 2.X
      // const FinalState fs;
      // ChargedLeptons bare_leptons(fs);
      // Cut cuts = (Cuts::abseta < 2.5) & (Cuts::pT > 25*GeV);
      // DressedLeptons leptons(fs, bare_leptons, 0.1, cuts);
      // declare(leptons, "DressedLeptons");		  
      ///--- Rivet 1.8.1
      IdentifiedFinalState chLep_fid = IdentifiedFinalState(FinalState(-1000., 1000., 0.0));
      chLep_fid.acceptIdPair(ELECTRON);
      chLep_fid.acceptIdPair(MUON);
      addProjection(chLep_fid, "ChLep_fid");
      vector< std::pair< double, double > > etaRanges;
      etaRanges.push_back(make_pair<double, double>(-1000., 1000.));
      addProjection(LeptonClusters(FinalState(-1000., 1000.), chLep_fid, 0.1, true,
                                   etaRanges, 0.0), "DressedLeptons"); 

      //Neutrinos (high-pT)
      const FinalState fsn(-6.0, 6.0, 5.*GeV);
      IdentifiedFinalState neutrinofs(fsn);
      neutrinofs.acceptNeutrinos();
      addProjection(neutrinofs, "Neutrinos");

      //Jets, anti-kt 0.4
      FinalState fs(-7.0, 7.0, 0.*GeV);
      addProjection(fs, "fs");
      VetoedFinalState fsJets(fs); //final state for jet finding: veto leptons and neutrinos
      fsJets.vetoNeutrinos();
      //fsJets.addVetoPairDetail(ELECTRON, 10.*GeV, numeric_limits<double>::max());
      fsJets.addVetoPairDetail(MUON, 0.*GeV, numeric_limits<double>::max());      
      //fsJets.addVetoPairDetail(TAU, 10.*GeV, numeric_limits<double>::max());
      addProjection(FastJets(fsJets, FastJets::ANTIKT, 0.4), "Jets");
      
      //unstable particle for finding taus
      addProjection(UnstableFinalState(-5., 5., 5.*GeV), "UnstableParticles");

      //missing momentum
      MissingMomentum vismom(fs);
      addProjection(vismom, "MissingET");
      
      // --- Book histograms
      _h_Nm1_lep1_pt = bookHistogram1D("Nm1_lep1_pt", 50, 0., 200.);
      _h_Nm1_lep2_pt = bookHistogram1D("Nm1_lep2_pt", 50, 0., 200.);
      _h_Nm1_lep1_eta = bookHistogram1D("Nm1_lep1_eta", 120, -6., 6.);
      _h_Nm1_lep2_eta = bookHistogram1D("Nm1_lep2_eta", 120, -6., 6.);
      _h_Nm1_mll = bookHistogram1D("Nm1_mll", 200, 0., 400.);
      _h_Nm1_mll_Zveto = bookHistogram1D("Nm1_mll_Zveto", 200, 0., 400.);
      _h_Nm1_jet1_pt = bookHistogram1D("Nm1_jet1_pt", 200, 0., 1000.);
      _h_Nm1_jet2_pt = bookHistogram1D("Nm1_jet2_pt", 200, 0., 1000.);
      _h_Nm1_jet1_eta = bookHistogram1D("Nm1_jet1_eta", 120, -6., 6.);
      _h_Nm1_jet2_eta = bookHistogram1D("Nm1_jet2_eta", 120, -6., 6.);
      _h_Nm1_deltaY = bookHistogram1D("Nm1_deltaY", 50, 0, 8.0);
      _h_Nm1_mjj = bookHistogram1D("Nm1_mjj", 100, 0., 1000.);
      _h_Nm1_NJets = bookHistogram1D("Nm1_NJets", 10, 0., 10.);
      _h_Nm1_missingET = bookHistogram1D("Nm1_missingET",20,0,500);
      _h_Nm1_BVeto = bookHistogram1D("Nm1_BVeto", 10, 0., 10.);
      
      _h_lep1_pt = bookHistogram1D("lep1_pt", 50, 0., 200.);
      _h_lep2_pt = bookHistogram1D("lep2_pt", 50, 0., 200.);
      _h_lep1_eta = bookHistogram1D("lep1_eta", 120, -6., 6.);
      _h_lep2_eta = bookHistogram1D("lep2_eta", 120, -6., 6.);
      _h_lep_deltaEta = bookHistogram1D("lep_deltaEta", 24, -6., 6.);
      _h_mll = bookHistogram1D("mll", 200, 0., 400.);
      _h_lep_centrality = bookHistogram1D("lep_centrality",24, -6., 6.);
      _h_NJets = bookHistogram1D("NJets", 10, 0., 10.);
      _h_jet1_pt = bookHistogram1D("jet1_pt", 200, 0., 1000.);
      _h_jet2_pt = bookHistogram1D("jet2_pt", 200, 0., 1000.);
      _h_jet3_pt = bookHistogram1D("jet3_pt", 200, 0., 1000.);
      _h_jet1_eta = bookHistogram1D("jet1_eta", 120, -6., 6.);
      _h_jet2_eta = bookHistogram1D("jet2_eta", 120, -6., 6.);
      _h_jet3_eta = bookHistogram1D("jet3_eta", 120, -6., 6.);
      _h_mjj = bookHistogram1D("mjj", 200, 0., 4000.);
      _h_mjj_low = bookHistogram1D("mjj_low", 50, 0., 800.);
      _h_oppHem = bookHistogram1D("oppHem", 120, -30., 30.);
      _h_deltaPhi_jj = bookHistogram1D("deltaPhi_jj", 50, 0, 3.2);
      _h_deltaY_jj = bookHistogram1D("deltaY_jj", 50, 0, 8.0);
      _h_deltaR_jj = bookHistogram1D("deltaR_jj", 40, 0, 8.0);
      _h_deltaPt_jj = bookHistogram1D("deltaPt_jj", 25, 0, 300.);
      _h_etaStar3 = bookHistogram1D("etaStar3", 120, -6., 6.);
      _h_Wmass = bookHistogram1D("Wmass", 100, -2, 198.);
      _h_missingET = bookHistogram1D("missingET",20,0,500);
      
      _h_deltaR_ej = bookHistogram1D("deltaR_ej", 250, 0, 5.0);
      _h_deltaR_vetol_j = bookHistogram1D("deltaR_vetol_j", 250, 0, 5.0);
      _h_cosThetaStarTauDecay = bookHistogram1D("cosThetaStar_TauDecay", 100, -1., 1.);
      
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      idxEvent++;
      /* Ad-hoc filter for debugging */
      //      if (idxEvent != 5 && idxEvent != 7 && idxEvent != 34 && idxEvent != 35 && idxEvent != 46 && idxEvent != 50 &&
      //	  idxEvent != 18 && idxEvent != 21 && idxEvent != 30) vetoEvent;
      //if (idxEvent != 13342) vetoEvent;
      /* End ad-hoc filter */
      MSG_DEBUG("Processing " << idxEvent << " event.");
      const double weight = event.weight();

      for (int ps = ps_PreFilter; ps <= ps_NCuts; ps++) {
        PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
        _passCuts[idx]=false;
      }

      cutFlow(ps_PreFilter, weight, cl_NChannels);
      _passCuts[ps_PreFilter] = true;

      //first clear global containers
      _vbs_lep.clear();
      _vbs_jets.clear();
      
      //evaluate needed projections (they have to be defined in init() first)
      const LeptonClusters& dressedLeptons = applyProjection<LeptonClusters>(event, "DressedLeptons");
      const FastJets& jetsProjection = applyProjection<FastJets>(event, "Jets");
      const Jets& jets = jetsProjection.jetsByPt(10.*GeV); //down to 10 GeV
      const UnstableFinalState& unstableP = applyProjection<UnstableFinalState>(event, "UnstableParticles");
      const ParticleVector& neutrinos = (applyProjection<IdentifiedFinalState>(event, "Neutrinos")).particlesByPt();


      //-- Evaluate selections cuts
      MSG_DEBUG("Evaluating selection cuts");
      _passCuts[ps_All] = true;

      MSG_DEBUG("Leptons");
      vector<Particle> _looseLeptons; //keep track of looser leptons as well
      //- select-then-dress -
      //sort the dressed leptons by pt of their constituent lepton (bare lepton pt)
      vector<ClusteredLepton> sortedClusteredLeptons = dressedLeptons.clusteredLeptons();
      if (sortedClusteredLeptons.size() > 1) //necessary for sorting
      std::sort(sortedClusteredLeptons.begin() , sortedClusteredLeptons.end() , ptbare_comp );

      //== veto event if any of the two leading leptons is from tau
      int nTau(0);
      int nPromptEle(0); //ele not from taus
      int nPromptMuon(0); //muons not from taus
      //--tau veto
      MSG_DEBUG("Vetoing taus");
      // check if one of the constituents of the first two (ordered in bare lepton pt) leptons has a tau ancestor 
      if ( sortedClusteredLeptons.size() >= _cutValues[ps_NLep] ) { //necessary for looping
	for (vector<ClusteredLepton>::const_iterator selLep = sortedClusteredLeptons.begin() ; 
	     selLep != sortedClusteredLeptons.begin() + (_cutValues[ps_NLep]) ; selLep++) {
	  if (selLep->constituentLepton().hasAncestor(15) || selLep->constituentLepton().hasAncestor(-15) ) {
	    nTau++;
	  } else {
	    if (abs(selLep->pdgId()) == 11) nPromptEle++;
	    else if (abs(selLep->pdgId()) == 13) nPromptMuon++;
	    else MSG_INFO("Analysis is failing assumptions (taus not decayed?) and needs debug.");
	  }
	}
	if (nTau < _cutValues[ps_TauVeto]) _passCuts[ps_TauVeto]=true;
	//	else _passCuts[ps_TauVeto]=false;      
      }
      else {
	//	MSG_DEBUG("vetoing event due to insufficient number of leptons in CBL container");
	_passCuts[ps_TauVeto]=true;
	// set the cut to true -> this event will be vetoed in the lepton selection cut anyway
      }
      //get explicitly tau ancestors for debug plots (selLepton, tauAncestor)
      /*
      std::vector<std::pair<GenParticle, GenParticle> > tauAncestorPairs;
      for (vector<ClusteredLepton>::const_iterator selLep = sortedClusteredLeptons.begin() ; 
	   selLep != sortedClusteredLeptons.begin() + (_cutValues[ps_NLep]) ; selLep++) {
	GenVertex* prodVtx = selLep->genParticle().production_vertex();
	int pdg_id = selLep->genParticle().pdg_id();
	if (prodVtx != 0) {
	  foreach (const GenParticle* ancestor, particles(prodVtx, HepMC::ancestors)) {
	    if (ancestor->pdg_id() == pdg_id) {
	      tauAncestorPairs.push_back(std::make_pair<GenParticle, GenParticle>(selLep->genParticle(), *ancestor));
	      break;
	    }
	  }
	}
      }
      */
      
      // loop over the first ps_NLep leptons in the bare pt sorted container 
      if ( sortedClusteredLeptons.size() >= _cutValues[ps_NLep] ) { //necessary for looping
	for ( vector<ClusteredLepton>::iterator lep = sortedClusteredLeptons.begin() ; 
	      lep != sortedClusteredLeptons.begin() + ( _cutValues[ps_NLep]  ) ; lep++ ) {
	  //select fiducial leptons
	  if (abs(lep->pdgId()) != 11 && abs(lep->pdgId()) != 13)
	    continue; //only electrons and muons
	  //applying acceptance requirements
	  if (lep->momentum().pT() > _cutValues_fidLep_pT && 
	      fabs(lep->momentum().eta()) < _cutValues_fidLep_eta) {
	    _vbs_lep.push_back(*lep);
	  } else { //looser (and not fiducial) leptons
	    // careful: they are not filled in agreement with analysis level -> don't use like this!
	    // ->cut currently disabled
	    if (abs(lep->pdgId()) == 11) {
	      if (lep->momentum().pT() > _cutValues_fidLepVeto_el_pT &&
		  fabs(lep->momentum().eta()) < _cutValues_fidLepVeto_Eta)
		_looseLeptons.push_back(*lep);
	    } else { //==13
	      if (lep->momentum().pT() > _cutValues_fidLepVeto_mu_pT &&
		  fabs(lep->momentum().eta()) < _cutValues_fidLepVeto_Eta)
		_looseLeptons.push_back(*lep);            
	    }
	  }
	}
      }
      else // sortedClusteredLeptons.size() < ps_NLep
	_passCuts[ps_NLep]=false;
      //check if all ps_NLep of the leptons pass the selection criteria
      if (_vbs_lep.size() >= static_cast<unsigned int>(_cutValues[ps_NLep])) 
        _passCuts[ps_NLep]=true; //requires exactly _cutValues[ps_NLep] fiducial leptons
      else _passCuts[ps_NLep]=false;


      if (_vbs_lep.size() >= 1) {
        if (_vbs_lep[0].momentum().pT() > _cutValues[ps_Lep1Pt])
          _passCuts[ps_Lep1Pt]=true;
      } 

      double mll(0.0);
      if (_vbs_lep.size() >= 2) {

        float minDRll=999.;
        for (vector<Particle>::iterator itLep1 = _vbs_lep.begin();
             itLep1 != _vbs_lep.end(); ++itLep1) 
          for (vector<Particle>::iterator itLep2 = itLep1;
               itLep2 != _vbs_lep.end(); ++itLep2) {
            if (itLep1 == itLep2) continue;
            float dR = deltaR(*itLep1, *itLep2);
            if (dR < minDRll) minDRll = dR;
          }        
        if (minDRll > _cutValues[ps_DRLep])
          _passCuts[ps_DRLep] = true;

        double ch_l0(0.0), ch_l1(0.0);
        ch_l0 = PID::charge(_vbs_lep[0].pdgId());
        ch_l1 = PID::charge(_vbs_lep[1].pdgId());

        if (fabs(_cutValues[ps_SS]) < 0.001 || _cutValues[ps_SS]*ch_l0*ch_l1 > 0) _passCuts[ps_SS] = true; //requires SS
      
	//get max and min mll between all distinct lepton pairs
	double mll_min(-1), mll_max(-1);
        for (vector<Particle>::iterator itLep1 = _vbs_lep.begin();
             itLep1 != _vbs_lep.end(); ++itLep1) 
          for (vector<Particle>::iterator itLep2 = itLep1;
               itLep2 != _vbs_lep.end(); ++itLep2) {
            if (itLep1 == itLep2) continue;
	    double mll_tmp = (itLep1->momentum()+itLep2->momentum()).mass();
	    if ((mll_min < 0) || (mll_tmp < mll_min)) mll_min = mll_tmp;
	    if (mll_tmp > mll_max) mll_max = mll_tmp;
	  }
	//for cutting mll > X, we use the minimum mll between leptons
        mll = mll_min;
        if (_cutValues[ps_Mll] < 0 || mll > _cutValues[ps_Mll])
          _passCuts[ps_Mll] = true;

        if (fabs(mll - 91.188*GeV) > _cutValues[ps_Zveto] ||
            abs(_vbs_lep[0].pdgId()) != 11 || abs(_vbs_lep[1].pdgId()) != 11)
          _passCuts[ps_Zveto] = true;

      } else {
        //no 2 fiducial leptons, still set to true for "N-1" plotting
        _passCuts[ps_Lep1Pt]=true;
        _passCuts[ps_DRLep] = true;
        _passCuts[ps_Mll] = true;
        _passCuts[ps_SS] = true;        
        _passCuts[ps_Zveto] = true;
      } 
      
      //jets
      MSG_DEBUG("Jets");
      foreach (const Jet& jet, jets) {
        //check eta,pT acceptance 
	if (jet.momentum().pT() < _cutValues_fidJets_pT) continue;
        if (fabs(jet.momentum().eta()) >= _cutValues_fidJets_eta) continue; 
	/* This is not needed since prompt electrons are already removed.
        //remove jets overlapping with electron with pT > 7 GeV and |eta| < 2.5
        float minDR=999.;
	foreach (const Particle &lep, _vbs_lep) {  //use only selected and dressed leptons
	  if (abs(lep.pdgId()) != 11) continue; //only electrons
	  if (deltaR(lep, jet) < minDR) minDR = deltaR(lep, jet);
        }
	_h_deltaR_ej->fill(minDR, weight);
        if (minDR < _cutValues_fidJets_eleOverlap_dR) continue;
	*/
        _vbs_jets.push_back(jet);
      }

      //before veoting on third lepton require it to be away from jets
      vector<Particle>::iterator itLooseLep = _looseLeptons.begin();
      while (itLooseLep != _looseLeptons.end()) {
	float minDRlj=999.;
	//	foreach (const Jet& jet, _vbs_jets) {
	foreach (const Jet& jet, _vbs_jets) {
	  float dR = deltaR(jet, *itLooseLep);
	  if (dR < minDRlj) minDRlj = dR;
	}
	_h_deltaR_vetol_j->fill(minDRlj, weight);
	if (minDRlj < _cutValues_fidLepVeto_dRJets) {
	  //do not consider this lepton for veto
	  itLooseLep = _looseLeptons.erase(itLooseLep);
	} else {
	  //go to the next loose lepton
	  ++itLooseLep;
	}
      }
      if (static_cast<int>(_cutValues[ps_LepVeto]) <= 0 ||
          _looseLeptons.size() < _cutValues[ps_LepVeto])
        _passCuts[ps_LepVeto] = true;

      //now jet cuts
      if (_vbs_jets.size() >= static_cast<unsigned int>(_cutValues[ps_NJets])) 
        _passCuts[ps_NJets]=true; // at least _cutValues[ps_NJets] fiducial jets

      double mjj(0.0);
      double dRjj(0.0);
      double etaP(0.);
      double lepCentrality(5.9); //last-bin
      double deltaY(0.0);
      int nBJets(0);
      float minDRLepJets=999.;
      if (_vbs_jets.size() >= 2) {
        foreach (const Jet& jet, _vbs_jets) {
          foreach (const Particle &lep, _vbs_lep) {
            float dR = deltaR(lep,jet);
            if (dR < minDRLepJets) minDRLepJets = dR;
          }        
        }
        if (minDRLepJets > _cutValues[ps_DRJetsLep])
          _passCuts[ps_DRJetsLep] = true;                
        
        deltaY = _vbs_jets[0].momentum().rapidity() - _vbs_jets[1].momentum().rapidity();
        if (fabs(deltaY) > _cutValues[ps_DeltaY])
          _passCuts[ps_DeltaY] = true;        

        mjj = (_vbs_jets[0].momentum() + _vbs_jets[1].momentum()).mass();
	if (mjj > _cutValues[ps_Mjj]) _passCuts[ps_Mjj]=true; 

	dRjj = deltaR(_vbs_jets[0].momentum(), _vbs_jets[1].momentum());
	if (dRjj > _cutValues[ps_DRjj] ) _passCuts[ps_DRjj]=true ;

        etaP = _vbs_jets[0].momentum().eta() * _vbs_jets[1].momentum().eta();
        if (_vbs_lep.size() >= 2) {
          lepCentrality = min(min(_vbs_lep[0].momentum().eta(),_vbs_lep[1].momentum().eta()) - 
                              min(_vbs_jets[0].momentum().eta(),_vbs_jets[1].momentum().eta()),
                              max(_vbs_jets[0].momentum().eta(),_vbs_jets[1].momentum().eta()) - 
                              max(_vbs_lep[0].momentum().eta(),_vbs_lep[1].momentum().eta()));        
        }

	//Get b-hadrons list (note: unstableP final state has already a 5 GeV cut)
	vector<Particle> B_hadrons;
	foreach (const Particle& p, unstableP.particles()) {
	  if ( !(Rivet::PID::isHadron( p.pdgId() ) && Rivet::PID::hasBottom( p.pdgId() )) ) continue;
	  if(p.momentum().perp()*GeV < 5) continue;
	  if(fabs(p.momentum().eta()) > 2.5) continue;
	  B_hadrons.push_back(p);
	}

	// For each of the good jets, check whether any are b-jets
	foreach(const Jet& j, _vbs_jets) {
	  if (fabs(j.momentum().eta()) > 2.5) continue; //outside acceptance of b-tagging
	  bool isbJet=false;
	  foreach(Particle& b, B_hadrons) {
	    double hadron_jet_dR = deltaR(j.momentum(), b.momentum());
	    if(hadron_jet_dR < 0.3) {isbJet = true; break;}
	  }
	  if (isbJet) nBJets++;
	}
	if (nBJets >= _cutValues[ps_BVeto] && _cutValues[ps_BVeto] >= 0) _passCuts[ps_BVeto] = false;
	else _passCuts[ps_BVeto]=true;
      } else {
        //still set to true other cuts for plotting, if necessary
        _passCuts[ps_Mjj]=true;
        _passCuts[ps_DRJetsLep]=true;
        _passCuts[ps_DeltaY]=true;
	_passCuts[ps_BVeto]=true;
	_passCuts[ps_DRjj]=true;
      }                           

      MSG_DEBUG("Met");
      const MissingMomentum& vismom = applyProjection<MissingMomentum>(event, "MissingET");
      if (vismom.vectorEt().mod() > _cutValues[ps_Met])
        _passCuts[ps_Met] = true;

      _passCuts[ps_NCuts] = true; //end evaluating cuts

      //=== Determine the channel based on selected leptons (and taus) 
      //    - works only for ps_NLep == 2; can be extended if necessary
      //    - use nTau, nPromptEle, nPromptMuon variables
      channelList lep_ch = cl_NChannels; 
      if (_cutValues[ps_NLep] == 2) {
	if (nPromptEle == 2) {
	  lep_ch = cl_ee;
	} else if (nPromptMuon == 2) {
	  lep_ch = cl_mm;
	} else if (nPromptEle == 1 && nPromptMuon == 1) {
	  lep_ch = cl_em;
	} else if (nPromptEle == 1 && nTau == 1) {
	  lep_ch = cl_et;
	} else if (nPromptMuon == 1 && nTau == 1) {
	  lep_ch = cl_mt;
	} else if (nTau == 2) {
	  lep_ch = cl_tt;
	}
      }
      //      MSG_DEBUG("Channel selections; nPromptElectrons="<<nPromptEle << ", nPromptMuons=" << nPromptMuon <<", nTaus="<< nTau << "; channel=" << getChannelName(lep_ch));

      //=== Fill N-1 plots
      MSG_DEBUG("Filling N-1 plots");
      //leptons
      if (checkAllBut(ps_NLep)) {
        int idxL=0;
	//        foreach (const Particle& lep, chLep.chargedLeptons()) {
	foreach (const ClusteredLepton& lep, dressedLeptons.clusteredLeptons()) { //use dressed leptons
          //fill eta/momentum of first two leptons
          idxL++;
          if (idxL == 1) {
            _h_Nm1_lep1_pt->fill(lep.momentum().pT()*GeV, weight);          
            _h_Nm1_lep1_eta->fill(lep.momentum().eta(), weight);          
          } else if (idxL == 2) {
            _h_Nm1_lep2_pt->fill(lep.momentum().pT()*GeV, weight);
            _h_Nm1_lep2_eta->fill(lep.momentum().eta(), weight);
            break; //no more
          }
        }    
      }    
      
      if (checkAllBut(ps_Mll))
        _h_Nm1_mll->fill(mll, weight);

      if (checkAllBut(ps_Zveto))
        _h_Nm1_mll_Zveto->fill(mll, weight);

      //jets
      if (checkAllBut(ps_NJets)) {
        _h_Nm1_NJets->fill(_vbs_jets.size(), weight); //use fiducial jets for counting!
        int idxJ=0;
        foreach (const Jet& jet, jets) {
          idxJ++;
          if (idxJ==1) {
            _h_Nm1_jet1_pt->fill(jet.momentum().pT()*GeV, weight);
            _h_Nm1_jet1_eta->fill(jet.momentum().eta(), weight);
          } else if (idxJ==2) {
            _h_Nm1_jet2_pt->fill(jet.momentum().pT()*GeV, weight);
            _h_Nm1_jet2_eta->fill(jet.momentum().eta(), weight);
            break;
          }
        }
      }

      if (checkAllBut(ps_DeltaY)) 
        _h_Nm1_deltaY->fill(fabs(_vbs_jets[0].momentum().rapidity() - _vbs_jets[1].momentum().rapidity()), weight);

      if (checkAllBut(ps_Mjj)) _h_Nm1_mjj->fill(mjj, weight);

      //met
      if (checkAllBut(ps_Met)) _h_Nm1_missingET->fill(vismom.vectorEt().mod(), weight);

      if (checkAllBut(ps_BVeto)) _h_Nm1_BVeto->fill(nBJets, weight);

      //debug: print selections
      printPassCuts();

      /* DEBUG events passing this cut */
      /*
      float tmp_key=0.0;
      //get key summing up all un-dressed leptons phi
      foreach (const Particle& lep, chLep.chargedLeptons()) { 
	tmp_key += lep.momentum().phi();
      }

      bool debug_for_cuts=(_passCuts[ps_TauVeto] && _passCuts[ps_NLep] && _passCuts[ps_Lep1Pt] &&
			   _passCuts[ps_DRLep] && _passCuts[ps_LepVeto] && _passCuts[ps_Mll] &&
			   _passCuts[ps_SS] && _passCuts[ps_Zveto] && _passCuts[ps_NJets]);
      bool debug_for_key = false; //(tmp_key == 10.6212);
      if (debug_for_cuts || debug_for_key) {
      MSG_INFO("====================");
      MSG_INFO("EventNumber = " << idxEvent << ", debug_for_key = " << debug_for_key << " debug_for_cuts = " << debug_for_cuts);
      MSG_INFO("key = " << tmp_key << " -- DR(l,j),MET,Mjj,Dy " 
	       << _passCuts[ps_DRJetsLep] << " "<< _passCuts[ps_Met] << " "<< _passCuts[ps_Mjj] << " "<< _passCuts[ps_DeltaY]);
      MSG_INFO("n leptons = " << chLep.chargedLeptons().size());
      MSG_INFO("n jets = " << jets.size());
      MSG_INFO("MET pT = " << vismom.vectorEt().mod() << ", phi = " << vismom.vectorEt().phi());
      MSG_INFO("DR(l,jet) = " << minDRLepJets);
      MSG_INFO("Mjj = " << mjj);
      MSG_INFO("Dy(jj) = " << deltaY);
      int tmp_idx=0;
      foreach (const ClusteredLepton& lep, dressedLeptons.clusteredLeptons()) { //use dressed leptons      
	MSG_INFO("lepton " << tmp_idx << ", PDGID=" << lep.pdgId());
	MSG_INFO(" pT = (dressed) " << lep.momentum().pT() << " (undressed) " << lep.constituentLepton().momentum().pT());
	MSG_INFO(" eta = (dressed)" << lep.momentum().eta() << " (undressed) " << lep.constituentLepton().momentum().eta());
	MSG_INFO(" phi = (dressed)" << lep.momentum().phi() << " (undressed) " << lep.constituentLepton().momentum().phi());
	tmp_idx++;
      }
      tmp_idx=0;
      foreach (const Jet& jet, jets) {
	MSG_INFO("jet " << tmp_idx);
	MSG_INFO(" pT = " << jet.momentum().pT());
	MSG_INFO(" eta = " << jet.momentum().eta());
	MSG_INFO(" phi = " << jet.momentum().phi());
	MSG_INFO(" M = " << jet.momentum().mass());
	tmp_idx++;
      }      
      }
      */
      /* END DEBUG */


      //=== Apply selections
      cutFlow(ps_All, weight, lep_ch);
      if (!_passCuts[ps_TauVeto]) vetoEvent; 
      cutFlow(ps_TauVeto, weight, lep_ch);
      if (!_passCuts[ps_NLep]) vetoEvent;
      cutFlow(ps_NLep, weight, lep_ch);
      if (!_passCuts[ps_Lep1Pt]) vetoEvent;
      cutFlow(ps_Lep1Pt, weight, lep_ch);
      if (!_passCuts[ps_DRLep]) vetoEvent;
      cutFlow(ps_DRLep, weight, lep_ch);
      if (!_passCuts[ps_LepVeto]) vetoEvent;
      cutFlow(ps_LepVeto, weight, lep_ch);
      if (!_passCuts[ps_Mll]) vetoEvent;
      cutFlow(ps_Mll, weight, lep_ch); 
      if (!_passCuts[ps_SS]) vetoEvent;
      cutFlow(ps_SS, weight, lep_ch);
      if (!_passCuts[ps_Zveto]) vetoEvent;
      cutFlow(ps_Zveto, weight, lep_ch);
      if (!_passCuts[ps_NJets]) vetoEvent;
      cutFlow(ps_NJets, weight, lep_ch);
      if (!_passCuts[ps_DRJetsLep]) vetoEvent;
      cutFlow(ps_DRJetsLep, weight, lep_ch);
      if (!_passCuts[ps_Met]) vetoEvent;
      cutFlow(ps_Met, weight, lep_ch);
      if (!_passCuts[ps_BVeto]) vetoEvent;
      cutFlow(ps_BVeto, weight, lep_ch);
      if (!_passCuts[ps_DRjj]) vetoEvent;
      cutFlow(ps_DRjj, weight, lep_ch);
      if (!_passCuts[ps_Mjj]) vetoEvent;
      cutFlow(ps_Mjj, weight, lep_ch);
      if (!_passCuts[ps_DeltaY]) vetoEvent;
      cutFlow(ps_DeltaY, weight, lep_ch);

      //loose fiducial selection
      cutFlow(ps_NCuts, weight, lep_ch); //for easy counting

      //== Fill plots after selections
      _h_lep1_pt->fill(_vbs_lep[0].momentum().pT()*GeV, weight);
      _h_lep2_pt->fill(_vbs_lep[1].momentum().pT()*GeV, weight);
      _h_lep1_eta->fill(_vbs_lep[0].momentum().eta(), weight);
      _h_lep2_eta->fill(_vbs_lep[1].momentum().eta(), weight);          
      _h_mll->fill(mll, weight);
      _h_lep_deltaEta->fill(_vbs_lep[0].momentum().eta() - _vbs_lep[1].momentum().eta(), weight);
      _h_lep_centrality->fill(lepCentrality, weight);
      _h_NJets->fill(_vbs_jets.size(), weight);
      _h_jet1_pt->fill(_vbs_jets[0].momentum().pT()*GeV, weight);
      _h_jet2_pt->fill(_vbs_jets[1].momentum().pT()*GeV, weight);
      _h_jet1_eta->fill(_vbs_jets[0].momentum().eta(), weight);
      _h_jet2_eta->fill(_vbs_jets[1].momentum().eta(), weight);
      _h_mjj->fill(mjj, weight);
      _h_mjj_low->fill(mjj, weight);
      _h_deltaPhi_jj->fill(deltaPhi(_vbs_jets[0].momentum().phi(),_vbs_jets[1].momentum().phi()), weight);
      _h_deltaY_jj->fill(fabs(_vbs_jets[0].momentum().rapidity() - _vbs_jets[1].momentum().rapidity()), weight);
      _h_deltaR_jj->fill(deltaR(_vbs_jets[0], _vbs_jets[1]), weight);
      _h_deltaPt_jj->fill(_vbs_jets[0].momentum().pT()*GeV - _vbs_jets[1].momentum().pT()*GeV, weight);

      double etaStar3;
      etaStar3=-999.;
      if (_vbs_jets.size() > 2) {
        etaStar3 = _vbs_jets[2].momentum().eta() - (_vbs_jets[0].momentum().eta() + _vbs_jets[1].momentum().eta())/2;
        _h_etaStar3->fill(etaStar3, weight);
      }

      // This is because we're interested in looking at 3rd jet veto
      if (_vbs_jets.size() > 2) {
        _h_jet3_pt->fill(_vbs_jets[2].momentum().pT(), weight);
        _h_jet3_eta->fill(_vbs_jets[2].momentum().eta(), weight);
      }

      _h_oppHem->fill(etaP, weight);
      
      //find W mass
      vector<Particle> lepVec = _vbs_lep; //copy of leptons
      //vector<pair<Particle, Particle> > Wcands;
      foreach (const Particle &nu, neutrinos) {
        if (lepVec.size() == 0) break;
        int targetPdgId = nu.pdgId() > 0 ? -(nu.pdgId()-1) : (-nu.pdgId()-1);
        foreach (Particle &lep, lepVec) {
          if (lep.pdgId() == targetPdgId) {
            //w candidate
            //Wcands.push_back(make_pair<Particle, Particle>(lep, nu));
            //MSG_DEBUG("W candidate (pdgId, pT): (" 
            //          << lep.pdgId() << ", " << lep.momentum().pT() << "), ("
            //          << nu.pdgId() << ", " << nu.momentum().pT() << ")");
            double wmass = (lep.momentum()+nu.momentum()).mass();
            _h_Wmass->fill(wmass*GeV, weight);
          }
        }
      }

      //Missing Momentum
      _h_missingET->fill(vismom.vectorEt().mod(), weight);      

      //Tau polarization studies
      /*
      std::vector<std::pair<GenParticle, GenParticle> >::iterator tauAPit;
      for (tauAPit = tauAncestorPairs.begin(); tauAPit != tauAncestorPairs.end(); ++tauAPit) {
	//get cos of the angle between tau decay product in the tau rest frame and the tau momentum
	FourMomentum tauMom = tauAPit->first.momentum();
	LorentzTransform tauBoost(-tauMom.boostVector());
	FourMomentum lepInTauRestFrame(tauBoost.transform(tauAPit->second.momentum()));
	float thetaStar = lepInTauRestFrame.angle(tauMom);
	_h_cosThetaStarTauDecay->fill(cos(thetaStar), weight);
      }
      */
	
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      /// Normalise, scale and otherwise manipulate histograms here
      if (_useShapeComparison) {
        myNormalize(_h_Nm1_lep1_pt);
        myNormalize(_h_Nm1_lep2_pt);
        myNormalize(_h_Nm1_lep1_eta);
        myNormalize(_h_Nm1_lep2_eta);
        myNormalize(_h_Nm1_mll);
        myNormalize(_h_Nm1_mll_Zveto);
        myNormalize(_h_Nm1_NJets);
        myNormalize(_h_Nm1_jet1_pt);
        myNormalize(_h_Nm1_jet2_pt);
        myNormalize(_h_Nm1_jet1_eta);
        myNormalize(_h_Nm1_jet2_eta);
        myNormalize(_h_Nm1_missingET);
        myNormalize(_h_Nm1_deltaY);
        myNormalize(_h_Nm1_mjj);
        myNormalize(_h_lep1_pt);
        myNormalize(_h_lep2_pt);
        myNormalize(_h_lep1_eta);
        myNormalize(_h_lep2_eta);
        myNormalize(_h_mll);
        myNormalize(_h_jet1_pt);
        myNormalize(_h_jet2_pt);
        myNormalize(_h_jet3_pt);
        myNormalize(_h_jet1_eta);
        myNormalize(_h_jet2_eta);
        myNormalize(_h_jet3_eta);
        myNormalize(_h_lep_centrality);
        myNormalize(_h_lep_deltaEta);
        myNormalize(_h_mjj);
        myNormalize(_h_mjj_low);
        myNormalize(_h_oppHem);
        myNormalize(_h_deltaPhi_jj);
        myNormalize(_h_deltaY_jj);
        myNormalize(_h_deltaR_jj);
        myNormalize(_h_deltaPt_jj);
        myNormalize(_h_NJets);
        myNormalize(_h_etaStar3);
        myNormalize(_h_Wmass);
        myNormalize(_h_missingET);
	myNormalize(_h_deltaR_ej);
	myNormalize(_h_deltaR_vetol_j);
	myNormalize(_h_cosThetaStarTauDecay);
      } else {
        scale(_h_Nm1_lep1_pt, crossSection()/sumOfWeights());
        scale(_h_Nm1_lep2_pt, crossSection()/sumOfWeights());
        scale(_h_Nm1_lep1_eta, crossSection()/sumOfWeights());
        scale(_h_Nm1_lep2_eta, crossSection()/sumOfWeights());
        scale(_h_Nm1_mll, crossSection()/sumOfWeights());
        scale(_h_Nm1_mll_Zveto, crossSection()/sumOfWeights());        
        scale(_h_Nm1_NJets, crossSection()/sumOfWeights());
        scale(_h_Nm1_jet1_pt, crossSection()/sumOfWeights());
        scale(_h_Nm1_jet2_pt, crossSection()/sumOfWeights());
        scale(_h_Nm1_jet1_eta, crossSection()/sumOfWeights());
        scale(_h_Nm1_jet2_eta, crossSection()/sumOfWeights());
        scale(_h_Nm1_missingET, crossSection()/sumOfWeights());
        scale(_h_Nm1_deltaY, crossSection()/sumOfWeights());
        scale(_h_Nm1_mjj, crossSection()/sumOfWeights());
        scale(_h_lep1_pt, crossSection()/sumOfWeights());
        scale(_h_lep2_pt, crossSection()/sumOfWeights());
        scale(_h_lep1_eta, crossSection()/sumOfWeights());
        scale(_h_lep2_eta, crossSection()/sumOfWeights());
        scale(_h_mll, crossSection()/sumOfWeights());
        scale(_h_lep_centrality, crossSection()/sumOfWeights());
        scale(_h_lep_deltaEta, crossSection()/sumOfWeights());
        scale(_h_jet1_pt, crossSection()/sumOfWeights());
        scale(_h_jet2_pt, crossSection()/sumOfWeights());
        scale(_h_jet3_pt, crossSection()/sumOfWeights());
        scale(_h_jet1_eta, crossSection()/sumOfWeights());
        scale(_h_jet2_eta, crossSection()/sumOfWeights());
        scale(_h_jet3_eta, crossSection()/sumOfWeights());
        scale(_h_mjj, crossSection()/sumOfWeights());
        scale(_h_mjj_low, crossSection()/sumOfWeights());
        scale(_h_oppHem, crossSection()/sumOfWeights());
        scale(_h_deltaPhi_jj, crossSection()/sumOfWeights());
        scale(_h_deltaY_jj, crossSection()/sumOfWeights());
        scale(_h_deltaR_jj, crossSection()/sumOfWeights());
        scale(_h_deltaPt_jj, crossSection()/sumOfWeights());
        scale(_h_NJets, crossSection()/sumOfWeights());
        scale(_h_etaStar3, crossSection()/sumOfWeights());
        scale(_h_Wmass, crossSection()/sumOfWeights());        
        scale(_h_missingET, crossSection()/sumOfWeights());
	scale(_h_deltaR_ej, crossSection()/sumOfWeights());
	scale(_h_deltaR_vetol_j, crossSection()/sumOfWeights());
	scale(_h_cosThetaStarTauDecay, crossSection()/sumOfWeights());
      }

      //Print counters
      MSG_INFO("Printing cutflow:");
      MSG_INFO("Fiducial selection = " << _vbsPhaseSpace);
      for (int ps = ps_PreFilter; ps < ps_NCuts; ps++) {
        PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
        string selName = getCutName(idx);
        double eff, eff_err;
        PhaseSpaceSelections denIdx = ps_All;
        double xsec = crossSection();//in pb
        xsec = xsec * 1E3; //move to fb
        if (ps == ps_PreFilter || ps == ps_All) {
          denIdx = ps_PreFilter;
        } else {
          //efficiency defined wrt filtered, correct xsec
          xsec = xsec * _fiducialWeights[ps_All] / _fiducialWeights[ps_PreFilter]; 
        }
        calculateEfficiency(eff, eff_err, 
                            _fiducialWeights[idx], _fiducialWeights[denIdx],
                            _fiducialWeights2[idx], _fiducialWeights2[denIdx]);
        MSG_INFO(" " << selName);
        if (selName.find("disabled") == std::string::npos) {
          MSG_INFO("   Raw events: " << _processedEvents[idx]);
          MSG_INFO("   Weighted events: "<< _fiducialWeights[idx]);
          MSG_INFO("   Efficiency = " << eff << " +/- " << eff_err);        
          MSG_INFO("   x-sec = " << xsec * eff << " +/- " << xsec*eff_err << " fb");
        }
      }
      //print only looseMjj and deltaY counters split by channel      
      /*
      if (not _adaptCutsForWZjj) {
      MSG_INFO("Final numbers split by channel."); 
      for (int cl = cl_ee; cl < cl_NChannels; ++cl) {
	//set denominator for initial cut
	_fiducialWeightsByChannel[cl][ps_PreFilter] = _fiducialWeights[ps_PreFilter];
	_fiducialWeights2ByChannel[cl][ps_PreFilter] = _fiducialWeights2[ps_PreFilter];
	string channelName = getChannelName(cl);
	MSG_INFO("Final counts for channel: " << channelName);
	for (int ps = ps_PreFilter; ps < ps_NCuts; ++ps) {
	  if (ps != ps_All && ps != ps_Mjj && ps != ps_DeltaY) continue;
	  PhaseSpaceSelections psE = static_cast<PhaseSpaceSelections>(ps);
	  double eff, eff_err;
	  PhaseSpaceSelections denIdx;
	  if (ps == ps_All) denIdx = ps_PreFilter;
	  else denIdx = ps_All;
	  double xsec = crossSection();//in pb
	  xsec = xsec * 1E3; //move to fb
	  string selName = getCutName(psE);

	  calculateEfficiency(eff, eff_err,
			      _fiducialWeightsByChannel[cl][psE], _fiducialWeightsByChannel[cl][denIdx],
			      _fiducialWeights2ByChannel[cl][psE], _fiducialWeights2ByChannel[cl][denIdx]);
	  MSG_INFO(" " << selName << "( " << channelName << " )");
	  if (selName.find("disabled") == std::string::npos) {
	    MSG_INFO("   Weighted events: "<< _fiducialWeightsByChannel[cl][psE]);
	    MSG_INFO("   Efficiency = " << eff << " +/- " << eff_err);        
	    MSG_INFO("   x-sec = " << xsec * eff << " +/- " << xsec*eff_err << " fb");
	  }
	}
      }
      }
      */
    }

    //@}

    /// @name Analysis helpers
    //@{

    string getCutName(PhaseSpaceSelections ps) {
      ostringstream outStr; //string stream, if needed
      switch (ps) {
      case ps_PreFilter:
        return string("Pre-Filter");
      case ps_All:
        return string("All");
      case ps_TauVeto: {
        outStr << "Tau Veto ";
        outStr << " n_tau leptons < " << _cutValues[ps_TauVeto];
        return outStr.str();
      }
      case ps_NLep: {
        outStr << "Fiducial Leptons ";
        outStr << "(pT > " << _cutValues_fidLep_pT << ", |eta| < " << _cutValues_fidLep_eta << ")";
        outStr << " >= " << _cutValues[ps_NLep];
        return outStr.str();
      }
      case ps_Lep1Pt: {
        outStr << "Leading lepton pT ";
        if (_cutValues[ps_Lep1Pt] > _cutValues_fidLep_pT)
          outStr << "> " << _cutValues[ps_Lep1Pt];
        else outStr << "disabled";
        return outStr.str();
      }
      case ps_DRLep: {
        outStr << "DR(l,l) ";
        if (_cutValues[ps_DRLep] >= 0)
          outStr << "> " << _cutValues[ps_DRLep];
        else outStr << "disabled.";
        return outStr.str();
      }
      case ps_LepVeto: {
        if (_cutValues[ps_LepVeto] > 0) {
          outStr << "Veto events with >= " << _cutValues[ps_LepVeto];
          outStr << " leptons (";
          outStr << "pT(el) > " << _cutValues_fidLepVeto_el_pT;
          outStr << ", pT(mu) > " << _cutValues_fidLepVeto_mu_pT;
          outStr << ", |eta| < " << _cutValues_fidLepVeto_Eta 
		 << ", minDR(l,j) > " << _cutValues_fidLepVeto_dRJets
		 << ")";
        } else {
          outStr << "Cut on additional leptons disabled.";
        }
        return outStr.str();
      }
      case ps_Mll: {
        outStr << "m(ll)";
        if (_cutValues[ps_Mll] > 0)
          outStr << " > " << _cutValues[ps_Mll];
        else 
          outStr << " disabled.";
        return outStr.str();
      }
      case ps_SS:
        if (_cutValues[ps_SS] > 0)
          return string("Same-Sign");
        else 
          return string("Opposite-Sign");
      case ps_Zveto: {
        outStr << "|m(ee) - m(Z)| ";
        if (_cutValues[ps_Zveto] > 0)
          outStr << "> " << _cutValues[ps_Zveto];
        else
          outStr << "disabled";
        return outStr.str();
      }
      case ps_NJets: {
        outStr << "N_fid(jets) ";
        outStr << "(pT > " << _cutValues_fidJets_pT << ", |eta| < " << _cutValues_fidJets_eta 
	       << ", dR(ele) > " << _cutValues_fidJets_eleOverlap_dR
	       << ")";        
        outStr << " >= " << _cutValues[ps_NJets];
        return outStr.str();
      }
      case ps_DRJetsLep: {
        outStr << "min. Delta R(l,j) ";
        if (_cutValues[ps_DRJetsLep] >= 0) 
          outStr << "> " << _cutValues[ps_DRJetsLep];
        else
          outStr << "disabled";
        return outStr.str();
      }
      case ps_Met: {
        outStr << "Missing ET ";
        if (_cutValues[ps_Met] > 0)
          outStr << "> " << _cutValues[ps_Met];
        else outStr << "disabled";
        return outStr.str();
      }
      case ps_BVeto: {
	outStr << "B-Veto";
	if (_cutValues[ps_BVeto] >= 0)
	  outStr << " (N b-jets < " << _cutValues[ps_BVeto] <<")";
	else outStr << "disabled";
	return outStr.str();
      }
      case ps_DRjj: {
	outStr << "DR(jj)";
	if (_cutValues[ps_DRjj] > 0)
	  outStr << " > " << _cutValues[ps_DRjj];
	return outStr.str();
      }
      case ps_Mjj: {
        outStr << "m(jj)";
        if (_cutValues[ps_Mjj] > 0)
          outStr << " > " << _cutValues[ps_Mjj];
        return outStr.str();
      }
      case ps_DeltaY: {
        outStr << "Delta Y(j,j) ";
        if (_cutValues[ps_DeltaY] > 0) 
          outStr << "> " << _cutValues[ps_DeltaY];
        else outStr << "disabled";
        return outStr.str();
      }
      case ps_NCuts:
        return string("Total Fiducial");
      }
      return string("UNKNOWN");
    }

    string getChannelName(int cl) {
      switch (cl) {
      case cl_ee:
	return string("ee");
      case cl_em:
	return string("e-mu");
      case cl_mm:
	return string("mu-mu");
      case cl_et:
	return string("e-tau");
      case cl_mt:
	return string("mu-tau");
      case cl_tt:
	return string("tau-tau");
      }
      return string("INVALID");
    }

    ///Increment cutflow
    void cutFlow(PhaseSpaceSelections ps, double weight, int channel) {
      MSG_DEBUG("Passing cut: " << getCutName(ps));
      _processedEvents[ps]++;
      _fiducialWeights[ps]+= weight;
      _fiducialWeights2[ps]+= weight*weight;      
      if (channel != cl_NChannels) {
	_fiducialWeightsByChannel[channel][ps] += weight;
	_fiducialWeights2ByChannel[channel][ps] += weight*weight;
      }
    }

    ///Check all cuts but one
    bool checkAllBut(PhaseSpaceSelections ps) {
      bool ret(true);
      for (int i = ps_PreFilter; i <= ps_NCuts; i++) {
        if (ps == i) continue; //do not check this
        if (!_passCuts[i]) {
          ret=false;
          break;
        }
      }
      return ret;
    }

    ///Print cut pass (for debug)
    void printPassCuts() {
      for (int i = ps_PreFilter; i <= ps_NCuts; i++) {
        PhaseSpaceSelections ps = static_cast<PhaseSpaceSelections>(i);
        MSG_DEBUG(getCutName(ps) << ": " << _passCuts[ps]);
      }
    }

    /** Calculate efficiency and errors.
     * @param eff return efficiency value
     * @param eff_err return efficiency error
     * @param w_sel sum of weights that pass selections
     * @param w_all sum of weight of all events
     * @param w2_sel sum of squared weights that pass selections
     * @param w2_all sum of squared weights of all events
     */
    void calculateEfficiency(double &eff, double &eff_err,
                             double w_sel, double w_all,
                             double w2_sel, double w2_all) {
      if (w_all != 0)
        eff = w_sel / w_all;
      else {
        MSG_INFO("Error calculating average. Zero denominator: " << w_sel << " / " << w_all << ". Forcing -1.");
        eff = eff_err = -1;
      }

      double tmp_w2_m(0.0); //sum of squared weights for events not passing selection
      double tmp_w_m(0.0); //sum of weights for events not passing selection
      tmp_w_m = w_all - w_sel;
      tmp_w2_m = w2_all - w2_sel;
      eff_err = sqrt(w2_sel*tmp_w_m*tmp_w_m + tmp_w2_m*w_sel*w_sel);
      eff_err = eff_err / (w_all*w_all);
            
    }

    void myNormalize(AIDA::IHistogram1D *h) {
      //takes into account bin widths
      normalize(h, h->axis().binWidth(0)); //assume equally-spaced bins
    }


    //@}

  private:

    // cut values
    float _cutValues_fidLep_pT; ///<leading lepton
    float _cutValues_fidLep_eta;
    float _cutValues_fidLepVeto_el_pT; ///<for third-lepton veto (electron)
    float _cutValues_fidLepVeto_mu_pT; ///<for third-lepton veto (muon)
    float _cutValues_fidLepVeto_Eta; ///<for third-lepton veto
    float _cutValues_fidLepVeto_dRJets; ///<min dR(l,j) to be considered a valid lepton veto
    float _cutValues_fidJets_pT;
    float _cutValues_fidJets_eta;
    float _cutValues_fidJets_eleOverlap_dR;
    map<PhaseSpaceSelections, float> _cutValues;

    /// event-level cuts bookeeping
    bool _passCuts[ps_NCuts+1];

    // Data members like post-cuts event weight counters go here    
    map<PhaseSpaceSelections, int> _processedEvents;
    map<PhaseSpaceSelections, double> _fiducialWeights;
    map<PhaseSpaceSelections, double> _fiducialWeights2;
    map<PhaseSpaceSelections, double> _fiducialWeightsByChannel[cl_NChannels];
    map<PhaseSpaceSelections, double> _fiducialWeights2ByChannel[cl_NChannels];


    vector<Particle> _vbs_lep;
    vector<Jet> _vbs_jets;   

  private:

    /// @name Histograms
    //@{

    //histograms for N-1 plots
    AIDA::IHistogram1D *_h_Nm1_lep1_pt, *_h_Nm1_lep2_pt;
    AIDA::IHistogram1D *_h_Nm1_lep1_eta, *_h_Nm1_lep2_eta;
    AIDA::IHistogram1D *_h_Nm1_mll;
    AIDA::IHistogram1D *_h_Nm1_mll_Zveto;
    AIDA::IHistogram1D *_h_Nm1_NJets;
    AIDA::IHistogram1D *_h_Nm1_jet1_pt, *_h_Nm1_jet2_pt;
    AIDA::IHistogram1D *_h_Nm1_jet1_eta, *_h_Nm1_jet2_eta;
    AIDA::IHistogram1D *_h_Nm1_missingET;
    AIDA::IHistogram1D *_h_Nm1_deltaY;
    AIDA::IHistogram1D *_h_Nm1_mjj;
    AIDA::IHistogram1D *_h_Nm1_BVeto;
    //histograms after selection
    AIDA::IHistogram1D *_h_lep1_pt, *_h_lep2_pt;
    AIDA::IHistogram1D *_h_mll;
    AIDA::IHistogram1D *_h_lep1_eta, *_h_lep2_eta;
    AIDA::IHistogram1D *_h_lep_centrality, *_h_lep_deltaEta;
    AIDA::IHistogram1D *_h_jet1_pt, *_h_jet2_pt, *_h_jet3_pt;
    AIDA::IHistogram1D *_h_jet1_eta, *_h_jet2_eta, *_h_jet3_eta;
    AIDA::IHistogram1D *_h_mjj, *_h_mjj_low, *_h_oppHem;
    AIDA::IHistogram1D *_h_deltaPhi_jj, *_h_deltaY_jj, *_h_deltaR_jj;
    AIDA::IHistogram1D *_h_deltaPt_jj, *_h_NJets;
    AIDA::IHistogram1D *_h_etaStar3;
    AIDA::IHistogram1D *_h_Wmass;
    AIDA::IHistogram1D *_h_missingET;
    //debug histograms for cut development
    AIDA::IHistogram1D *_h_deltaR_ej; ///< deltaR(electron, jet) before jet removal
    AIDA::IHistogram1D *_h_deltaR_vetol_j; ///< deltaR(3rd lepton, jet) before lepton removal
    AIDA::IHistogram1D *_h_cosThetaStarTauDecay; ///< study polarization for tau decays

    //@}

    int idxEvent;

  };



  // This global object acts as a hook for the plugin system
  AnalysisBuilder<MC_VBSVAL> plugin_MC_VBSVAL;


}
