//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file exampleB2a.cc
/// \brief Main program of the B2a example

//geant headers
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4TrajectoryContainer.hh"
#include "FTFP_BERT.hh"
#include "G4SystemOfUnits.hh"

//my headers
#include "BasicActionInitialization.hh" 
#include "BasicDetectorConstruction.hh"
#include "BasicPrimaryGeneratorAction.hh"
#include "BasicEventAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc, char *argv[])
{ 
  
  if(argc!=2){
    G4cout << "I need the energy of the gun.\n";
    return 0;
  }
  
  G4double pionEnergy;
  pionEnergy=atof(argv[1]);
    
  G4RunManager * runManager = new G4RunManager;
  
  // Detector construction
  runManager->SetUserInitialization(new BasicDetectorConstruction());
  
  // Output
  G4UImanager* UI = G4UImanager::GetUIpointer();
  UI->ApplyCommand("/tracking/verbose 0");

  // Physics list
  G4VModularPhysicsList* physicsList = new FTFP_BERT;
  runManager->SetUserInitialization(physicsList);
    
  // Primary generator action
  runManager->SetUserAction(new BasicPrimaryGeneratorAction(pionEnergy));
  
  // Set user action classes
  runManager->SetUserInitialization(new BasicActionInitialization());
   
  // Initialize G4 kernel
  runManager->Initialize();
  
  //Cause the run manager to generate a single event using the
  //primary generator action registered above.
  runManager->BeamOn(1e6);


  
  //After the run is complete, free up the memory used by run 
  //manager and return 
  delete runManager;  
  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
