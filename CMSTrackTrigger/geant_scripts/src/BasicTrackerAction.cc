//
 // ********************************************************************
 // * License and Disclaimer                                           *
 // *                                                                  *
 // * The  Geant4 software  is  copyright of the Copyright Holders  of *
 // * the Geant4 Collaboration.  It is provided  under  the terms  and *
 // * conditions of the Geant4 Software License,  included in the file *
 // * LICENSE and available at  http://cern.ch/geant4/license .  These *
 // * include a list of copyright holders.                             *
 // *                                                                  *
 // * Neither the authors of this software system, nor their employing *
 // * institutes,nor the agencies providing financial support for this *
 // * work  make  any representation or  warranty, express or implied, *
 // * regarding  this  software system or assume any liability for its *
 // * use.  Please see the license in the file  LICENSE  and URL above *
 // * for the full disclaimer and the limitation of liability.         *
 // *                                                                  *
 // * This  code  implementation is the result of  the  scientific and *
 // * technical work of the GEANT4 collaboration.                      *
 // * By using,  copying,  modifying or  distributing the software (or *
 // * any work based  on the software)  you  agree  to acknowledge its *
 // * use  in  resulting  scientific  publications,  and indicate your *
 // * acceptance of all terms of the Geant4 Software license.          *
 // ********************************************************************
 //
 //
 
#include "BasicTrackerAction.hh"
#include "G4DynamicParticle.hh"
  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 
 BasicTrackerAction::BasicTrackerAction()
 : G4UserTrackingAction()
 {}
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 BasicTrackerAction::~BasicTrackerAction()
 {}
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 void BasicTrackerAction::PreUserTrackingAction(const G4Track* track)
 {}
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 void BasicTrackerAction::PostUserTrackingAction(const G4Track* track)
 {
   G4int trackID, PDGcode; 
   G4ThreeVector trackPosition, trackmomentum;
   G4double trackEnergy;
   //G4DynamicParticle particleType;  
   
   trackID        = track -> GetTrackID();
   trackPosition  = track -> GetPosition();
   trackmomentum  = track -> GetMomentum();
   trackEnergy    = track -> GetKineticEnergy();
   PDGcode   = (track -> GetDynamicParticle())->GetPDGcode();
   
   //fix this, currently selecting all particles with mass above 500 MeV
   if(trackEnergy>1500){
   
    G4cout<< trackID << "\t";
    G4cout<< PDGcode << "\t";
    //G4cout<< trackPosition.x() << "\t";
    //G4cout<< trackPosition.y() << "\t";
    //G4cout<< trackPosition.z() << "\t";
    //G4cout<< trackEnergy << "\t";
    G4cout<< trackmomentum.x()/1000. << "\t";
    G4cout<< trackmomentum.y()/1000. << "\t";
    G4cout<< trackmomentum.z()/1000. << "\t";
    G4cout<< "\n";
   }
   
 }
 