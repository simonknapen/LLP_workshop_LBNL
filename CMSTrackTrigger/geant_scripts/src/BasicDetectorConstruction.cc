//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
 
#include "G4VUserDetectorConstruction.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "BasicDetectorConstruction.hh"
#include "G4PhysicalConstants.hh"

BasicDetectorConstruction::BasicDetectorConstruction()
{
}

BasicDetectorConstruction::~BasicDetectorConstruction()
{
}

G4VPhysicalVolume* BasicDetectorConstruction::Construct()
	{
		//Obtain pointer to NIST material manager
		G4NistManager* nist = G4NistManager::Instance();
		
		//Build materials for world and target
		G4Material* Si =  nist->FindOrBuildMaterial("G4_Si");
        // Vacuum
        G4double a;  // mass of a mole;
        G4double z;  // z=mean number of protons;
        G4double density;
        G4Material* vacuum = new G4Material("Galactic", z=1., a=1.01*g/mole,density= universe_mean_density,kStateGas, 2.73*kelvin, 3.e-18*pascal );
		//G4Material* air = nist->FindOrBuildMaterial("G4_AIR");

		//Define the world solid. The world will be 4cm x 10m x10m
		G4double world_sizeX=4*cm;
		G4double world_sizeY=10*m;
		G4double world_sizeZ=10*m;
		G4Box* solidWorld = 
			new G4Box("World",world_sizeX,world_sizeY,world_sizeZ);

		//Fill the world with air
		G4LogicalVolume* logicWorld = 
			new G4LogicalVolume(solidWorld, vacuum, "World");

		//Create the world physical volume. The world is the only
		//physical volume with no mother volume.
		G4VPhysicalVolume* physWorld = 
			new G4PVPlacement(0,                       //no rotation
								G4ThreeVector(0,0,0),       //at (0,0,0)
								logicWorld,            //its logical volume
								"World",               //its name
								0,                     //its mother  volume
								false,                 //no boolean operation
								0,                     //copy number
								true);			       //overlaps checking                     

		//Create  the shape of a target to fire particles at
		G4double target_sizeX=1*cm;
		G4double target_sizeY=10*m;
		G4double target_sizeZ=10*m;
		G4Box* solidTarget = 
			new G4Box("Target",target_sizeX, target_sizeY, target_sizeZ);

		//Create the target logical volume by
		//assigning the material of the target to be Si
		G4LogicalVolume* logicTarget = 
			new G4LogicalVolume(solidTarget, Si, "Target");

		//Create the target physical volume by placing it in the
		//"logicWorld" logical volume.
		 G4VPhysicalVolume* physTarget = new G4PVPlacement(
						0, 			//no rotation
						G4ThreeVector(0,0,0),	//at (0,0,0)
						logicTarget,		//its logical volume
						"Target",		//its name
						logicWorld,		//its mother  volume
						false,	                //no boolean operation
						0,			//copy number
						true);			//overlaps checking 
                    

		return physWorld;
	}
