#!/usr/bin/python

import sys
import glob
import os
import numpy as np
import math

def p(track):
    return np.linalg.norm(track[3:6])

def pT(track):
    return np.linalg.norm(track[3:5])
    
def lT(track):
    return np.linalg.norm(track[6:8])

def lz(track):
    return np.abs(track[8])
    
def eta(track):
    return np.arctanh(track[5]/np.linalg.norm(track[3:6]))

def ATLAScuts(tracks):# all truth-level
    if(pT(tracks[0])<10 or pT(tracks[1])<10):
        return False
    if(lT(tracks[0])>600):
        return False
    if(np.abs(eta(tracks[0]))>2.4 or np.abs(eta(tracks[1]))>2.4):
        return False
    return True


datadir="/home/data1/knapen/CMS_track_trigger/BtoKX/ATLAS/"
outdir="/home/data1/knapen/CMS_track_trigger/BtoKX/results/"
outfile=outdir+"BtoKX_efficiencies_ATLAS.txt"

filenames=glob.glob(datadir+'*txt')

masslist=list(dict.fromkeys([filename.split("_")[-3] for filename in filenames]))
ctaulist=list(dict.fromkeys([filename.split("_")[-2] for filename in filenames]))

print masslist
print ctaulist

results=[]

for m in masslist:
    for ctau in ctaulist:
#for m in ["1"]:
#    for ctau in ["0.1"]:
        table=np.loadtxt(datadir+"BtoKX_ATLAS\_"+m+"_"+ctau+"_1.txt")
        
        print "processing "+m+" "+ctau
        
        if(len(table)%2 !=0):
            print "error, uneven number of tracks"
            sys.exit()
    
        table=np.split(table,len(table)/2)
        
        fidlist=filter(ATLAScuts,table)
        eff=float(len(fidlist))/float(len(table))
        
        results.append([float(m),float(ctau),eff])

np.savetxt(outfile,results)





















