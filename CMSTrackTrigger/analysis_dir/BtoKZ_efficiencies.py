#!/usr/bin/python

import sys
import glob
import os
import numpy as np
import math


def dT(tracks):
    v = tracks[0][16:18]
    p = tracks[0][21:23]+tracks[1][21:23]
  
    impact = np.sqrt(np.dot(v,v) - np.dot(v,p)**2/np.dot(p,p))
    return impact

def radius(track):
    return np.sqrt(track[16]**2+track[17]**2)

def radiusTruth(track):
    return np.sqrt(track[13]**2+track[14]**2)
    
def pT(track):
    return np.linalg.norm(track[21:23])

def pTTruth(track):
    return np.linalg.norm(track[3:5])

def eta(track):
    return np.arctanh(track[5]/np.linalg.norm(track[3:6]))

def pTruth(track):
    return np.linalg.norm(track[3:6])

def cosalphaT(tracks):
    v = tracks[0][16:18]
    p = tracks[0][21:23]+tracks[1][21:23]
    
    cosangle = np.dot(v,p)/(np.linalg.norm(p)*np.linalg.norm(v))
    return cosangle

def cuts (tracks):
    if(not (np.isfinite(tracks[0][23]) and np.isfinite(tracks[1][23]))): #check if both tracks exist
        return False
    if(tracks[0][11]<5 and tracks[1][11]<5):  # nstubs
        return False
    if(np.abs(tracks[0][7])<0.1 or np.abs(tracks[1][7])<0.1):  # |d0|>1mm
        return False
    if(tracks[0][20]>1.0): # Delta_z
        return False
    if(tracks[0][19]>0.2): # Delta_xy
        return False
    if(cosalphaT(tracks)<0.9995): # alpha_T
        return False
    if(dT(tracks)>0.1): # d_T
        return False
    if(radius(tracks[0])<1.5): # cut on radius, to get rid of B mesons
        return False
    return True

def LHCbcuts(tracks):# all truth-level
    if(pTTruth(tracks[0])<0.5 or pTTruth(tracks[1])<0.5):
        return False
    if(pTruth(tracks[0])<10 or pTruth(tracks[1])<10):
        return False
    if (eta(tracks[0])>5 or eta(tracks[0])<2 or eta(tracks[1])>5 or eta(tracks[1])<2):
        return False
    if (radiusTruth(tracks[0])>2.2):
        return False
    return True

def ATLAScuts(tracks):# all truth-level
    #if(pTTruth(tracks[0])<10 or pTTruth(tracks[1])<10):
    #    return False
    if(radiusTruth(tracks[0])>600):
        return False
    if(np.abs(eta(tracks[0]))>1.05 or np.abs(eta(tracks[1]))>1.05):
        return False
    return True
    
def pTcuts(tracks,cut1,cut2):
    if(max(pT(tracks[0]),pT(tracks[1]))>cut1 and min(pT(tracks[0]),pT(tracks[1]))>cut2):
           return True
    else:
           return False
    

datadir="/home/data1/knapen/CMS_track_trigger/BtoKX/reco/"
outdir="/home/data1/knapen/CMS_track_trigger/BtoKX/results/"
outfile=outdir+"BtoKX_efficiencies.txt"

filenames=glob.glob(datadir+'*dat')

masslist=list(dict.fromkeys([filename.split("_")[-3] for filename in filenames]))
ctaulist=list(dict.fromkeys([filename.split("_")[-2] for filename in filenames]))

print masslist
print ctaulist

results=[]

for m in masslist:
    for ctau in ctaulist:
#for m in ["1"]:
#    for ctau in ["1"]:
        table=np.loadtxt(datadir+"BtoKX_reco_"+m+"_"+ctau+"_1.dat")
        
        print "processing "+m+" "+ctau
        
        if(len(table)%2 !=0):
            print "error, uneven number of tracks"
            sys.exit()
    
        table=np.split(table,len(table)/2)
        fidlistAll=filter(cuts,table)
        
        fidlist1=filter(lambda tracks:pTcuts(tracks,3,3),fidlistAll)   #3GeV/3GeV
        fidlist2=filter(lambda tracks:pTcuts(tracks,4,4),fidlistAll)   #4GeV/4GeV 
        fidlist3=filter(lambda tracks:pTcuts(tracks,5,3),fidlistAll)   #5GeV/3GeV
        LHCblist=filter(LHCbcuts,table)
        ATLASlist1=filter(lambda x: ATLAScuts(x) and pTcuts(x,10,10),table)  #10GeV/10GeV (future magic)
        ATLASlist2=filter(lambda x: ATLAScuts(x) and pTcuts(x,20,15),table)  #20GeV/15GeV (now)
        
        eff1=float(len(fidlist1))/float(len(table))
        eff2=float(len(fidlist2))/float(len(table)) 
        eff3=float(len(fidlist3))/float(len(table))
        effLHCb=float(len(LHCblist))/float(len(table))/10. # luminosity penalty
        effATLAS1=float(len(ATLASlist1))/float(len(table))
        effATLAS2=float(len(ATLASlist2))/float(len(table))
        effATLAS2=float(len(ATLASlist2))/float(len(table))
           
        results.append([float(m),float(ctau),eff1,eff2,eff3,effLHCb,effATLAS1,effATLAS2])
        
        print map(lambda tracks: radius(tracks[0]),fidlistAll)
        
print results
np.savetxt(outfile,results)
