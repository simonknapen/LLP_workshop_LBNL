#!/usr/bin/python

# This script preprosesses the fake track files, by splitting it into a file with "good" vertices and "bad" vertices, asking for three tracks.
# The former are then used as a seed fake vertex, the latter as additional, loose tracks, which may or may not overlap with#
# with the seed.

import sys
import glob
import os
import numpy as np
import math

filenameingood="/home/data1/knapen/CMS_track_trigger/fakes/fakes_2tr_good.dat"
filenameinbad="/home/data1/knapen/CMS_track_trigger/fakes/fakes_2tr_good.dat"

filanameoutgood="/home/data1/knapen/CMS_track_trigger/fakes/fakes_3tr_good.dat"
filanameoutbad="/home/data1/knapen/CMS_track_trigger/fakes/fakes_3tr_bad.dat"

fgood= open(filanameoutgood,"w")
fbad= open(filanameoutbad,"w")

finbad=open(filenameinbad,"w")


for line in open(filenameingood, "r"):
  splitted=line.split("\t")
  for i in range(len(50)):
    badline=finbad.readline()
    
    
    
    fgood.write(line)
  else:
    fbad.write(line)
  #if (int(splitted[0]) % 50000)==0:
  #  print splitted[0]
    
fgood.close()
fbad.close()
  