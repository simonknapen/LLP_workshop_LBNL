#!/usr/bin/python

import sys
import glob
import os
import numpy as np
import math

def p(track):
    return np.linalg.norm(track[3:6])

def pT(track):
    return np.linalg.norm(track[3:5])
    
def lT(track):
    return np.linalg.norm(track[6:8])

def lz(track):
    return np.abs(track[8])
    
def eta(track):
    return np.arctanh(track[5]/np.linalg.norm(track[3:6]))

def weight(tracks,mass,ctau):
    pmother=tracks[0][3:6]+tracks[1][3:6]
    boost= (tracks[0][2]+tracks[1][2])/mass
    cosangle=pmother[2]/np.linalg.norm(pmother)
    sinangle=np.sqrt(1-cosangle**2)
        
    if sinangle/cosangle>2.2/60: # transverse intersection
        L = 2.2/sinangle
    else:# longitudinal intersection
        L = 60/cosangle    
    return 1.0-np.exp(-L/(ctau*boost))    
        


def LHCbcuts(tracks):# all truth-level
    if(pT(tracks[0])<0.5 or pT(tracks[1])<0.5):
        return False
    #if(p(tracks[0])<10 or p(tracks[1])<10):
    if(p(tracks[0])<6 or p(tracks[1])<6):   # per vava's suggestion 
        return False
    if (eta(tracks[0])>5 or eta(tracks[0])<2 or eta(tracks[1])>5 or eta(tracks[1])<2):
        return False
    if (lT(tracks[0])>22): # pythia prints mm not cm
        return False
    if (lz(tracks[0])>600):# pythia prints mm not cm
        return False
    return True

def LHCbweights(tracks,mass,ctau):# all truth-level
    if(pT(tracks[0])<0.5 or pT(tracks[1])<0.5):
        return 0.0
    if(p(tracks[0])<10 or p(tracks[1])<10):
        return 0.0
    if (eta(tracks[0])>5 or eta(tracks[0])<2 or eta(tracks[1])>5 or eta(tracks[1])<2):
        return 0.0
    return weight(tracks,mass,ctau)


datadir="/home/data1/knapen/CMS_track_trigger/BtoKX/LHCb/"
outdir="/home/data1/knapen/CMS_track_trigger/BtoKX/results/"
outfile=outdir+"BtoKX_efficiencies_LHCb.txt"

filenames=glob.glob(datadir+'*txt')

masslist=list(dict.fromkeys([filename.split("_")[-3] for filename in filenames]))
ctaulist=list(dict.fromkeys([filename.split("_")[-2] for filename in filenames]))

print masslist
print ctaulist

results=[]

for m in masslist:
    for ctau in ctaulist:
#for m in ["1"]:
#    for ctau in ["0.1"]:
        table=np.loadtxt(datadir+"BtoKX_"+m+"_"+ctau+"_1.txt")
        
        print "processing "+m+" "+ctau
        
        if(len(table)%2 !=0):
            print "error, uneven number of tracks"
            sys.exit()
    
        table=np.split(table,len(table)/2)
        
        fidlist=filter(LHCbcuts,table)
        eff=float(len(fidlist))/float(len(table))
        weightsum=np.sum(map(lambda x:LHCbweights(x,float(m),float(ctau)),table))/float(len(table))
        
        results.append([float(m),float(ctau),eff,weightsum])

np.savetxt(outfile,results)





















