#!/usr/bin/python

# This script preprosesses the fake track files, by splitting it into a file with "good" vertices and "bad" vertices, asking for two tracks.
# The former are then used as a seed fake vertex, the latter as additional, loose tracks, which may or may not overlap with#
# with the seed.

import sys
import glob
import os
import numpy as np
import math



filename="/home/data1/knapen/CMS_track_trigger/fakes/"+sys.argv[1]
filanamegood="/home/data1/knapen/CMS_track_trigger/fakes/"+sys.argv[1].replace(".dat","_2tr_good.dat")
filanamebad="/home/data1/knapen/CMS_track_trigger/fakes/"+sys.argv[1].replace(".dat","_2tr_bad.dat")

fgood= open(filanamegood,"w")
fbad= open(filanamebad,"w")

for line in open(filename, "r"):
  splitted=line.split("\t")
  if float(splitted[19])<0.2 and float(splitted[20])<1.0:
    fgood.write(line)
  else:
    fbad.write(line)
  #if (int(splitted[0]) % 50000)==0:
  #  print splitted[0]
    
fgood.close()
fbad.close()
  