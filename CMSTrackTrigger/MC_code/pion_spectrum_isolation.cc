// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to muons. 
// Output is in reduced hepmc format

 
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include <math.h> 

using namespace Pythia8;



template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

float angleMod(float a){
  if(a>M_PI){
    return a-2.0*M_PI;
  }
  else{
    if(a<-M_PI){
      return a+2.0*M_PI;
    }
    else{
      return a;
    }
  }
}

//3 digits behind komma
double myround(double val){
    return roundf(val * 1000) / 1000;
}

bool isTrack(Particle tr){
  return tr.isFinal() && tr.pT()>2.0 && tr.charge()!=0;
}

double isolation(Event event,int ipi){
   double sumpT=0.0;
   double conesize=0.4;
   double Deltaeta, Deltaphi, DeltaRsq;
   
          for(int j = 0; j < event.size(); ++j){
            if(isTrack(event[j]) && ipi!=j){
              Deltaeta=event[ipi].eta()-event[j].eta();
              Deltaphi=angleMod(event[ipi].phi()-event[j].phi());
              DeltaRsq=Deltaeta*Deltaeta + Deltaphi*Deltaphi;
              if(DeltaRsq<conesize*conesize){
                //cout << ipi<< "\t" << j << "\t" << event[j].id()<< "\t" << event[j].pT() << "\t" << sqrt(DeltaRsq) << "\n";
                sumpT=sumpT+event[j].pT();
              } 
            }
        }
        return sumpT/event[ipi].pT();
  }
    

                                                    
int main() {
  Pythia pythia;

  std::string outfilename, outfilenamefull;   
  outfilename="/home/data1/knapen/CMS_track_trigger/geant/pion_pT_spectrum_isolation_5_w4_5_w4.txt";
  outfilenamefull="/home/data1/knapen/CMS_track_trigger/geant/pion_spectrum_isolation_full_5_w4.txt";

  // path of output file
  ofstream myfile, myfilefull;
  myfile.open(outfilename.c_str());   
  myfilefull.open(outfilenamefull.c_str());  
  
  //pythia.readString("Random:setSeed = on");
  //pythia.readString("Random:seed = 1");    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:all = on"); 
  pythia.readString("PhaseSpace:pTHatMin  = 5");
  pythia.readString("PhaseSpace:bias2Selection = on");
  pythia.readString("PhaseSpace:bias2SelectionPow =4");
  pythia.readString("111:mayDecay  = off");
  
  pythia.init(); 
  
  Event& event = pythia.event;
  Info& info = pythia.info;

  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < 10000 ; ++iEvent) {
    if (!pythia.next()) continue;
    // only process the event if the "hard process" has pT < 5 GeV
    //event = pythia.event;     
    // List first few events. 
    if (iEvent < 15) {
      event.list();
    }

    for (int i = 0; i < event.size(); ++i){ 
        if (event[i].isFinal() && event[i].pT()>15.0 && event[i].isHadron()&& event[i].id()!=111){
          myfile << iEvent<<"\t"<< event[i].id() <<"\t"<< event[i].pT() <<"\t"<<event[i].phi() <<"\t"<< event[i].eta() << "\t"<<isolation(event,i) <<"\t" << info.weight() <<"\n"; 
        }
        if (event[i].isFinal()){
          myfilefull << iEvent<<"\t"<< event[i].id() <<"\t"<< event[i].pT() <<"\t"<< event[i].phi() <<"\t"<< event[i].eta() << "\t"<<isolation(event,i) <<"\t" << info.weight() <<"\n"; 
        }
      
      
    }
  }
      
  // End of event loop. Statistics. Histogram.
  pythia.stat();
  
  std::cout << info.weightSum() <<"\n";
  
  myfile.close();
  myfilefull.close();
  // Done.
  // Done.
  return 0;
  
  }

