// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to muons. 
// Output is in reduced hepmc format


#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include<string>
#include<stdio.h>
#include<stdlib.h>

using namespace Pythia8;
Pythia pythia;

template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 


Event TrimEvent(Event event,Event trimmedevent){
    int trimmedAprimepos=0;
    int trimmedmotherpos=0;
    int trimmedsisterpos=0;
    std::vector<int> motherpos;
    std::vector<int> sisterpos;
    int sisterposlast;
        
    for (int i = 0; i < event.size(); ++i){ 
        if (event[i].id()==999999) { 
        
        // Find mother (the B meson)
        motherpos=event[i].motherList();
        trimmedevent.append(event[motherpos[0]]);
        trimmedmotherpos=trimmedevent.size()-1; //record mother pos in new event
        trimmedevent[trimmedmotherpos].mothers(0,0);//fix the mother of the B to be the header
            
        // Find sister (the K meson) 
        sisterpos=event[i].sisterList();
        sisterposlast=event[sisterpos[0]].iBotCopyId();
        if (event[sisterposlast].isFinal()) {
            trimmedevent.append(event[sisterposlast]);
        }
        else{           //If K0, go one more level down, to K_L or K_S
            sisterposlast=(event[sisterposlast].daughterList())[0];
            trimmedevent.append(event[sisterposlast]);
        }
        trimmedsisterpos=trimmedevent.size()-1;
        trimmedevent[trimmedsisterpos].mothers(trimmedmotherpos,0);
         
        // Now add the A' to the event    
        trimmedevent.append(event[i]);
        trimmedAprimepos=trimmedevent.size()-1;
        trimmedevent[trimmedAprimepos].mothers(trimmedmotherpos,0);
       
        // Search for the daughters of the A'    
        for (int j = 0; j < event.size(); ++j){
              if(event[j].isAncestor(i) && event[j].isFinal()){
                event[j].mothers(trimmedAprimepos,0);  
                trimmedevent.append(event[j]);
                }
            }
        break; // For this case we only keep one B decay, the first one I find    
          }
      }
  return trimmedevent;
}

  
int main(int argc, char *argv[]) {
     
   
  if(!(argc==4)){
    std::cout << "I need X mass, ctau and random seed";
    return 0;
  }
     

  string mX;
  mX=tostr(argv[1]);
  string ctau_in_mm, ctau_in_cm;
  ctau_in_cm=tostr(atof(argv[2]));    // input in cm;
  ctau_in_mm=tostr(atof(argv[2])*10); // pythia needs mm;    
  string seed;
  seed=tostr(argv[3]);      
  std::string outfilename;   
  //outfilename="BtoKX_"+mX+"_"+ctau_in_cm+"_"+seed+".hepmc";
  outfilename="BtoKX_4track_"+mX+"_"+ctau_in_cm+"_"+seed+".hepmc";    

  // path of output file  
  std::string dir="/home/data1/knapen/CMS_track_trigger/BtoKX/hepmc_in/";         
      
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(dir+outfilename);    
  // Generator; shorthand for event and particleData.
  Event& event = pythia.event;

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = "+seed);    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:hardbbbar  = on");
  pythia.readString("PhaseSpace:pTHatMin = 1.0");
  //pythia.readString("PhaseSpace:pTHatMin = 10.0");    
  //pythia.readString("PartonLevel:ISR = off");
  //pythia.readString("PartonLevel:FSR = off");
  //pythia.readString("HadronLevel:Hadronize= off");

    
  ////////////////////////////////////////////////////////////////////////////    
  // X definition and branching ratios  
  pythia.readString("999999:all = GeneralResonance void 0 0 0 "+mX+" 0.001 0.0 0.0 "+ctau_in_mm);
  pythia.readString("999999:addChannel = 1 1.0 101 13 -13 13 -13");
  //pythia.readString("999999:addChannel = 1 1.0 101 13 -13");    
  // B -> K X decay
  //pythia.readString("521:tau0 = 0.0");//for testing purposes
  //pythia.readString("511:tau0 = 0.0");//for testing purposes     
  pythia.readString("521:oneChannel = 1 1.0 0 999999 321");
  pythia.readString("511:oneChannel = 1 1.0 0 999999 311");
  ///////////////////////////////////////////////////////////////////////////    
        
  pythia.init();  

  Event trimmedevent;      
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < 50000 ; ++iEvent) {
    if (!pythia.next()) continue;

    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build; but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::CM);

    trimmedevent=event;
    trimmedevent.reset(); 
    trimmedevent=TrimEvent(event,trimmedevent);  
    //trimmedevent.list();
      
    ToHepMC.fill_next_event(trimmedevent,hepmcevt);
    ascii_io << hepmcevt;
    delete hepmcevt;
    
    // List first few events.
    if (iEvent < 1) {
      trimmedevent.list();
    }
      
  // End of event loop. Statistics. Histogram.
  }
  //pythia.stat();   
  // Done.
     
  // Done.
  return 0;

  }

