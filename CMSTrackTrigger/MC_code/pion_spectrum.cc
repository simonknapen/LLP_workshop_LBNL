// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to muons. 
// Output is in reduced hepmc format

 
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"

using namespace Pythia8;

template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

//3 digits behind komma
double myround(double val){
    return roundf(val * 1000) / 1000;
}


                                                    
                                                    
int main(int argc, char *argv[]) {
  Pythia pythia;
  
  double jetpT;

  std::string outfilename;   
  outfilename="/home/data1/knapen/CMS_track_trigger/geant/pion_pT_spectrum_5_w4.txt";

  // path of output file
  ofstream myfile;
  myfile.open(outfilename.c_str());   
  
  //pythia.readString("Random:setSeed = on");
  //pythia.readString("Random:seed = 1");    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:all = on"); 
  pythia.readString("PhaseSpace:pTHatMin  = 5");
  pythia.readString("PhaseSpace:bias2Selection = on");
  pythia.readString("PhaseSpace:bias2SelectionPow =4");
  
  pythia.init(); 
  
  Event& event = pythia.event;
  Info& info = pythia.info;

  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < 50000 ; ++iEvent) {
    if (!pythia.next()) continue;
    // only process the event if the "hard process" has pT < 5 GeV
    //event = pythia.event;     
    // List first few events. 
    if (iEvent < 3) {
      event.list();
    }
    
    for (int i = 0; i < event.size(); ++i){
      if (event[i].status()==-23){
        jetpT=event[i].pT();
        break;
      }
    }

    for (int i = 0; i < event.size(); ++i){ 
        if ((event[i].id()==211 || event[i].id()==-211 || event[i].id()==130 || event[i].id()== 321 || 
             event[i].id()== 2212 || event[i].id()== 2112)
            && event[i].isFinal() && std::abs(event[i].eta())<2.4 && event[i].pT()>8.0){ 
          myfile << event[i].pT() <<"\t"<< event[i].eta() <<"\t" << info.weight() << "\t" <<jetpT<<"\n"; 
        }
    }
  
      
  // End of event loop. Statistics. Histogram.
  }
  pythia.stat();
  
  std::cout << info.weightSum()<<"\n";
  
  myfile.close();
  // Done.
  // Done.
  return 0;
  
  }

