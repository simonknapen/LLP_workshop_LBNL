// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to multi-tracks. 
// This is just meant as a test until the ALP code is ready
// Output is in reduced hepmc format

// ROOT, for saving Pythia events as trees in a file.
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "Pythia8/Pythia.h"
#include<string>
#include<stdio.h>
#include<stdlib.h>

using namespace Pythia8;
Pythia pythia;

template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

Event TrimEvent(Event event,Event trimmedevent){
    int trimmedAprimepos=0;
    int trimmedmotherpos=0;
    std::vector<int> motherpos;
        
    for (int i = 0; i < event.size(); ++i){ 
        if (std::abs(event[i].id())==511 || std::abs(event[i].id())==521) { 
        
        // Now add the B to the event    
        trimmedevent.append(event[i]);
        trimmedAprimepos=trimmedevent.size()-1;
        trimmedevent[trimmedAprimepos].mothers(trimmedmotherpos,0);
       
        // Search for the daughters of the B    
        for (int j = 0; j < event.size(); ++j){
              if(event[j].isAncestor(i) && event[j].isFinal()){
                event[j].mothers(trimmedAprimepos,0);  
                trimmedevent.append(event[j]);
                }
            }
          break; // only record the first B meson you encounter, for simplicity
          }
      }  
  return trimmedevent;
}


  
int main(int argc, char *argv[]) {
  
  if(!(argc==2)){
    std::cout << "I need random seed";
    return 0;
  }
  
  int Ntracks;  
  
    //create the file, the Tree and a few branches
  std::string rootdir="/home/data1/knapen/CMS_track_trigger/B_background/root/";
  std::string rootfilename=rootdir+"B_background_"+tostr(argv[1])+".root";
  
  // file to store event weights
  std::string weightfile;   
  weightfile=rootdir+"B_backgrounds_weigths_"+tostr(argv[1])+".txt";

  // path of output file
  ofstream myfile;
  myfile.open(weightfile.c_str());   
  
  gROOT->ProcessLine("#include <vector>");
  
  char * rootfilenamechar;
  rootfilenamechar = new char [rootfilename.size()+1];
  strcpy (rootfilenamechar, rootfilename.c_str());  
    
  TFile f(rootfilenamechar,"recreate");
  TTree t1("t1","a simple Tree with simple variables");
  std::vector<int> pid;
  std::vector<float> px, py, pz;
  std::vector<float> Vx, Vy, Vz, Vt;
  t1.Branch("pid","std::vector",&pid);    
  t1.Branch("px","std::vector",&px);
  t1.Branch("py","std::vector",&py);
  t1.Branch("pz","std::vector",&pz);
  t1.Branch("Vt","std::vector",&Vt);    
  t1.Branch("Vx","std::vector",&Vx);
  t1.Branch("Vy","std::vector",&Vy);
  t1.Branch("Vz","std::vector",&Vz);
  
   // pythia settings
  Event& event = pythia.event;
  Info& info = pythia.info;

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = "+tostr(argv[1]));    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:hardbbbar  = on");
  pythia.readString("PhaseSpace:pTHatMin = 8.0");
  pythia.readString("PhaseSpace:bias2Selection = on");
  pythia.readString("PhaseSpace:bias2SelectionPow =4");
    
  pythia.init();
  
  

  Event trimmedevent;      
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < 50000 ; ++iEvent) {
    if (!pythia.next()) continue;
      
    trimmedevent=event;
    trimmedevent.reset(); 
    trimmedevent=TrimEvent(event,trimmedevent);  
            
    // List first few events.
    if (iEvent < 5) {
      trimmedevent.list();
    }
    
    for (int i = 0; i < trimmedevent.size(); ++i){
            
        if((trimmedevent[i].isFinal() && trimmedevent[i].isCharged()) || std::abs(trimmedevent[i].id())==511 || std::abs(trimmedevent[i].id())==521){
                pid.push_back(trimmedevent[i].id());
                px.push_back(trimmedevent[i].px());
                py.push_back(trimmedevent[i].py());
                pz.push_back(trimmedevent[i].pz());
                Vx.push_back(trimmedevent[i].xProd()/10.);//convert mm to cm
                Vy.push_back(trimmedevent[i].yProd()/10.);//convert mm to cm
                Vz.push_back(trimmedevent[i].zProd()/10.);//convert mm to cm
                Vt.push_back(trimmedevent[i].tProd());
                }
            //}
        }
    
    t1.Fill();
        
    pid.clear();
    px.clear();
    py.clear();
    pz.clear();
    Vx.clear();
    Vy.clear();
    Vz.clear();
    Vt.clear();
    
    myfile << info.weight() << "\n";
      
      
  // End of event loop. Statistics. Histogram.
  }
  pythia.stat();   
  std::cout <<"\n sum of weights:" << info.weightSum()<<"\n";
  //  Write tree.
  t1.Print();
  t1.Write();
  myfile.close();
  
  // Done.
  return 0;

  }

