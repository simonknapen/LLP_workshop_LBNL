// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to muons. 
// Output is in reduced hepmc format


#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include<string>
#include<stdio.h>
#include<stdlib.h>

using namespace Pythia8;
Pythia pythia;

template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 


Event TrimEvent(Event event,Event trimmedevent){
    int trimmedAprimepos=0;
    int trimmedmotherpos=0;
    for (int i = 0; i < event.size(); ++i){ 
        if (event[i].id()==531) { 
         
        // Now add the A' to the event    
        trimmedevent.append(event[i]);
        trimmedAprimepos=trimmedevent.size()-1;
        trimmedevent[trimmedAprimepos].mothers(trimmedmotherpos,0);
       
        // Search for the daughters of the A'    
        for (int j = 0; j < event.size(); ++j){
              if(event[j].isAncestor(i) && event[j].isFinal()){
                event[j].mothers(trimmedAprimepos,0);  
                trimmedevent.append(event[j]);
                }
            }
        break; // For this case we only keep one B decay, the first one I find    
          }
      }
  return trimmedevent;
}

  
int main(int argc, char *argv[]) {
     
   
  if(!(argc==2)){
    std::cout << "I need random seed";
    return 0;
  }
     
  string seed;
  seed=tostr(argv[1]);      
  std::string outfilename;   
  outfilename="Bs_"+seed+".hepmc";

  // path of output file  
  std::string dir="/home/data1/knapen/CMS_track_trigger/Bs/hepmc_in/";         
      
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(dir+outfilename);    
  // Generator; shorthand for event and particleData.
  Event& event = pythia.event;

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = "+seed);    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:hardbbbar  = on");
  pythia.readString("PhaseSpace:pTHatMin = 10.0");
    
  pythia.readString("531:oneChannel = 1 1.0 0 13 -13");
        
  pythia.init();  

  Event trimmedevent;      
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < 50000 ; ++iEvent) {
    if (!pythia.next()) continue;

    trimmedevent=event;
    trimmedevent.reset(); 
    trimmedevent=TrimEvent(event,trimmedevent);  
    //trimmedevent.list();  
      
    if(trimmedevent.size()>1){  
        // Construct new empty HepMC event and fill it.
        // Units will be as chosen for HepMC build; but can be changed
        // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
        HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::CM);

    
      
        ToHepMC.fill_next_event(trimmedevent,hepmcevt);
        ascii_io << hepmcevt;
        delete hepmcevt;
    
        // List first few events.
        if (iEvent < 1) {
        trimmedevent.list();
        }
    }
  // End of event loop. Statistics. Histogram.
  }
  //pythia.stat();   
  // Done.
     
  // Done.
  return 0;

  }

