// main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates H->A'A', with A' a dark photon. For mA' > 5 GeV, 
// the decay is handled by the pythia Z' model. For mA'< 5 GeV, explicit
// decay tables must be specified. ctau is fixed to 1mm
// Output is in hepmc

// WARNING: typically one needs 25 MB/100 events at the LHC.
// Therefore large event samples may be impractical.

// needs module load root/6.04.18

#include "Pythia8/Pythia.h"
//#include "Pythia8Plugins/HepMC2.h"
  
// ROOT, for saving Pythia events as trees in a file.
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"

using namespace Pythia8;


template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

inline bool file_exists (const std::string& name) {
    ifstream f(name.c_str());
    return f.good();
}

bool hadronization_ok (Event event) {
    for (int i = 0; i < event.size(); ++i){
        if(event[i].isFinal() && event[i].id()==4900101){
            return false;
        }
    }
    return true; 
}
 

Event TrimEvent(Event event,Event trimmedevent){
    int trimmedAprimepos=0;
    int trimmedmotherpos=0;
    int trimmedsisterpos=0;
    std::vector<int> motherpos;
    std::vector<int> sisterpos;
    int sisterposlast;
        
    for (int i = 0; i < event.size(); ++i){ 
        if (event[i].id()==999999) { 
        
        // Find mother (the B meson)
        motherpos=event[i].motherList();
        trimmedevent.append(event[motherpos[0]]);
        trimmedmotherpos=trimmedevent.size()-1; //record mother pos in new event
        trimmedevent[trimmedmotherpos].mothers(0,0);//fix the mother of the B to be the header
         
        // Now add the A' to the event    
        trimmedevent.append(event[i]);
        trimmedAprimepos=trimmedevent.size()-1;
        trimmedevent[trimmedAprimepos].mothers(trimmedmotherpos,0);
       
        // Search for the daughters of the A'    
        for (int j = 0; j < event.size(); ++j){
              if(event[j].isAncestor(i) && event[j].isFinal()){
                event[j].mothers(trimmedAprimepos,0);  
                trimmedevent.append(event[j]);
                }
            }  
          }
      }
  return trimmedevent;
}



int main( int argc, char *argv[]) {

   if(!(argc==4)){
        std::cout << "I need pi0' mass, ctau, seed \n";
        return 0;
    }
  
  float pi0mass= atof(argv[1]);
  float ctau= atof(argv[2]);
  string seed;
  seed=tostr(argv[3]);   
  std::string massAp;    
    
    
  Pythia pythia("/home/smknapen/pythia8235/share/Pythia8/xmldoc"); //pythia object 
  //Pythia pythia;
  Event& event      = pythia.event; // Generator; shorthand for event and particleData.
  Event trimmedevent;    
    
  // hepmc output file    
  //std::string outfilename;
  //outfilename="HV_ggF_"+tostr(pi0mass)+"_"+tostr(ctau)+"_"+seed+".hepmc";
  //std::string dir="/home/data1/knapen/CMS_track_trigger/h_to_HV/hepmc_in/";    
    
  gROOT->ProcessLine("#include <vector>");
  
  //create the file, the Tree and a few branches
  std::string rootdir="/home/data1/knapen/CMS_track_trigger/h_to_HV/root/";
  std::string rootfilename=rootdir+"HV_ggF_"+tostr(pi0mass)+"_"+tostr(ctau)+"_"+seed+".root";

  char * rootfilenamechar;
  rootfilenamechar = new char [rootfilename.size()+1];
  strcpy (rootfilenamechar, rootfilename.c_str());  
    
  TFile f(rootfilenamechar,"recreate");
  TTree t1("t1","a simple Tree with simple variables");
  std::vector<int> pid;
  std::vector<float> px, py, pz;
  std::vector<float> Vx, Vy, Vz, Vt;
  t1.Branch("pid","std::vector",&pid);    
  t1.Branch("px","std::vector",&px);
  t1.Branch("py","std::vector",&py);
  t1.Branch("pz","std::vector",&pz);
  t1.Branch("Vt","std::vector",&Vt);    
  t1.Branch("Vx","std::vector",&Vx);
  t1.Branch("Vy","std::vector",&Vy);
  t1.Branch("Vz","std::vector",&Vz);
    
    
  // Read in commands from external file.
  pythia.readFile("/home/smknapen/CMS_track_trigger/MC_code/ggF.cmnd");
  pythia.readFile("/home/smknapen/CMS_track_trigger/MC_code/HV_settings.cmnd");
    
  // Set the mass scales
  pythia.readString("4900101:m0 = "+tostr(0.5*pi0mass));    // Setting Dark Quark Mass    
  pythia.readString("4900111:m0 = "+tostr(pi0mass));  // Setting pi0  Mass 
  pythia.readString("4900113:m0 = "+tostr(4.0*pi0mass)); // Setting rho0 Mass
  pythia.readString("4900211:m0 = "+tostr(pi0mass));  // Setting pi+  Mass
  pythia.readString("4900213:m0 = "+tostr(4.0*pi0mass)); // Setting rho+ Mass   
    
  // Setting Dark pi0 Decay Channels
  pythia.readString("4900111:oneChannel = 1 1.0 91 999999 999999");             // po0' -> \gamma' \gamma'

  // Setting Dark pi0 Proper Lifetime
  pythia.readString("4900111:tau0 = 0.");    
  massAp=tostr(0.4*pi0mass); // Always fix the dark photon mass to be 0.4 of pi0 mass
  pythia.readString("999999:all = GeneralResonance void 1 0 0 "+massAp+" 0.001 0. 0. "+tostr(ctau*10));//convert to mm
  pythia.readString("999999:addChannel = 1 1.0 101 13 -13");                 //  -> mu+ mu-      
                
  // set up collision parameters  
  pythia.readString("Random:setSeed = on");//reinitialize!!!
  pythia.readString("Random:seed = "+seed);

    
  // Interface for conversion from Pythia8::Event to HepMC event.
  //HepMC::Pythia8ToHepMC ToHepMC;
  //HepMC::IO_GenEvent ascii_io(dir+outfilename);  // Specify file where HepMC events will be stored.
  //ToHepMC.set_print_inconsistency(false);// Switch off warnings for parton-level events.
  //ToHepMC.set_free_parton_warnings(false);
  //ToHepMC.set_store_pdf(false);// Do not store cross section information, as this will be done manually.
  //ToHepMC.set_store_proc(false);
  //ToHepMC.set_store_xsec(false);      
    
  pythia.init();
   
     
  // Begin event loop. Generate event. Skip if error.
  int iEvent = 0;
  while (iEvent < 5) {
    if (!pythia.next()) continue;
      
    //Check if the HV hadronization has succeeded  
    if(hadronization_ok(event)){  
        // Construct new empty HepMC event and fill it.
        // Units will be as chosen for HepMC build; but can be changed
        // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
        //HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::CM); 
      
        trimmedevent=event;
        trimmedevent.reset();
        trimmedevent=TrimEvent(event,trimmedevent);  
    
        if(iEvent<2){  
        //    event.list();
            trimmedevent.list();  
        }
    
       //ToHepMC.fill_next_event(trimmedevent,hepmcevt);
       //ascii_io << hepmcevt;
       //delete hepmcevt;
        
        
        for (int i = 0; i < trimmedevent.size(); ++i){ 
            if (trimmedevent[i].id()==-13||trimmedevent[i].id()==13) {//only write out the muons
                
                pid.push_back(trimmedevent[i].id());
                px.push_back(trimmedevent[i].px());
                py.push_back(trimmedevent[i].py());
                pz.push_back(trimmedevent[i].pz());
                Vx.push_back(trimmedevent[i].xProd()/10.);//convert mm to cm
                Vy.push_back(trimmedevent[i].yProd()/10.);//convert mm to cm
                Vz.push_back(trimmedevent[i].zProd()/10.);//convert mm to cm
                Vt.push_back(trimmedevent[i].tProd());
            }
        }
        t1.Fill();
        
        pid.clear();
        px.clear();
        py.clear();
        pz.clear();
        Vx.clear();
        Vy.clear();
        Vz.clear();
        Vt.clear();
        
       iEvent+=1;    
    }

  // End of event loop. Statistics. Histogram. 
  }
  pythia.stat();   
  // Done.
  
  //  Write tree.
  t1.Print();
  t1.Write();  
    
  // Done.
  return 0;
  
  }

