// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to muons. 
// Output is in reduced hepmc format
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include <cstdlib>

using namespace Pythia8;
Pythia pythia;

template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

int main() {
     

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = 1");    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:hardbbbar  = on");
  pythia.readString("PhaseSpace:pTHatMin = 10.0");
        
  pythia.init();  

  Event& event = pythia.event;     
  // Begin event loop. Generate event. Skip if error.
    
  int eventcounter = 0;    
  for (int iEvent = 0; iEvent < 10000 ; ++iEvent) {
    if (!pythia.next()) continue;
    
    int muoncounter = 0;   
    for (int i = 0; i < event.size(); ++i){
        if((event[i].id()==13||event[i].id()==-13)&& event[i].isFinal() && event[i].pT()>3.0){
            muoncounter++;
        }        
    }
    if(muoncounter>1){
        eventcounter++;
        std::cout << muoncounter <<"\n";
    }  
  }
  std::cout << "#events passed: " <<eventcounter;      
      
  return 0;

  }

