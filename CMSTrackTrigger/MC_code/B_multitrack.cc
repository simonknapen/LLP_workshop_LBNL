// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates B->K X, with X a real scalar decaying to multi-tracks. 
// This is just meant as a test until the ALP code is ready
// Output is in reduced hepmc format


#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include<string>
#include<stdio.h>
#include<stdlib.h>

using namespace Pythia8;
Pythia pythia;

template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

  
int main() {
       
  std::string outfilename;   
  outfilename="B_multitrack.hepmc";

  // path of output file  
  std::string dir="/home/data1/knapen/CMS_track_trigger/alp/hepmc_in/";         
      
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(dir+outfilename);    
  // Generator; shorthand for event and particleData.
  Event& event = pythia.event;

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = 1");    
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("HardQCD:hardbbbar  = on");
  pythia.readString("PhaseSpace:pTHatMin = 10.0");
    
  // X definition and branching ratios
  pythia.readString("999999:all = chi antichi 2 0 0 3.0 0.001 0.0 0.0 10");//10 mm
  pythia.readString("999999:addChannel = 1 0.34 101 1103 2");
  pythia.readString("999999:addChannel = 1 0.66 101 2101 1");
  // B -> K X decay
  pythia.readString("521:oneChannel = 1 1.0 0 999999 2212");
    
  pythia.init();  

  Event trimmedevent;      
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < 10 ; ++iEvent) {
    if (!pythia.next()) continue;
      
    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build; but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::CM);

    ToHepMC.fill_next_event(event,hepmcevt);
    ascii_io << hepmcevt;
    delete hepmcevt;
    
        // List first few events.
    if (iEvent < 1) {
    event.list();
    }
    
  // End of event loop. Statistics. Histogram.
  }
  //pythia.stat();   
  // Done.
     
  // Done.
  return 0;

  }

