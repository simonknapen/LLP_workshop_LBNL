// based in part on main41.cc is a part of the PYTHIA event generator.
// Copyright (C) 2015 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Simon Knapen
// This program simulates 
// p p -> A' j -> chi1 ch2 j  with chi2 -> chi1 l+ l- with displaced vertex
// To compile, add in the pythia example folder
// add "inelastic_DM" to the line in the Makefile which links the hepmc library
// make inelastic_DM

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include <cstdlib>

using namespace Pythia8;



template <typename T> string tostr(const T& t) { 
   ostringstream os; 
   os<<t; 
   return os.str(); 
} 

// This routine strips the event of all particles except the LLP, it's daughters and mother.
Event TrimEvent(Event event,Event trimmedevent){
    int trimmedchi2pos=0;
    int trimmedmotherpos=0;
    int trimmedsisterpos=0;
    std::vector<int> motherpos;
    std::vector<int> sisterpos;
    int sisterposlast;
       
    for (int i = 0; i < event.size(); ++i){ 
        if (event[i].id()==999998) { // find the chi2 
        
        // Find mother (the A')
        motherpos=event[i].motherList();
        trimmedevent.append(event[motherpos[0]]);
        trimmedmotherpos=trimmedevent.size()-1; //record mother pos in new event
        trimmedevent[trimmedmotherpos].mothers(0,0);//fix the mother of the B to be the header
            
        // Find sister (the chi1) 
        sisterpos=event[i].sisterList();
        for (int j = 0; j < sisterpos.size(); ++j){
            sisterposlast=event[sisterpos[j]].iBotCopyId();
            if (event[sisterposlast].id() != 311) {
                trimmedevent.append(event[sisterposlast]);
            }
            else{           //If K0, go one more level down, to K_L or K_S, not applicable here
                sisterposlast=(event[sisterposlast].daughterList())[0];
                trimmedevent.append(event[sisterposlast]);
            }
            trimmedsisterpos=trimmedevent.size()-1;
            trimmedevent[trimmedsisterpos].mothers(trimmedmotherpos,0);
        }
        // Now add the chi2 to the event    
        trimmedevent.append(event[i]);
        trimmedchi2pos=trimmedevent.size()-1;
        trimmedevent[trimmedchi2pos].mothers(trimmedmotherpos,0);
        
        // Search for the daughters of the chi2    
        for (int j = 0; j < event.size(); ++j){
             if(event[j].isAncestor(i) && event[j].isFinal()){
               event[j].mothers(trimmedchi2pos,0);  
               trimmedevent.append(event[j]);
                }
            }
          }
      }
  return trimmedevent;
}

  
int main(int argc, char *argv[]) {
    
    int Nevents;
    Nevents=10000;
      
    
  if(!(argc==6)){
    std::cout << "I need A' mass, chi1 mass, chi2 mass, ctau (cm) and random seed";
    return 0;
  }
  // keep in mind that pythia refuses to decay if ctau > 1000 mm
        
  string mA;
  mA=tostr(argv[1]);
  string mchi1;
  mchi1=tostr(argv[2]); 
  string mchi2;
  mchi2=tostr(argv[3]);
  string chi2ctau;
  chi2ctau=tostr(atoi(argv[4])*10); // input in cm, pythia needs mm
  string seed;
  seed=tostr(argv[5]);  
      
  // path of output file  
  string dir="/home/data1/knapen/CMS_track_trigger/inelastic_DM/hepmc_in/";      
        
  std::string outfilename;   
  outfilename="inelastic_DM_"+mA+"_"+mchi1+"_"+mchi2+"_"+tostr(argv[4])+"_"+seed+".hepmc";      
      
  Pythia pythia; 
// set up beams and random seed    
    pythia.readString("Next:numberShowEvent =0");  
    pythia.readString("Random:setSeed = on");
    pythia.readString("Random:seed = "+seed);    
    pythia.readString("Beams:eCM = 13000.");
    
// set up A' production  
    pythia.readString("NewGaugeBoson:ffbar2gmZZprime = on");
    pythia.readString("Zprime:gmZmode =3"); //only on-shell Z'  
    pythia.readString("Zprime:universality =on");
// initialize couplings for a dark photon
    pythia.readString("Zprime:vd =-0.333");
    pythia.readString("Zprime:vu =0.666");
    pythia.readString("Zprime:ve =-1.0");
    pythia.readString("Zprime:vnue =0");
    pythia.readString("Zprime:ad =0.0");
    pythia.readString("Zprime:au =0.0");
    pythia.readString("Zprime:ae =0.0");
    pythia.readString("Zprime:anue =0.0");
    pythia.readString("32:m0="+mA);
    pythia.readString("32:doForceWidth=on");
    pythia.readString("32:mWidth=0.01");
    
    
//initialize the DM doublet  
    //id:all = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
    pythia.readString("999999:all = chi1 chi1bar 2 0 0 "+mchi1+" 0.0 0.0 0.0 1.0");
    pythia.readString("999998:all = chi2 chi2bar 2 0 0 "+mchi2+" 0.0 0.0 0.0 "+chi2ctau);    
    pythia.readString("999999:mayDecay = off");    
    pythia.readString("999998:mWidth=0.5");
    
//set the decay chains up
    pythia.readString("32:oneChannel = 1 1.0 100 999999 999998");
    pythia.readString("32:addChannel = 1 1e-6 100 11 -11"); // Due to what appears to be a bug in pythia, we need to add a non-zero BR to the standard model  ¯\_(ツ)_/¯
    pythia.readString("999998:oneChannel = 1 1.0 100 999999 -13 13");  //100% branching ratio to muons for now.
    //pythia.readString("999998:oneChannel = 1 1.0 100 999999 -211 211 -211 211");  //100% branching ratio to 4pi for now.
    
   
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMCcc;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_iocc(dir+outfilename);    
  // Generator; shorthand for event and particleData.
  Event& event = pythia.event;
    
  pythia.init();  

  Event trimmedevent;      
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < Nevents ; ++iEvent) {
    if (!pythia.next()) continue;

    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build; but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::CM);

    trimmedevent=event;
    trimmedevent.reset(); 
    trimmedevent=TrimEvent(event,trimmedevent);    
    
    //event.list();
    
    // List first few events.
    if (iEvent < 5) {
      //event.list();    
      trimmedevent.list();
    }  
      
    ToHepMCcc.fill_next_event(trimmedevent,hepmcevt);
    ascii_iocc << hepmcevt;
    delete hepmcevt;
        
  // End of event loop. Statistics. Histogram.
  }
  //pythia.stat();   
      
  return 0;
    
  
  }

