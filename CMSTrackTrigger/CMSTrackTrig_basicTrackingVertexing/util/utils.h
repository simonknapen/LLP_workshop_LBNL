/*###############################################
 name: CMSTrackTrig_Utils 
 type: C++ namespace header
 responsible: rcarney@lbl.gov
 
 description:
    Contains 2 structs: one for the Vtx, one for the particle.
    Has functions to estimate displaced vertex position, check if vertex seems feasible,
    and count how many vertices point back to PV. 
 
 TODO: 
    - Finish off all circle-overlap cases
    - Do the z-y overlap (just a straight line overlap)
    - Check that the vertex made matches the d0/z0 of the tracks within
        the errors generated by Yuri for varying number of stubs. If not, reject vertex and track pairs.
    - Check vertex points to PV. Count.

 last updated: July 2018
###############################################*/

#ifndef CMS_TRACK_TRIG_UTILS_h
#define CMS_TRACK_TRIG_UTILS_h

#include <iostream>
#include <vector>
#include <cmath>
#include "trigfit.h"

namespace CMSTrackTrig_Utils{

   template <typename T> int sgn(T val) {
           return (T(0) < val) - (val < T(0));
   }    
    
   struct Vtx{

       double x;
       double y;
       double z;
       double Deltaxy;
       double Deltaz;
       double r;
       double phi;

       //Constructors
       Vtx():
           x(0.),
           y(0.),
           z(0.),
           r(0.),
           phi(0.),
           Deltaxy(0.),
           Deltaz(0.){}

       Vtx(double arg_x, double arg_y, double arg_z, double arg_Deltaxy, double arg_Deltaz){
        
           x = arg_x;
           y = arg_y;
           z = arg_z;
           r = std::sqrt(x*x+y*y);
           phi = std::atan2(y,x);
           Deltaxy = arg_Deltaxy;
           Deltaz = arg_Deltaz;       
       }
       
       void reset(){
           x = 0.;
           y = 0.;
           z = 0.;
           r = 0.;
           phi = 0.;
           Deltaxy = 0.;
           Deltaz = 0.;
       }
   }; //end Vtx struct def

   struct Particle{

       double px;
       double py;
       double pz;
       double pt;

       int charge;
       int pdgID;

       double energy;
       
       Vtx vtx;

       //Constructors
       Particle():
           px(0.),
           py(0.),
           pz(0.),
           pt(0.),
           charge(0),
           pdgID(0),
           energy(0.),
           vtx(){}

       Particle(double arg_px, double arg_py, double arg_pz, int arg_pdgID, double arg_energy){
           
           px = arg_px;
           py = arg_py;
           pz = arg_pz;
           pt = std::sqrt( px*px + py*py );

           pdgID = arg_pdgID;
           energy = arg_energy;
           charge = sgn( pdgID );
           vtx=Vtx();
       }

       Particle(double arg_px, double arg_py, double arg_pz, int arg_pdgID, double arg_energy, double arg_x, double arg_y, double arg_z){
           
           px = arg_px;
           py = arg_py;
           pz = arg_pz;
           pt = std::sqrt( px*px + py*py );

           pdgID = arg_pdgID;
           energy = arg_energy;
           charge = sgn( pdgID );
           vtx=Vtx(arg_x,arg_y,arg_z,0.0,0.0);
       }
       
       void reset(){
           px = 0.;
           py = 0.;
           pz = 0.;
           pt = 0.;

           charge = 0;
           pdgID = 0;
           energy = 0.;
       }
   }; //end Particle struct def


   Vtx estimateVertexPosition( std::vector<trigfit> trigfit );
   std::vector<double> recoMomentum(trigfit track, Vtx vtx );
   double closestApproach(std::vector<double> preco, Vtx vtx);
   double closestApproachTransverse(std::vector<double> preco, Vtx vtx);    
   double dot(std::vector<double> A, std::vector<double> B);
   double transverseDistanceToVertex(trigfit track, Vtx vtx );
   double zDistanceToVertex(trigfit track, Vtx vtx ); 
  
  
}


#endif
