// This needs root/6.04.18. Lower versions result in a seg fault on GetEvent. Probably related to the declarations of the pid pointers etc

// Instead of reading in the lifetime, we throw vertices uniform in Lxy, for later reweigthing.

//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

//random numbers
#include <random>

//root
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TBasket.h"
#include "TBrowser.h"
#include "TH2.h"
#include "TRandom.h"

//custom
#include "utils.h"
#include "trigfit.h"

using namespace std;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
 
template <typename T> string tostr(const T& t) { 
   std::stringstream os; 
   os<<t; 
   return os.str(); 
} 

int main(int argc, char *argv[]){
        
    if(!(argc==3)){
    std::cout << "I need inputfile name and outputfile name";
    return 0;
  }
    gROOT->ProcessLine("#include <vector>");
    
    std::string  datfilename = tostr(argv[2]);
    datfilename.erase(datfilename.end()-5, datfilename.end()); 
    datfilename = datfilename + ".dat";
    
    cout << datfilename <<"\n";
    
    //read the input file, create the Tree and a few branches
    TFile *f = new TFile(argv[1]);
    TTree *t1 = (TTree*)f->Get("t1");
    
    std::vector<int> *pid= 0;
    std::vector<float> *px= 0;
    std::vector<float> *py= 0;
    std::vector<float> *pz= 0;
    std::vector<float> *Vt= 0;
    std::vector<float> *Vx= 0;
    std::vector<float> *Vy= 0;
    std::vector<float> *Vz= 0;
    
    t1->SetBranchAddress("pid",&pid);
    t1->SetBranchAddress("px",&px);
    t1->SetBranchAddress("py",&py);
    t1->SetBranchAddress("pz",&pz);
    t1->SetBranchAddress("Vt",&Vt);
    t1->SetBranchAddress("Vx",&Vx);
    t1->SetBranchAddress("Vy",&Vy);
    t1->SetBranchAddress("Vz",&Vz);
    
    Int_t nevent = t1->GetEntries();
    //Int_t nevent = 20;
    
    //create the output file, the Tree and a few branches
    std::vector<int> pid2, nstubs;
    std::vector<float> px2, py2, pz2;
    std::vector<float> R0, d0, phi0, t, z0;
    std::vector<float> Vt2, Vx2, Vy2, Vz2, VxReco, VyReco, VzReco, Deltaxy, Deltaz;
    std::vector<double> pxReco, pyReco, pzReco;
    std::vector<double> preco;
    std::vector<double> distance;
    std::vector<double> distanceT;
  
    //TFile fout("test.root","recreate");
    TFile fout(argv[2],"recreate");
    TTree t2("t2","tracks with reco vertex");
    t2.Branch("pid","std::vector",&pid2);
    t2.Branch("px","std::vector",&px2);
    t2.Branch("py","std::vector",&py2);
    t2.Branch("pz","std::vector",&pz2);
    
    t2.Branch("Vx","std::vector",&Vx2);
    t2.Branch("Vy","std::vector",&Vy2);
    t2.Branch("Vz","std::vector",&Vz2);
    
    t2.Branch("R0","std::vector",&R0);
    t2.Branch("d0","std::vector",&d0);
    t2.Branch("phi0","std::vector",&phi0);
    t2.Branch("t","std::vector",&t);
    t2.Branch("z0","std::vector",&z0);
    t2.Branch("nstubs","std::vector",&nstubs);
    
    t2.Branch("VxReco","std::vector",&VxReco);
    t2.Branch("VyReco","std::vector",&VyReco);
    t2.Branch("VzReco","std::vector",&VzReco);
    t2.Branch("Deltaxy","std::vector",&Deltaxy);
    t2.Branch("Deltaz","std::vector",&Deltaz);
    
    t2.Branch("pxReco","std::vector",&pxReco);
    t2.Branch("pyReco","std::vector",&pyReco);
    t2.Branch("pzReco","std::vector",&pzReco);
    
    // theorist friendly format
    ofstream outfile;
    outfile.open(datfilename);
    outfile <<"event\ttrack\tid\tpx\tpy\tpz\tR0\td0\tphi0\tt\tz0\tnstubs\tVx\tVy\tVz\tVxReco\tVyReco\tVzReco\tDeltaxy\tDeltaz\tpxReco\tpyReco\tpzReco\n";  
    
    // auxiliary objects
    CMSTrackTrig_Utils::Vtx vertex;
    std::vector<trigfit> tracks;
    std::vector<trigfit> leadingtracks;
    trigfit trigfit1; 
  
    // initialize random nr generator, for vertex throwing
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0.0,1.0);
    double Lxy, alp_pT, alp_Vx, alp_Vy, alp_Vz;
  
    //for (Int_t i=0;i<1;i++) {
    for (Int_t i=0;i<nevent;i++) {
        
        t1->GetEvent(i);
        double nparticles = pid->size();
      
        // add the alp
        for(int j = 0; j<nparticles ;j++){
            if(54 == std::abs(pid->at(j)) || 511 == std::abs(pid->at(j)) || 521 == std::abs(pid->at(j))){
            
                pid2.push_back(pid->at(j));
                px2.push_back(px->at(j));
                py2.push_back(py->at(j));
                pz2.push_back(pz->at(j));
            
                Vx2.push_back(0.0);
                Vy2.push_back(0.0);
                Vz2.push_back(0.0);
          
                R0.push_back(0.0);
                d0.push_back(0.0);
                phi0.push_back(0.0);
                t.push_back(0.0);
                z0.push_back(0.0);
                nstubs.push_back(0.0);
          
                VxReco.push_back(0.0);
                VyReco.push_back(0.0);
                VzReco.push_back(0.0);
            
                Deltaxy.push_back(0.0);
                Deltaz.push_back(0.0);
          
                pxReco.push_back(0.0);
                pyReco.push_back(0.0);
                pzReco.push_back(0.0);
              
                alp_pT=std::sqrt((px->at(j))*(px->at(j))+(py->at(j))*(py->at(j)));
                Lxy=50.0*distribution(generator); // Lxy between 0 and 50 cm
                alp_Vx=(px->at(j))/alp_pT*Lxy;
                alp_Vy=(py->at(j))/alp_pT*Lxy;
                alp_Vz=(pz->at(j))/alp_pT*Lxy;
              
                
            } 
        }
        //If there is no LLP or B in the sample, add a dummy line to catch future exceptions
        if(nparticles==0){
            
                pid2.push_back(0);
                px2.push_back(0);
                py2.push_back(0);
                pz2.push_back(0);
            
                Vx2.push_back(0.0);
                Vy2.push_back(0.0);
                Vz2.push_back(0.0);
          
                R0.push_back(0.0);
                d0.push_back(0.0);
                phi0.push_back(0.0);
                t.push_back(0.0);
                z0.push_back(0.0);
                nstubs.push_back(0.0);
          
                VxReco.push_back(0.0);
                VyReco.push_back(0.0);
                VzReco.push_back(0.0);
            
                Deltaxy.push_back(0.0);
                Deltaz.push_back(0.0);
          
                pxReco.push_back(0.0);
                pyReco.push_back(0.0);
                pzReco.push_back(0.0);
            }
      
        
        //select all the reconstructed tracks
        for(int j = 0; j<nparticles ;j++){     
            if(211 == std::abs(pid->at(j)) || 321 == std::abs(pid->at(j))|| 2212 == std::abs(pid->at(j)) || 11 == std::abs(pid->at(j)) || 13 == std::abs(pid->at(j))){
                trigfit1.track(px->at(j),py->at(j),pz->at(j), alp_Vx, alp_Vy, alp_Vz,-sgn(pid->at(j)));
                trigfit1.fit(2); // smearing + multiple scattering
                if(std::abs(1.0/trigfit1.frinv())<100000.){
                  tracks.push_back(trigfit1);
                }
            }
        }
      
        //sort tracks according to reconstructed pT
        for(int j = 0; j<tracks.size() ;j++){ 
          for(int k = j; k<tracks.size() ;k++){
            if(tracks[j].get_track_pt() < tracks[k].get_track_pt()){
              swap(tracks[j], tracks[k]);
            }
          }
        }
      
        //write root tree
        // if less than two tracks, don't botter making vertex
        if(2>tracks.size()) {
          
          for(int j = 0; j<tracks.size() ;j++){
            
              pid2.push_back(tracks.at(j).get_charge());
              px2.push_back(tracks.at(j).get_true_px());
              py2.push_back(tracks.at(j).get_true_py());
              pz2.push_back(tracks.at(j).get_true_pz());
            
              Vx2.push_back(tracks.at(j).get_true_vx());
              Vy2.push_back(tracks.at(j).get_true_vy());
              Vz2.push_back(tracks.at(j).get_true_vz());
          
              R0.push_back(1.0/tracks.at(j).frinv());
              d0.push_back(tracks.at(j).fd0());
              phi0.push_back(tracks.at(j).fphi0());
              t.push_back(tracks.at(j).ft());
              z0.push_back(tracks.at(j).fz0());
              nstubs.push_back(tracks.at(j).n());
          
              VxReco.push_back(0.0);
              VyReco.push_back(0.0);
              VzReco.push_back(0.0);
            
              Deltaxy.push_back(0.0);
              Deltaz.push_back(0.0);
          
              pxReco.push_back(0.0);
              pyReco.push_back(0.0);
              pzReco.push_back(0.0);
          }
        }
        else{// if two or more tracks
          
          //find vertex of two leading tracks
          leadingtracks.push_back(tracks.at(0));
          leadingtracks.push_back(tracks.at(1));
          vertex = CMSTrackTrig_Utils::estimateVertexPosition( leadingtracks );
          
          
          
          //write root tree
          for(int j = 0; j<tracks.size() ;j++){
            
              pid2.push_back(tracks.at(j).get_charge());
              px2.push_back(tracks.at(j).get_true_px());
              py2.push_back(tracks.at(j).get_true_py());
              pz2.push_back(tracks.at(j).get_true_pz());
            
              Vx2.push_back(tracks.at(j).get_true_vx());
              Vy2.push_back(tracks.at(j).get_true_vy());
              Vz2.push_back(tracks.at(j).get_true_vz());
          
              R0.push_back(1.0/tracks.at(j).frinv());
              d0.push_back(tracks.at(j).fd0());
              phi0.push_back(tracks.at(j).fphi0());
              t.push_back(tracks.at(j).ft());
              z0.push_back(tracks.at(j).fz0());
              nstubs.push_back(tracks.at(j).n());
          
              VxReco.push_back(vertex.x);
              VyReco.push_back(vertex.y);
              VzReco.push_back(vertex.z);
            
              if(j<2){
                Deltaxy.push_back(0.5*vertex.Deltaxy); //vertex.Deltaxy was defined as the distance between the two tracks. The distance from a track to the vertex is half of this. 
                Deltaz.push_back(0.5*vertex.Deltaz); //vertex.Deltaz was defined as the distance between the two tracks. The distance from a track to the vertex is half of this. 
              }
              else{
                Deltaxy.push_back(CMSTrackTrig_Utils::transverseDistanceToVertex(tracks.at(j),vertex));
                Deltaz.push_back(CMSTrackTrig_Utils::zDistanceToVertex(tracks.at(j),vertex));
              }
            
              preco = CMSTrackTrig_Utils::recoMomentum(tracks.at(j),vertex);//there is no "track" object for the mother, need to decrease index
              pxReco.push_back(preco.at(0));
              pyReco.push_back(preco.at(1));
              pzReco.push_back(preco.at(2));
            
          }
        }
        
    
        t2.Fill();
        std::cout <<i << "\n"; 
        std::cout << "nr tracks: "<<tracks.size() <<"\n";
        //write to theorist txt file
        for(int j = 0; j<tracks.size()+1 ;j++){//+1 is to account for the mother, either LLP or B
            outfile << i << "\t";
            outfile << j << "\t";
          
            outfile << pid2.at(j) << "\t";
            outfile << px2.at(j) << "\t";
            outfile << py2.at(j) << "\t";
            outfile << pz2.at(j) << "\t";
          
            outfile << R0.at(j) << "\t";
            outfile << d0.at(j) << "\t";
            outfile << phi0.at(j) << "\t";
            outfile << t.at(j) << "\t";
            outfile << z0.at(j) << "\t";
            outfile << nstubs.at(j) << "\t";
          
            outfile << Vx2.at(j) << "\t";
            outfile << Vy2.at(j) << "\t";
            outfile << Vz2.at(j) << "\t";
          
            outfile << VxReco.at(j) << "\t";
            outfile << VyReco.at(j) << "\t";
            outfile << VzReco.at(j) << "\t";
            outfile << Deltaxy.at(j) << "\t";
            outfile << Deltaz.at(j) << "\t";
          
            outfile << pxReco.at(j) << "\t";
            outfile << pyReco.at(j) << "\t";
            outfile << pzReco.at(j) << "\n";
              
        }
        
        pid2.clear();
        px2.clear();
        py2.clear();
        pz2.clear();
        
        Vx2.clear();
        Vy2.clear();
        Vz2.clear();
        
        R0.clear();
        d0.clear();
        phi0.clear();
        t.clear();
        z0.clear();
        nstubs.clear();
        
        VxReco.clear();
        VyReco.clear();
        VzReco.clear();
        Deltaxy.clear();
        Deltaz.clear();
        
        pxReco.clear();
        pyReco.clear();
        pzReco.clear();
        
        tracks.clear();
        leadingtracks.clear();
        
        }
    
    t2.Write();
    outfile.close();
    
    return 0;

}

  