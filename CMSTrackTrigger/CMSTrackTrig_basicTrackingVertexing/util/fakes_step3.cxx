// This needs root/6.04.18. Lower versions result in a seg fault on GetEvent. Probably related to the declarations of the pid pointers etc

//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

//custom
#include "utils.h"
#include "trigfit.h"

using namespace std;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

template <typename T> string tostr(const T& t) { 
   std::stringstream os; 
   os<<t; 
   return os.str(); 
} 

int main(int argc, char *argv[]){
        
    if(!(argc==2)){
    std::cout << "I need inputfile name";
    return 0;
  }
    std::string  infilename = tostr(argv[1]);
    std::string  outfilename=infilename;
    outfilename.replace(10,1,"3");
    
    cout << infilename <<"\n";
    cout << outfilename <<"\n";
    
    // open files
    std::fstream infile("/home/data1/knapen/CMS_track_trigger/fakes/step3/"+infilename,std::ios_base::in);
    ofstream outfile;
    outfile.open("/home/data1/knapen/CMS_track_trigger/fakes/step3/"+outfilename);
    
    // auxiliary objects
    CMSTrackTrig_Utils::Vtx vertex;
    std::vector<trigfit> tracks, leadingtracks;
    trigfit trk;
    
    // definitions
    double eventnr, R0, d0, phi0, t, z0, nstub;
    std::vector<double> preco;
    double current_event=0.0;
  
    while (infile >> eventnr >> R0 >> d0 >> phi0 >> t >> z0 >> nstub)
    {
        if(eventnr>current_event){
          std::cout << eventnr << "\t"<<tracks.size() <<"\n";
        
          //find vertex of two leading tracks. They should be pT ordered already, done in python part
          leadingtracks.push_back(tracks.at(0));
          leadingtracks.push_back(tracks.at(1));
          vertex = CMSTrackTrig_Utils::estimateVertexPosition( leadingtracks );
          
          
          //write the tree tree
          for(int j = 0; j<tracks.size() ;j++){
            
              outfile << current_event << "\t";
              outfile << j << "\t";
          
              outfile << "0.0" << "\t";
              outfile << "0.0" << "\t";
              outfile << "0.0" << "\t";
              outfile << "0.0" << "\t";
          
              outfile << 1.0/tracks.at(j).frinv()   << "\t";
              outfile << tracks.at(j).fd0()   << "\t";
              outfile << tracks.at(j).fphi0() << "\t";
              outfile << tracks.at(j).ft()    << "\t";
              outfile << tracks.at(j).fz0()   << "\t";
              outfile << tracks.at(j).n() << "\t";
          
              outfile << "0.0" << "\t";
              outfile << "0.0" << "\t";
              outfile << "0.0" << "\t";
              outfile << "0.0" << "\t";
          
              outfile << vertex.x << "\t";
              outfile << vertex.y << "\t";
              outfile << vertex.z << "\t";
            
              //vertex.Deltaxy and vertex.Deltaz were defined as the distance between the two tracks. The distance from a track to the vertex is half of this.
              if(j<2){
                outfile << 0.5*vertex.Deltaxy << "\t";
                outfile << 0.5*vertex.Deltaz << "\t";
              }
              else{
                outfile << CMSTrackTrig_Utils::transverseDistanceToVertex(tracks.at(j),vertex) << "\t";
                outfile << CMSTrackTrig_Utils::zDistanceToVertex(tracks.at(j),vertex) << "\t";
              }
              
              preco = CMSTrackTrig_Utils::recoMomentum(tracks.at(j),vertex);
          
              outfile << preco.at(0) << "\t";
              outfile << preco.at(1) << "\t";
              outfile << preco.at(2) << "\t";
            
              outfile << "0.0" << "\n";  //distance variable, don't care
          }
          
          
          // reset vectors
          tracks.clear();
          leadingtracks.clear();          
          
          // counter
          current_event=eventnr;
        }
        
      
        // read line
        trk.track(1.0/R0, phi0, d0, t, z0, nstub);
        tracks.push_back(trk);
      
        
          
          
          
      

      
      
      
    }
    outfile.close(); 


    
    return 0;

}



    