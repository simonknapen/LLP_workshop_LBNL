//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
 
//custom
#include "utils.h"
#include "trigfit.h"

// There appears to be a bug related to initializing the fitter on line 110 of "trigfit.h":
// Consider the following code:


int main(){
    
    //construct two trigfit objects
    trigfit trigfit1, trigfit2;

                
    // create a track on the first trigfit object, originating at the origin
    trigfit1.track(10.0 , 10.0 , 10.0, 0.0, 0.0, 0.0,1);
    trigfit1.fit();
    //this prints out -2.4069e-08 for the distance of closest approach, d0, close to zero as it should  
    std::cout << trigfit1.fd0() << "\n";
    
    //create a track in the second trigfit object, also orinating at the origin
    trigfit2.track(20.0 , 10.0 , 10.0, 0.0, 0.0, 0.0,1);
    trigfit2.fit();
    //this prints out 2.26279e-11 for the distance of closest approach, d0, close to zero as it should 
    std::cout << trigfit2.fd0() << "\n";
    
    //create a new track on the first trigfit object, IDENTICAL to the original track. The only difference is that "trigfit1" now has been initialized.
    trigfit1.track(10.0 , 10.0 , 10.0, 0.0, 0.0, 0.0,1);
    trigfit1.fit();
    //this prints out -5.17283 for the distance of closest approach, d0, which is incorrect  
    std::cout << trigfit1.fd0() << "\n";
    
        
    // The problem goes away if there is only 1 trigfit object in the routine, or if I always run "reinitialize()" in the "track" routine, regardless on whether the trigfit object has already been reinitialized. I am guessing there is a memory effect in the "TF1" object on line 110 of trigfit.h. I don't know why the presence of a second trigfit object matters.
    
    return 0;

}
