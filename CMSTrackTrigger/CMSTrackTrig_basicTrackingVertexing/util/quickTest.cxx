//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

//custom
#include "utils.h"
#include "trigfit.h"

int main(){
    
    std::vector<trigfit> tracks;

 
    
    double vx;
    double vy;
    double vz;
    double px1, px2;
    double py1, py2;
    double pz1, pz2;
    CMSTrackTrig_Utils::Vtx vertex;
    trigfit trigfit1, trigfit2;
    

    
    //track (double px, double py, double pz, double vx, double vy, double vz, int charge)
    trigfit1.track(1.0/7219.29, 2.89315, 2.74094, -1.84589, 9.11693, 5);
    //trigfit1.fit();
    tracks.push_back(trigfit1);  
    //std::cout  <<1.0/trigfit1.frinv()<<"\t"<<trigfit1.fphi0()<<"\t"<<trigfit1.fd0() <<"\t"<< trigfit1.ft() << "\t : "<< pz1/sqrt(px1*px1+py1*py1)<< "\n";
        
    //track (double px, double py, double pz, double vx, double vy, double vz, int charge)
    trigfit2.track(-1.0/2538.75, 4.70313, -1.6979, -1.28741, 5.58312, 5);
    //trigfit2.fit();
    tracks.push_back(trigfit2); 
    //std::cout << 1.0/trigfit2.frinv()<<"\t"<<trigfit2.fphi0()<<"\t"<<trigfit2.fd0() <<"\t"<< trigfit2.ft() << "\t : "<< pz2/sqrt(px2*px2+py2*py2)<< "\n";
        
    vertex = CMSTrackTrig_Utils::estimateVertexPosition( tracks );
    std::cout << vertex.x << ", " << vertex.y << ", " << vertex.z << "\n";
        
        
        
     
    
    return 0;

}
