//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

//root
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TBasket.h"
#include "TBrowser.h"
#include "TH2.h"
#include "TRandom.h"

//custom
#include "utils.h"
#include "trigfit.h"

using namespace std;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

template <typename T> string tostr(const T& t) { 
   std::stringstream os; 
   os<<t; 
   return os.str(); 
} 

int main(int argc, char *argv[]){
        
    if(!(argc==3)){
    std::cout << "I need inputfile name and outputfile name";
    return 0;
  }
    gROOT->ProcessLine("#include <vector>");
    
    std::string  datfilename = tostr(argv[2]);
    datfilename.erase(datfilename.end()-5, datfilename.end()); 
    datfilename = datfilename + ".dat";
    
    cout << datfilename <<"\n";
    
    //read the input file, create the Tree and a few branches
    TFile *f = new TFile(argv[1]);
    TTree *t1 = (TTree*)f->Get("t1");
    
    std::vector<int> *pid= 0;
    std::vector<float> *px= 0;
    std::vector<float> *py= 0;
    std::vector<float> *pz= 0;
    std::vector<float> *Vt= 0;
    std::vector<float> *Vx= 0;
    std::vector<float> *Vy= 0;
    std::vector<float> *Vz= 0;
    
    
    t1->SetBranchAddress("pid",&pid);
    t1->SetBranchAddress("px",&px);
    t1->SetBranchAddress("py",&py);
    t1->SetBranchAddress("pz",&pz);
    t1->SetBranchAddress("Vt",&Vt);
    t1->SetBranchAddress("Vx",&Vx);
    t1->SetBranchAddress("Vy",&Vy);
    t1->SetBranchAddress("Vz",&Vz);
    
    Int_t nevent = t1->GetEntries();
    //Int_t nevent = 3;
    
    //create the output file, the Tree and a few branches
    std::vector<int> pid2, nstubs;
    std::vector<float> px2, py2, pz2;
    std::vector<float> R0, d0, phi0, t, z0;
    std::vector<float> Vt2, Vx2, Vy2, Vz2, VxReco, VyReco, VzReco, Deltaxy, Deltaz;
    std::vector<double> pxReco, pyReco, pzReco;
    std::vector<double> preco;
    std::vector<double> distance;
    
    //TFile fout("test.root","recreate");
    TFile fout(argv[2],"recreate");
    TTree t2("t2","tracks with reco vertex");
    t2.Branch("pid","std::vector",&pid2);
    t2.Branch("px","std::vector",&px2);
    t2.Branch("py","std::vector",&py2);
    t2.Branch("pz","std::vector",&pz2);
    
    t2.Branch("Vt","std::vector",&Vt2);
    t2.Branch("Vx","std::vector",&Vx2);
    t2.Branch("Vy","std::vector",&Vy2);
    t2.Branch("Vz","std::vector",&Vz2);
    
    t2.Branch("R0","std::vector",&R0);
    t2.Branch("d0","std::vector",&d0);
    t2.Branch("phi0","std::vector",&phi0);
    t2.Branch("t","std::vector",&t);
    t2.Branch("z0","std::vector",&z0);
    t2.Branch("nstubs","std::vector",&nstubs);
    
    t2.Branch("VxReco","std::vector",&VxReco);
    t2.Branch("VyReco","std::vector",&VyReco);
    t2.Branch("VzReco","std::vector",&VzReco);
    t2.Branch("Deltaxy","std::vector",&Deltaxy);
    t2.Branch("Deltaz","std::vector",&Deltaz);
    
    t2.Branch("pxReco","std::vector",&pxReco);
    t2.Branch("pyReco","std::vector",&pyReco);
    t2.Branch("pzReco","std::vector",&pzReco);
    
    t2.Branch("distance","std::vector",&distance);
    
    // theorist friendly format
    ofstream outfile;
    outfile.open(datfilename);
    
    // auxiliary objects
    CMSTrackTrig_Utils::Vtx vertex;
    std::vector<trigfit> tracks;
    trigfit trigfit1;
 
    //for (Int_t i=0;i<1;i++) {
    for (Int_t i=0;i<nevent;i++) {
        std::cout << i<<"\n";
        t1->GetEvent(i);
        
        // so far only works for events with exactly 3 particles!
        for(int j = 0; j< 3 ;j++){
             
        // so far only works for decay to two muons
            if(211 == std::abs(pid->at(j))){
                trigfit1.track(px->at(j),py->at(j),pz->at(j), Vx->at(j), Vy->at(j), Vz->at(j),-sgn(pid->at(j)));
                trigfit1.fit();
                tracks.push_back(trigfit1);
                
                R0.push_back(1.0/trigfit1.frinv());
                d0.push_back(trigfit1.fd0());
                phi0.push_back(trigfit1.fphi0());
                t.push_back(trigfit1.ft());
                z0.push_back(trigfit1.fz0());
                nstubs.push_back(trigfit1.n());
                
                //std::cout << "pz/pT:" << (pz->at(j))/std::sqrt((px->at(j))*(px->at(j))+(py->at(j))*(py->at(j))) <<"\n";
                //std::cout << "t:" << t[j]<<"\n";
                
            }
            else{ // there is no track for the mother, so return 0.0
                R0.push_back(0.0);
                d0.push_back(0.0);
                phi0.push_back(0.0);
                t.push_back(0.0);
                z0.push_back(0.0);
                nstubs.push_back(0);
            }
          
        }
        //std::cout << "angle: \t"<< ((px->at(1))*(px->at(2))+(py->at(1))*(py->at(2)))/std::sqrt((px->at(1))*(px->at(1))+(py->at(1))*(py->at(1)))/std::sqrt((px->at(2))*(px->at(2))+(py->at(2))*(py->at(2)))<<"\n";
        
        //trigfit2.track(px->at(1),py->at(1),pz->at(1), Vx->at(1), Vy->at(1), Vz->at(1),-1);
        //trigfit2.fit();
        // tracks.push_back(trigfit2);
        
        vertex = CMSTrackTrig_Utils::estimateVertexPosition( tracks );
        
        for(int j = 0; j<3 ;j++){
            
            //pid2[j]= pid->at(j);
            pid2.push_back(pid->at(j));
            px2.push_back(px->at(j));
            py2.push_back(py->at(j));
            pz2.push_back(pz->at(j));
            
            Vt2.push_back(Vt->at(j));
            Vx2.push_back(Vx->at(j));
            Vy2.push_back(Vy->at(j));
            Vz2.push_back(Vz->at(j));
    
            
            
            
            if(211 == std::abs(pid->at(j))){
                VxReco.push_back(vertex.x);
                VyReco.push_back(vertex.y);
                VzReco.push_back(vertex.z);
                Deltaxy.push_back(vertex.Deltaxy);
                Deltaz.push_back(vertex.Deltaz);
                
                preco = CMSTrackTrig_Utils::recoMomentum(tracks[j-1], vertex);//there is no "track" object for the mother, need to decrease index
                
                pxReco.push_back(preco[0]);
                pyReco.push_back(preco[1]);
                pzReco.push_back(preco[2]);
                
                
                
            }
            else{
                VxReco.push_back(Vx->at(j));
                VyReco.push_back(Vy->at(j));
                VzReco.push_back(Vz->at(j));
                Deltaxy.push_back(0.0);
                Deltaz.push_back(0.0);
                
                //placeholder, will be overwritten
                pxReco.push_back(0.0);
                pyReco.push_back(0.0);
                pzReco.push_back(0.0);
                
            }    
        }
        
        //compute mother reco momentum
        pxReco.at(0) = pxReco[1] + pxReco[2];
        pyReco.at(0) = pyReco[1] + pyReco[2];
        pzReco.at(0) = pzReco[1] + pzReco[2];
        preco.at(0)= pxReco[0];
        preco.at(1) = pyReco[0];
        preco.at(2) = pzReco[0];
        
        //compute the distance of the mother track to the primary vertex
        distance.push_back(0.0);
        distance.push_back(CMSTrackTrig_Utils::closestApproach(preco, vertex));
        distance.push_back(distance[1]);
        //std::cout << CMSTrackTrig_Utils::dot(preco,preco) << "\n";
        
        t2.Fill();
        for(int j = 0; j<3 ;j++){
            outfile << i <<"\t" << j<< "\t" << pid->at(j) <<"\t";
            outfile << px->at(j) << "\t" << py->at(j) << "\t" << pz->at(j) << "\t";
            outfile << R0[j] << "\t" << d0[j] << "\t" <<phi0[j] << "\t" << t[j] << "\t" << z0[j] <<"\t" <<nstubs[j] << "\t";
            outfile << Vt->at(j) << "\t" << Vx->at(j) << "\t" << Vy->at(j) << "\t" << Vz->at(j) << "\t";
            if( j == 0){
                outfile << 0.0 << "\t" <<0.0 << "\t" << 0.0 << "\t";
                outfile << 0.0 << "\t" << 0.0    << "\t";
            }
            else{
                outfile << vertex.x << "\t" <<vertex.y << "\t" << vertex.z << "\t";
                outfile << vertex.Deltaxy << "\t" << vertex.Deltaz    << "\t";
            }
            
            outfile << pxReco[j] << "\t" << pyReco[j] << "\t" << pzReco[j] << "\t";
            outfile << distance[j] << "\n";
        }
        
        pid2.clear();
        px2.clear();
        py2.clear();
        pz2.clear();
        
        Vt2.clear();
        Vx2.clear();
        Vy2.clear();
        Vz2.clear();
        
        R0.clear();
        d0.clear();
        phi0.clear();
        t.clear();
        z0.clear();
        nstubs.clear();
        
        VxReco.clear();
        VyReco.clear();
        VzReco.clear();
        Deltaxy.clear();
        Deltaz.clear();
        
        pxReco.clear();
        pyReco.clear();
        pzReco.clear();
        
        distance.clear();
        
        tracks.clear();
        
        }
    
    t2.Write();
    outfile.close();
    
    return 0;

}



    