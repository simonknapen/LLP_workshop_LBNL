//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

//root
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TBasket.h"
#include "TBrowser.h"
#include "TH2.h"
#include "TRandom.h"

//custom
#include "utils.h"
#include "trigfit.h"

using namespace std;

#include "throwtrack.C"


template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

template <typename T> string tostr(const T& t) { 
   std::stringstream os; 
   os<<t; 
   return os.str(); 
} 

int main(int argc, char *argv[]){
        
    if(!(argc==3)){
    std::cout << "need number of events and a run number\n";
    return 0;
  }
    gROOT->ProcessLine("#include <vector>");

    int nevent = atoi(argv[1]);
    int randomseed = atoi(argv[2]);

    printf("making %i events \n",nevent);

    //create the output file, the Tree and a few branches
    string fname = "/home/data1/knapen/CMS_track_trigger/fakes/fakes_"+tostr(nevent)+"_"+tostr(randomseed)+".root";
    TFile fout(fname.c_str(),"recreate");
    TTree *tt = new TTree("tt","");
    
     // theorist friendly format
    ofstream outfile;
    outfile.open("/home/data1/knapen/CMS_track_trigger/fakes/fakes_"+tostr(nevent)+"_"+tostr(randomseed)+".dat");
    
    double px1, py1, pz1, px2, py2, pz2;
    tt->Branch("px1",&px1);
    tt->Branch("py1",&py1);
    tt->Branch("pz1",&pz1);
    tt->Branch("px2",&px2);
    tt->Branch("py2",&py2);
    tt->Branch("pz2",&pz2);

    int nstub1; // reco track 1
    double rinv1,phi01,d01,t1,z01;
    tt->Branch("nstub1",&nstub1);
    tt->Branch("rinv1",&rinv1);
    tt->Branch("phi01",&phi01);
    tt->Branch("d01",  &d01);
    tt->Branch("t1",   &t1);
    tt->Branch("z01",  &z01);
    
    int nstub2; //reco track 2
    double rinv2,phi02,d02,t2,z02;
    tt->Branch("nstub2",&nstub2);
    tt->Branch("rinv2",&rinv2);
    tt->Branch("phi02",&phi02);
    tt->Branch("d02",  &d02);
    tt->Branch("t2",   &t2);
    tt->Branch("z02",  &z02);
    
    double sd01, sd02; //errors on d0
    tt->Branch("sd01",  &sd01);
    tt->Branch("sd02",  &sd02);

    //reconstructed vertex
    double VxReco, VyReco, VzReco, Deltaxy, Deltaz;
    double vpx, vpy, vpz;
    tt->Branch("VxReco",&VxReco);
    tt->Branch("VyReco",&VyReco);
    tt->Branch("VzReco",&VzReco);
    tt->Branch("Deltaxy",&Deltaxy);
    tt->Branch("Deltaz",&Deltaz); 

    double theta2; //2-d opening angle at vertex 
    double theta3; //3-d opening angle at vertex 
    double alpha;  //2-d angle between the vertex and pT at vertex
    double m_mu, m_pi; // invariant masses assuming muon or pion masses
    double distance; // closest approach to primary vertex
    std::vector<double> preco; //reconstructed 3 momentum of mother

    tt->Branch("theta2",&theta2);
    tt->Branch("theta3",&theta3);
    tt->Branch("alpha",&alpha);
    tt->Branch("m_mu",&m_mu);
    tt->Branch("m_pi",&m_pi);
    tt->Branch("distance",&distance);
        
    // auxiliary objects
    CMSTrackTrig_Utils::Vtx vertex;
    std::vector<trigfit> tracks;
    trigfit trk;
    TRandom3 rr;
    rr.SetSeed(randomseed);
    
    double rinv_max = 0.0057; // 2 GeV @ 3.8 T 
    double d0_max = 20;
    double t_max = 2;
    double z0_max = 20;
    for (Int_t i=0;i<nevent;i++) {
        
        if ( i % 100000 == 0 )
        {
        cout << i << " events done \n ";
        }    
        
      tracks.clear();
      throwtrack(&rr, rinv1, phi01, d01, t1, z01, nstub1);
      trk.track(rinv1, phi01, d01, t1, z01, nstub1);
      tracks.push_back(trk);

      do{
	throwtrack(&rr, rinv2, phi02, d02, t2, z02, nstub2);
      } while (rinv1*rinv2>0);
      trk.track(rinv2, phi02, d02, t2, z02, nstub2);
      tracks.push_back(trk);
            
      if(tracks.size()!=2){
	printf("not two muons! %i\n",(int)tracks.size());
	continue;
      }

      px1 = tracks[0].getpx();
      py1 = tracks[0].getpy();
      pz1 = tracks[0].getpz();
      px2 = tracks[1].getpx();
      py2 = tracks[1].getpy();
      pz2 = tracks[1].getpz();

      nstub1 = tracks[0].n();
      rinv1  = tracks[0].frinv();
      phi01  = tracks[0].fphi0();
      d01    = tracks[0].fd0();
      t1     = tracks[0].ft();
      z01    = tracks[0].fz0();
      
      nstub2 = tracks[1].n();
      rinv2  = tracks[1].frinv();
      phi02  = tracks[1].fphi0();
      d02    = tracks[1].fd0();
      t2     = tracks[1].ft();
      z02    = tracks[1].fz0();
      
      sd01   = tracks[0].n() >3 ? tracks[0].get_sigma() : 0;
      sd02   = tracks[1].n() >3 ? tracks[1].get_sigma() : 0;

      if(tracks[0].n() >3 && tracks[1].n() >3) {
      
	vertex = CMSTrackTrig_Utils::estimateVertexPosition( tracks );

	VxReco = vertex.x;
	VyReco = vertex.y;
	VzReco = vertex.z;
	Deltaxy = vertex.Deltaxy;
	Deltaz = vertex.Deltaz;

	double rvtx = sqrt(VxReco*VxReco+VyReco*VyReco); 
	double ptx1 = tracks[0].get_track_px(rvtx);
	double pty1 = tracks[0].get_track_py(rvtx);
	double ptz1 = tracks[0].get_track_pz();
	
	double ptx2 = tracks[1].get_track_px(rvtx);
	double pty2 = tracks[1].get_track_py(rvtx);
	double ptz2 = tracks[1].get_track_pz();
	
	double pt1sq = (ptx1*ptx1+pty1*pty1);
	double pt2sq = (ptx2*ptx2+pty2*pty2);
	double p1sq = (ptx1*ptx1+pty1*pty1+ptz1*ptz1);
	double p2sq = (ptx2*ptx2+pty2*pty2+ptz2*ptz2);
	
	theta2 = (ptx1*ptx2+pty1*pty2)/sqrt(pt1sq)/sqrt(pt2sq);
	theta3 = (ptx1*ptx2+pty1*pty2+ptz1*ptz2)/sqrt(p1sq)/sqrt(p2sq);

	vpx = ptx1+ptx2;
	vpy = pty1+pty2;
	vpz = ptz1+ptz2;
    
          
	alpha  = (vpx*VxReco+vpy*VyReco)/rvtx/sqrt(vpx*vpx+vpy*vpy);
	
	m_mu = pow(sqrt(0.10*0.10+p1sq)+sqrt(0.10*0.10+p2sq),2) - vpx*vpx - vpy*vpy -vpz*vpz;
	m_pi = pow(sqrt(0.14*0.14+p1sq)+sqrt(0.14*0.14+p2sq),2) - vpx*vpx - vpy*vpy -vpz*vpz;
	
	m_mu = sqrt(m_mu);
	m_pi = sqrt(m_pi);
    
    preco.push_back(vpx);
    preco.push_back(vpy);
    preco.push_back(vpz);      
    distance = CMSTrackTrig_Utils::closestApproach(preco, vertex);      
    preco.clear();      
          
      }
      else{

	VxReco = 0;
	VyReco = 0;
	VzReco = 0;
	Deltaxy = -99;
	Deltaz  = -99;
	theta2 = 0;
	theta3 = 0;
	alpha = 0;
	m_mu = 0;
	m_pi = 0;

      }	
        
    if (Deltaxy<0.2 && Deltaz < 1.0){    //to save disc space, only write out if vertex is kinda decent
        
    tt->Fill();    
        
    // write in theorist format
    // track 1    
    outfile << i <<"\t" << 1<< "\t" << "0" <<"\t";    // set PDG code to 0
    outfile << 0 << "\t" << 0 << "\t" << 0 << "\t"; // set truth momentum to zero
    outfile << 1.0/rinv1 << "\t" <<d01 << "\t" <<phi01 << "\t" << t1 << "\t" << z01 <<"\t" <<nstub1 << "\t";
    outfile << "nan" << "\t" << "nan" << "\t" << "nan" << "\t" << "nan" << "\t";    // set "truth-level" vertex to nan
    outfile << VxReco << "\t" <<VyReco << "\t" << VzReco << "\t";
    outfile << Deltaxy << "\t" << Deltaz    << "\t";
    outfile << vpx << "\t" << vpy << "\t" << vpz << "\t";
    outfile << distance << "\n";
        
    // track 2    
    outfile << i <<"\t" << 2<< "\t" << "0" <<"\t";    // set PDG code to 0
    outfile << 0 << "\t" << 0 << "\t" << 0 << "\t"; // set truth momentum to zero
    outfile << 1.0/rinv2 << "\t" <<d02 << "\t" <<phi02 << "\t" << t2 << "\t" << z02 <<"\t" <<nstub1 << "\t";
    outfile << "nan" << "\t" << "nan" << "\t" << "nan" << "\t" << "nan" << "\t";    // set "truth-level" vertex to nan
    outfile << VxReco << "\t" <<VyReco << "\t" << VzReco << "\t";
    outfile << Deltaxy << "\t" << Deltaz    << "\t";
    outfile << vpx << "\t" << vpy << "\t" << vpz << "\t";
    outfile << distance << "\n";    
    }
        
    }
    
    tt->Write();
    outfile.close();
    return 0;

} 
 


    
