//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <cstdlib>
#include <ctime>

//custom
#include "utils.h"
#include "trigfit.h"

static double centerx(double R0, double phi0, double d0) {
    
    return -1.0*(R0+d0)*sin(phi0);

}

static double centery(double R0, double phi0, double d0) {
    
    return 1.0*(R0+d0)*cos(phi0);

}


static double Vx(double px, double py, double R0, double phi0, double d0) {
    
    return centerx(R0,phi0,d0)+R0*sin(atan2(py,px));

}

static double Vy(double px, double py, double R0, double phi0, double d0) {
    
    return centery(R0,phi0,d0)-R0*cos(atan2(py,px));

}

static std::vector<double> ClosestApproach(double vx, double vy, double R0, double phi0, double d0) {
    
    std::vector<double> result;
        
    double norm;
    double xc,yc;
    double alpha1, alpha2;
    double x1,y1,x2,y2;
    
    xc=centerx(R0,phi0,d0);
    yc=centery(R0,phi0,d0);
    norm=sqrt((vx-xc)*(vx-xc)+(vy-yc)*(vy-yc));
       
    alpha1=atan2((vx-xc)/norm,-(vy-yc)/norm);
    alpha2=atan2(-(vx-xc)/norm,(vy-yc)/norm);
    
    x1=xc+R0*sin(alpha1);
    y1=yc-R0*cos(alpha2);
    
    x2=xc+R0*sin(alpha2);    
    y2=yc-R0*cos(alpha2);
    
    if((vx-x1)*(vx-x1)+(vy-y1)*(vy-y1) < (vx-x2)*(vx-x2)+(vy-y2)*(vy-y2)){
        result.push_back(x1);
        result.push_back(y1); 
    }
    else{
        result.push_back(x2);
        result.push_back(y2);
    }
    
    return result;

}




int main(int argc, char** argv){
    
    double vx;
    double vy;
    double vz;
    double px;
    double py;
    double pz;
    
    double R0;
    double d0;
    double phi0;
    
    if(argc==7){
        
    px=atof(argv[1]);
    py=atof(argv[2]);
    pz=atof(argv[3]); 

    vx=atof(argv[4]);
    vy=atof(argv[5]);
    vz=atof(argv[6]);    
        
        
    }
    else{
    // throw random vertex and random momentum between -10 and 10    
    srand( (unsigned) time(NULL) * getpid());        
        
    px=-10. + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(20.)));
    py=-10. + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(20.)));
    pz=-10. + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(20.))); 

    vx=-10. + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(20.)));
    vy=-10. + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(20.)));
    vz=-10. + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(20.)));
    }
    
    trigfit trigfit1;
    trigfit1.track(px , py , pz, vx, vy, vz,1);
    int status = trigfit1.fit();
    
    R0=1.0/trigfit1.frinv();
    phi0=trigfit1.fphi0();
    d0=trigfit1.fd0();
    
    std::cout<< "Track has "<<trigfit1.n()<<"\n";
    std::cout<< "Fit status: "<<status<<"\n";
    std::cout << R0 <<"\t"<< phi0 <<"\t" << d0 <<"\n";
    
    std::vector<double> closestApproach;
    closestApproach=ClosestApproach(vx, vy, R0, phi0, d0);
    
    std::cout << px<<"\t"<<py<<"\t"<<pz<<"\t"<<vx<<"\t"<<vy<<"\t"<<vz <<"\t"<<Vx(px,py,R0,phi0,d0)<<"\t"<<Vy(px,py,R0,phi0,d0)<< "\t"<< closestApproach[0]<< "\t"<<closestApproach[1]<<"\n";
    
    return 0;
    

}






















