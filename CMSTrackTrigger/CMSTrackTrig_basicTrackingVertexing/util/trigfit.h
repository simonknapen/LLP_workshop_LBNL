#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <TRandom3.h>
#include "TGraphErrors.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TF1.h"

//  declare a single instance of the class
//  make a track by calling 
//     track(px, py, pz, vx, vy, vz, charge);
//
//  that calculates the positions of the hits
//  int n()                - returns number of hits
//  unsigned int stubmap() - returns bitmap of stubs (layer 1 is LSB)
//  int get_stubs(double *r, double *phi, double *z)
//                         - returns the number of stubs, as well as their positions
//  int get_smearedstubs(double *r, double *phi, double *z , double *sphi, double *sz)
//                         - same as get_stubs, but the positions of stubs are smeared
//                           sphi and sz will hold the errors on hit positions.
//  int fit()  fits the smeared track. Returns 0 if the fit converged
//             every time you call the fit(), it re-smears the hits
//
//  double frinv(), fphi0(), fd0(), ft(), fz0() - return the fit results
//  get_conv(double **m) returns covariance matrix m[5][5], as determined by the number of stubs
//

#ifndef TRIGFIT_H
#define TRIGFIT_H

class trigfit {

 public:

  static double helix(Double_t *x, Double_t *par)
  {
    double rproj = fabs(x[0]);
    
    double rinv = par[0];
    double phi0 = par[1];
    double d0   = par[2];
    double t    = par[3];
    double z0   = par[4];
    
    double rho = 1/rinv;
    double r0 = rho - d0; 
    double phiproj=phi0-asin((rproj*rproj+r0*r0-rho*rho)/(2*rproj*r0));
    double beta = acos((rho*rho+r0*r0-rproj*rproj)/(2*r0*rho));
    //std::cout << beta << "\n";  
    double zproj=z0+t*fabs(rho)*fabs(beta);
    
    if(x[0]>0){
      // that is the phi measurement
      return phiproj;
    }
    else{
      //this is z measurement
    return zproj;
    }
  }

  static double helix_der(Double_t *x, Double_t *par)
  {
    double rproj = fabs(x[0]);
    
    double rinv = par[0];
    double phi0 = par[1];
    double d0   = par[2];
    double t    = par[3];
    double z0   = par[4];
    
    double rho = 1/rinv;
    double r0 = rho - d0; 

    double s0 = (rproj*rproj+r0*r0-rho*rho)/(2*rproj*r0);
    double der = - (1/r0 - s0/rproj)/sqrt(1-s0*s0);

    return der;
  }

  static double prop(double phi, double R, double xD, double yD, double rho, double &beta, bool &isok) {

    if(xD*xD+yD*yD > rho*rho){
      isok = false;
      return 0;
    }
     
    double xD0 = xD*cos(phi) + yD*sin(phi);
    double yD0 = yD*cos(phi) - xD*sin(phi);

    double x1  = xD0;
    double y1  = R + yD0;
    double d2  = x1*x1 + y1*y1;
    double d   = sqrt(d2);
    
    //std::cout <<"center yuri: "  << x1 <<"\t"<<y1<<"\t"<<xD <<"\t"<<yD << "\t" << phi << "\n";
    //std::cout <<"center mine: "  << x1*cos(-phi) + y1*sin(-phi) <<"\t"<<y1*cos(-phi) - x1*sin(-phi)<<"\t"<<xD <<"\t"<<yD << "\t" << phi << "\n";  
    double phi_c = - atan(x1 / y1);
    int s = R>0? 1 : -1;
    double y2  = (rho*rho + d2 - R*R)/(2*d)*s;
    
    double phi2 = y2 / rho;
    if(fabs(phi2)>1){
      isok = false;
      return 0;
    }
    isok = true;
    phi2 = asin(phi2);
    
    double yy = rho * sin(phi2+phi_c);
    double cbeta = (y1-yy)/R;
    beta = 0;
    if(fabs(cbeta)<=1)
      beta = acos(cbeta);
    else {
      printf("cbeta too large %f\n",cbeta);
    }
    return  phi + phi2 + phi_c;
  }

  void initialize(){
    //printf("initializing trigstubs...\n");
    bool isok;
    double beta;
    double R2 = 100 * pt_cut / (0.3 * 3.8);
    for(int l=0; l<6; ++l){
      double phi12 = prop(1., R2, 0, 0, Rlayer[l], beta, isok);
      assert(isok);
      double phi22 = prop(1., R2, 0, 0, Rlayer[l]+DRlayer[l], beta, isok);
      assert(isok);
      phi22 = acos(cos(phi22 - phi12));
      bend_cut[l] = fabs(phi22 * Rlayer[l]);
    }
    f1   = new TF1("f1",  helix, -120,120,5);
    fder = new TF1("fder",helix_der,0,120,5);
    is_initialized = true;
  }

  void track (double px, double py, double pz, double vx, double vy, double vz, int charge){
    //std::cout << is_initialized << "\n";
    initialize();  
    //if(!is_initialized) initialize();
    px_ = px;
    py_ = py;
    pz_ = pz;
    vx_ = vx;
    vy_ = vy;
    vz_ = vz;
    q_  = charge;
      
    double pt = sqrt(px*px+py*py);
    double phi = atan2(py,px); 
    double RR = 100 * pt / (0.3 * 3.8);
    if(charge<0) RR = -RR;
    double lambdamax = 6;

    bool isok1, isok2;
    stubmap_ =0;
    n_ = 0;
    double beta = 0;
    for(unsigned int l=0; l<6; ++l){
      double phi2 = prop(phi, RR, vx, vy, Rlayer[l]+DRlayer[l], beta, isok2);
      double phi1 = prop(phi, RR, vx, vy, Rlayer[l], beta, isok1);
      phi2 = asin(sin(phi2 - phi1));
      double bend = phi2 * Rlayer[l];
      double zstub = vz + pz/pt*fabs(RR*beta);
      double lambdahit = fabs(zstub / Rlayer[l]);
      //std::cout << n_<<"\t{"<<Rlayer[l] <<","<< phi1 <<"}, "<<(int)(isok1&&isok2)<<" "<<bend_cut[l]<<" "<<bend<<"\n";    
      if(isok1 && isok2 && bend_cut[l]-fabs(bend)>0 && lambdahit<lambdamax){
	//trigger stub
	stubmap_ |= (1<<l);
	l_[n_]   = l;
	r_[n_]   = Rlayer[l];
	phi_[n_] = phi1;
	z_[n_]   = zstub;
	++n_;
      }
    }
  }

  void track (double rinv, double phi0, double d0, double t, double z0, int n){
    frinv_ = rinv;
    fphi0_ = phi0;
    fd0_   = d0;
    ft_    = t;
    fz0_   = z0;
    n_     = n;
    
    f1   = new TF1("f1",  helix, -120,120,5);
    fder = new TF1("fder",helix_der,0,120,5);

    f1->SetParameter(0,rinv);
    f1->SetParameter(1,phi0);
    f1->SetParameter(2,d0);
    f1->SetParameter(3,t);
    f1->SetParameter(4,z0);
    fder->SetParameters(f1->GetParameters());

    //stubs are not set (need to propagate...)
    //not needed now because ths is really for fake tracks.
  }
  
  void track (double rinv, double phi0, double d0, double t, double z0){

    //calculate the DCA point
    double phiDCA = phi0 + 2*atan(1);
    double vx = d0 * cos(phiDCA);
    double vy = d0 * sin(phiDCA);
    double vz = z0;
    double pt = fabs((0.3*3.8)/(100*rinv));
    double pz = t*pt;
    double px = pt*cos(phi0);
    double py = pt*sin(phi0);
    int charge = 1;
    if(rinv > 0) charge = -1;

    track(px,py,pz,vx,vy,vz,charge);
    fit(0);

    /* std::cout<<"********************\n"; */
    /* std::cout<<n_<<"\n"; */
    /* std::cout<<rinv<<"\t"<<frinv_<<"\n"; */
    /* std::cout<<d0<<"\t"<<fd0_<<"\n"; */
    /* std::cout<<phi0<<"\t"<<fphi0_<< " "<<fphi0_+8*atan(1.)<<"\n"; */
    /* std::cout<<t<<"\t"<<ft_<<"\n"; */
    /* std::cout<<z0<<"\t"<<fz0_<<"\n"; */
  }
  
  double frinv(){return frinv_;}
  double fphi0(){return fphi0_;}
  double fd0(){return fd0_;}
  double ft(){return ft_;}
  double fz0(){return fz0_;}
  int  fstat(){return fstat_;}
    
  double getvx(){return vx_;}
  double getvy(){return vy_;}
  double getvz(){return vz_;}

  double getpx(){return px_;}
  double getpy(){return py_;}
  double getpz(){return pz_;}    

  int fit(int mode = 0)
  {
    double stub_r[6], stub_phi[6], stub_z[6];
    double stub_sphi[6], stub_sz[6];

    int n;
    //mode:

    if(mode == 0){
      //unsmeared stubs
      get_smearedstubs(stub_r, stub_phi, stub_z, stub_sphi, stub_sz);
      n = get_stubs(stub_r, stub_phi, stub_z);//turn off smearing for testing purposes
    }
    else if(mode == 1){
      //detector resolution
      n = get_smearedstubs(stub_r, stub_phi, stub_z, stub_sphi, stub_sz);
    }
    else {
      //detector resolution + multiple scattering
      n = get_scatteredstubs(stub_r, stub_phi, stub_z, stub_sphi, stub_sz, sqrt(px_*px_+py_*py_), sqrt(vx_*vx_+vy_*vy_));
    }
    
    TGraphErrors *tg = new TGraphErrors();
    if(n<4) {
        frinv_ = 0;
        fphi0_ = 0;
        fd0_   = 0;
        ft_    = 0;
        fz0_   = -99;
        return -1;
    }
    for(int is=0; is<n; is++){
      tg->SetPoint(n+is, stub_r[is], stub_phi[is]);
      tg->SetPointError(n+is, 0, stub_sphi[is]);
      tg->SetPoint(n-is-1, -stub_r[is], stub_z[is]);
      tg->SetPointError(n-is-1, 0, stub_sz[is]);
    }

    double pt = sqrt(px_*px_+py_*py_);
    double phi = atan2(py_,px_);
    double t   = pz_ / pt;
    f1->FixParameter(0,0.3*3.8/(100. * pt));
    f1->SetParameter(1,phi);
    f1->SetParameter(2,0);
    f1->SetParameter(3,t);
    f1->SetParameter(4,vz_);
      
    tg->Fit("f1","Q");
    f1->ReleaseParameter(0);
    fstat_ = tg->Fit("f1","Q");
    int fcount = 100;
    while (fstat_!=0 && fcount>0){
      f1->SetParameter(0,f1->GetParameter(0)+0.01*rr.Rndm());
      fstat_ = tg->Fit("f1","Q");
      fcount --;
    } 
    frinv_ = f1->GetParameter(0);
    fphi0_ = f1->GetParameter(1);
    fd0_   = f1->GetParameter(2);
    ft_    = f1->GetParameter(3);
    fz0_   = f1->GetParameter(4);

    fder->SetParameters(f1->GetParameters());
    
    return fstat_;
  }
  
  unsigned int stubmap() { return stubmap_;}
  int n() {return n_;}

  int get_smearedstubs(double *r, double *phi, double *z , double *sphi, double *sz){
    for(int i=0; i<n_; ++i){
      r[i]   =   r_[i];
      phi[i] = phi_[i] + 0.01 /r_[i] * (rr.Rndm()-0.5);
      sphi[i] = 0.01 /r_[i]/sqrt(12.);
      if(r_[i]<60){
	z[i]   =   z_[i] + 0.15 * (rr.Rndm()-0.5);
	sz[i]  =  0.15/sqrt(12.);
      }
      else{ 
	z[i]   =   z_[i] + 5. * (rr.Rndm()-0.5);
	sz[i]  =   5./sqrt(12.);
      }
    }
    return n_;
  }

  int get_scatteredstubs(double *r, double *phi, double *z , double *sphi, double *sz, double pt, double rvtx){
    double theta = 0.4 * 1e-3/pt; //1mrad or scattering for 1 GeV track per layer
    // scatters befor the first hit
    double phi_bias[6]={0,0,0,0,0,0};
    for(int i=0; i<4; ++i){
      if( rvtx < Rpix[i]){
	double tms = rr.Gaus(0,theta);
	for(int j=0; j<6; ++j){
	  double dr = Rlayer[j]-Rpix[i];
	  phi_bias[j] += tms*dr/Rlayer[i];
	}
      }
    }

    for(int i=0; i<5 ; ++i){
      if(rvtx < Rlayer[i]){
	double tms = rr.Gaus(0,theta);
	for(int j=i+1; j<6; ++j){
	  double dr = Rlayer[j]-Rlayer[i];
	  phi_bias[i] += tms*dr/Rlayer[i];
	}
      }
    }

    /* std::cout<<"pt = "<<pt<<"\t rvtx = "<<rvtx<<"\n"; */
    /* for(int i=0; i<6; ++i) */
    /*   std::cout<<i<<" "<<phi_bias[i]<<"\n"; */
    
    for(int i=0; i<n_; ++i){
      r[i]   =   r_[i];
      phi[i] = phi_[i] + phi_bias[l_[i]] + 0.01 /r_[i] * (rr.Rndm()-0.5);
      sphi[i] = 0.01 /r_[i]/sqrt(12.);
      if(r_[i]<60){
	z[i]   =   z_[i] + 0.15 * (rr.Rndm()-0.5);
	sz[i]  =  0.15/sqrt(12.);
      }
      else{ 
	z[i]   =   z_[i] + 5. * (rr.Rndm()-0.5);
	sz[i]  =   5./sqrt(12.);
      }
    }
    return n_;
  }
  int get_stubs(double *r, double *phi, double *z){
    for(int i=0; i<n_; ++i){
      r[i]   =   r_[i];
      phi[i] = phi_[i];
      z[i]   =   z_[i];
    }
    return n_;
  }

  double get_sigma(){
    return f1->GetParError(2);
  }
  
  double get_z(double r){
    return f1->Eval(-fabs(r));
  }
  double get_phi(double r){
    return f1->Eval(fabs(r));
  }
  double get_track_phi(double r){
    double rvtx = fabs(r);
    double phi = get_phi(r);
    double der = fder->Eval(rvtx);
    double dx = cos(phi) - der * rvtx * sin(phi);
    double dy = sin(phi) + der * rvtx * cos(phi);
    
    return atan2(dy,dx);
  }
  double get_track_pt(){
    return 0.01*0.3*3.8 / fabs(frinv_);
  }
  double get_track_px(double r){
    return get_track_pt()*cos(get_track_phi(r));
  }
  double get_track_py(double r){
    return get_track_pt()*sin(get_track_phi(r));
  }
  double get_track_pz(){
    return get_track_pt()*ft_;
  }
  double get_true_px(){
    return px_;
  }
  double get_true_py(){
    return py_;
  }
  double get_true_pz(){
    return pz_;
  }
  double get_true_vx(){
    return vx_;
  }
  double get_true_vy(){
    return vy_;
  }
  double get_true_vz(){
    return vz_;
  }
  int get_charge(){
    return q_;
  }
  
 private:
   //geometry for the stub finding
   double Rpix[4] = {3, 6, 10, 14}; 
   double Rlayer[6]={23, 36, 51, 68, 88, 108};
   double DRlayer[6] = {0.26, 0.16, 0.16, 0.18, 0.18, 0.18};
   double pt_cut = 2; //hits are out if stub pt>pt_cut GeV

   bool is_initialized = false;
   double bend_cut[6];
   TRandom3 rr;

   //track pars
   // truth:
   double px_;
   double py_;
   double pz_;
   double vx_;
   double vy_;
   double vz_;
   int q_;

   //fit:
   TF1 *f1;
   TF1 *fder;
   double frinv_;
   double fphi0_;
   double fd0_;
   double ft_;
   double fz0_;
   int fstat_;
   
   //stubs:
   unsigned int stubmap_;
   int n_;
   int l_[6];
   double r_[6];
   double phi_[6];
   double z_[6];
   
};

#endif
