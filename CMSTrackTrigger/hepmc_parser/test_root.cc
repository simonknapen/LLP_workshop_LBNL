
#include <stdlib.h>
#include "TRandom3.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBrowser.h"
#include "TH2.h"
#include "TRandom.h"
#include <iostream>

class Rootwriter{
public:    
    void tree1w()
    {
    //create a Tree file tree1.root
   
    //create the file, the Tree and a few branches
    TFile f("/home/smknapen/CMS_track_trigger/tree1.root","recreate");
    TTree t1("t1","a simple Tree with simple variables");
    Float_t px[2], py[2], pz[2];
    Double_t random;
    Int_t ev;
    t1.Branch("px",&px,"px/F");
    t1.Branch("py",&py,"py/F");
    t1.Branch("pz",&pz,"pz/F");
    t1.Branch("random",&random,"random/D");
    t1.Branch("ev",&ev,"ev/I");
  
        
    //fill the tree
    for (Int_t i=0;i<10000;i++) {
        for (Int_t j=0;j<2;j++) {
            gRandom->Rannor(px[j],py[j]);
            pz[j] = px[j]*px[j] + py[j]*py[j];
            random = gRandom->Rndm();
            ev = i;
        }
        t1.Fill();
    }
  
    //save the Tree header. The file will be automatically closed
    //when going out of the function scope
    t1.Write();      
    f.Close();    
    };
};

int main(int argc, char *argv[]) { 
 Rootwriter rootwriter;
 rootwriter.tree1w();
 std::cout << "did it\n";    
 return 0;     
}