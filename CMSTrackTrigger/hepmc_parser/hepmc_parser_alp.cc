// -*- C++ -*-
//
// This file is part of HepMC
// Copyright (C) 2014 The HepMC collaboration (see AUTHORS for details)

#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenEvent.h"
#include "trigstubs.h"
#include <stdlib.h>
#include <vector>
#include "TROOT.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TFile.h"



int main(int argc, char *argv[]) { 
    
    if(!(argc==3)){
    std::cout << "I need inputfile name and outputfile name";
    return 0;
  }
    std::cout << "WTF\n";

    gROOT->ProcessLine("#include <vector>");
    
    //create the file, the Tree and a few branches
    TFile f(argv[2],"recreate");
    TTree t1("t1","a simple Tree with simple variables");
    std::vector<float> px, py, pz;
    std::vector<float> Vt, Vx, Vy, Vz;
    std::vector<int> pid;
    t1.Branch("pid","std::vector",&pid);
    t1.Branch("px","std::vector",&px);
    t1.Branch("py","std::vector",&py);
    t1.Branch("pz","std::vector",&pz);
    t1.Branch("Vt","std::vector",&Vt);
    t1.Branch("Vx","std::vector",&Vx);
    t1.Branch("Vy","std::vector",&Vy);
    t1.Branch("Vz","std::vector",&Vz);
    float px1, py1, pz1, Vt1, Vx1, Vy1, Vz1;
    
     std::cout << "WTF\n";
    
    { // begin scope of ascii_in and ascii_out
        
      HepMC::IO_GenEvent ascii_in(argv[1],std::ios::in);
      // declare another IO_GenEvent for writing out the good events
      HepMC::FourVector mom;
      HepMC::FourVector vert;
      
      //........................................EVENT LOOP
      int icount=0;
      int num_good_events=0;
      HepMC::GenEvent* evt = ascii_in.read_next_event();
      while ( evt ) {
	icount++;
        px.clear();
        py.clear();
        pz.clear();
	pid.clear();
        Vt.clear();
        Vx.clear();
        Vy.clear();
        Vz.clear();
    std::cout << "Processing Event Number " << icount <<"\n";      
	if ( icount%50==1 ) std::cout << "Processing Event Number " << icount
				      << " its # " << evt->event_number() 
				      << std::endl;
	 
        
	for ( HepMC::GenEvent::particle_const_iterator p 
		= evt->particles_begin(); p != evt->particles_end(); ++p ) {
      std::cout <<  abs((*p)->pdg_id()) <<"\n";  
	  if (true) {    
	    
	    mom=(*p)->momentum();
	    vert=(*p)->production_vertex()->position();

	    pid.push_back((*p)->pdg_id());
	    px.push_back(mom.px());
	    py.push_back(mom.py());
	    pz.push_back(mom.pz());
	    Vt.push_back(vert.t());
	    Vx.push_back(vert.x());
	    Vy.push_back(vert.y());
	    Vz.push_back(vert.z());      
	  } 
	}


	t1.Fill();    
	
	++num_good_events;
	
	delete evt;
	ascii_in >> evt;
	
      }
      t1.Write();
      
      //........................................PRINT RESULT
      std::cout << num_good_events << " out of " << icount 
		<< " processed events passed the cuts. Finished." << std::endl;
    } // end scope of ascii_in and ascii_out
    return 0;
    //}
}
 


