#!/share/apps/python272/bin/python
import sys
import os
import numpy as np
import pickle


def isCharged(pdgCode):
    return np.abs(pdgCode)==211 or np.abs(pdgCode)== 321 or np.abs(pdgCode)== 2212 or np.abs(pdgCode)== 11 or np.abs(pdgCode)== 12


#outdir="/home/data1/knapen/CMS_track_trigger/geant/"

filename=sys.argv[1]
outfilename=filename+"_trimmed"

# sample with 2 GeV energy cut
pion10GeV=[]
event=[]
f = open(filename, "r") 
for line in f: 
    if line[0:3]==">>>":
        if(len(event)>3):
          pion10GeV.append(np.array(event))
          print eventnr
        event=[]
        eventnr=int(line.split(" ")[-1])
   
    splitted=line.split("\t")
    if(len(splitted)==6):
        pdgcode=int(splitted[1])
        px=float(splitted[2])
        py=float(splitted[3])
        pz=float(splitted[4])
        
        if(isCharged(pdgcode)):
             event.append([eventnr,pdgcode,px,py,pz])

pion10GeV=np.array(pion10GeV)

output = open(outfilename, 'wb')
pickle.dump(pion10GeV, output)
output.close()
