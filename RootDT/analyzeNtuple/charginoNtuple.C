#define charginoNtuple_cxx
#include "charginoNtuple.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>

void charginoNtuple::InitHistos() {
  _eff_DR = new TH1F("eff_DR","Efficiency (both charginos) vs Size of Cone (#Delta R)", 9, 0.1, 1.0); 
  h_charginos=new hists_4vect("ch", "chargino");
  h_met = new TH1F("met","MET;MET [GeV]", 100, 0, 500); 
  h_metEff = new TH1F("eff_MET_trigger","met trigger efficiency v MET threshold; MET threshold [GeV]; trigger efficiency", 40, 0, 200);  

  h_jets=new hists_4vect("jets", "inclusive jet");
  h_jet0=new hists_4vect("jet0", "leading jet");
  h_jet1=new hists_4vect("jet1", "subleading jet");

}

void charginoNtuple::Loop()
{

  vector<Double_t> R_vals = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
  // histos used internally
  TH1F * h_metEffDenom = new TH1F("eff_MET denom","met trigger efficiency v MET threshold; MET threshold [GeV]; trigger efficiency", 40, 0, 200); 

  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntriesFast();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++)
    {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;

      jets->Clear();
      charginos->Clear();
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      double eventWeight=1.; //weight/nentries; to be fixed in ntuples

      // plot basic jet stuff
      for(int i = 0; i < jets->GetEntriesFast(); i++){
	TLorentzVector * jet = (TLorentzVector*)jets->At(i);
	h_jets->Fill(*jet,eventWeight);
	if(i==0) h_jet0->Fill(*jet, eventWeight);
	if(i==1) h_jet1->Fill(*jet, eventWeight);
      }

      // get charginos 
      if (charginos->GetEntriesFast() != 2) {
	std::cout<<"ERROR: you don't have 2 charginos"<<std::endl;
	continue;
      }
      TLorentzVector * chargino_1 = (TLorentzVector*)charginos->At(0);
      TLorentzVector * chargino_2 = (TLorentzVector*)charginos->At(1);
 
      // plot basic chargino stuff
      h_charginos->Fill(*chargino_1, eventWeight);
      h_charginos->Fill(*chargino_2, eventWeight);
      
      // returns the MET-4-vector of event  
      TLorentzVector MET = findconeAxis(jets);
      for(int i = 0; i<R_vals.size(); i++){
	// return (efficiency of chargino 1, efficiency of chargino 2)
	// input: MET, TLV of both charginos, and Double_t R is Delta R (the size of the cone) 
	vector<int> eff = getEfficiencies(MET, chargino_1, chargino_2, R_vals[i]);
	if (eff[0] == 1 && eff[1] == 1){
	  _eff_DR->Fill(R_vals[i]/10000);  // normalized to 10,000 events 
	}
      }
      
      // fil MET histo
      h_met->Fill(MET.Perp());
      
      // fill MET trigger efficiency as a function of threshold
      for (int i = 0; i< h_metEff->GetNbinsX()+1; i++) {
	float threshold = h_metEff->GetBinLowEdge(i);
	bool passed = ( MET.Perp() > threshold ? 1 : 0);
	h_metEffDenom->Fill(threshold,1);
	if (passed) h_metEff->Fill(threshold,1);
      }

    } // loop over events

  h_metEff->Divide(h_metEffDenom);
}

void charginoNtuple::writeHistos(const std::string& fileName)
{
  TFile * f = TFile::Open(fileName.c_str(), "RECREATE");

  h_charginos->Write();
  h_jets->Write();
  h_jet0->Write();
  h_jet1->Write();
  _eff_DR->Write();
  h_met->Write();
  h_metEff->Write();

  f->Close();
}

TLorentzVector charginoNtuple::findconeAxis(TClonesArray * jets){

  // axis of ROI cone 
  TLorentzVector MET(0., 0., 0., 0.);
  
  for(int i = 0; i < jets->GetEntriesFast(); i++){
    TLorentzVector * jet = (TLorentzVector*)jets->At(i);
    MET = MET - *jet; // includes z information   
  }
  
  //cout << MET.Px()<<" "<< MET.Py()<< " "<< MET.Pz()<< endl ;
  
  return MET;
  
}

vector<int> charginoNtuple::getEfficiencies(TLorentzVector MET,  TLorentzVector* ch_1, TLorentzVector* ch_2,  Double_t R){
  vector<int> efficiencies = {0,0}; // to keep track of each chargino
  
  // Delta R between cone axis and chargino 
  vector<Double_t> DR = {0., 0.};
  
  Double_t DR_ch1 = pow(pow(ch_1->Phi()-MET.Phi() , 2)+ pow(ch_1->Eta()-MET.Eta() ,2) , 0.5);
  Double_t DR_ch2 = pow(pow(ch_2->Phi()-MET.Phi() , 2)+ pow(ch_2->Eta()-MET.Eta() ,2) , 0.5);
  
  DR[0] = DR_ch1;
  DR[1] = DR_ch2;
  for (int i = 0; i<2; i++){
    if( DR[i] < R){
      efficiencies[i] = 1;
    }
  }
  //cout << efficiencies[0] << " "<< efficiencies[1] << endl;
  return efficiencies;
}

hists_4vect::hists_4vect(const std::string& name, const std::string& title)
{
  h_pt =new TH1F((name+"_pt") .c_str(), "",100, 0   , 500   );
  h_pt ->GetXaxis()->SetTitle((title+" p_{T} [GeV]").c_str());
  h_eta=new TH1F((name+"_eta").c_str(), "",100,-4   ,   4   );
  h_eta->GetXaxis()->SetTitle((title+" #eta").c_str());
  h_phi=new TH1F((name+"_phi").c_str(), "",100,-6.28,   6.28);
  h_phi->GetXaxis()->SetTitle((title+" #phi").c_str());
  h_m  =new TH1F((name+"_m")  .c_str(), "",100, 0   ,1000   );
  h_m  ->GetXaxis()->SetTitle((title+" p_{T} [GeV]").c_str());
}

hists_4vect::~hists_4vect()
{ }

void hists_4vect::Fill(const TLorentzVector& particle, double weight)
{
  h_pt ->Fill(particle.Pt() , weight);
  h_eta->Fill(particle.Eta(), weight);
  h_phi->Fill(particle.Phi(), weight);
  h_m  ->Fill(particle.M()  , weight);
}

void hists_4vect::Write()
{
  h_pt ->Write();
  h_eta->Write();
  h_phi->Write();
  h_m->Write();
}

